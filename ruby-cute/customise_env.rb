#!/usr/bin/ruby -w
# coding: utf-8
gem 'ruby-cute', ">=0.6"
require 'cute'
require 'pp'
require 'net/scp'
require 'date'

# This script reserve two nodes, and deploy a ubuntu1404-x64-min env.
# We upload the Aurora_provision script on the nodes.
# After the deployment we connect to the nodes and execute the script by hand.
# Finaly we save our environments with tgz-g5k

G5K_SITE = `hostname --fqdn`.split('.')[-3] # get the site name from the `hostname` command
user_name = ENV['USER']

g5k = Cute::G5K::API.new
date = Time.now.strftime("%d_%m_%Y")

#master_build_log = File.open('./master_build.log', 'w')
#worker_build_log = File.open('./worker_build.log', 'w')

# /!\ WARNING: Brute force removing the frontend known_hosts so we don't get the strict checking alert anymore.
# This quick fix may not fit your security need as you may want to strict check the key's fingerprints for connections comming from outside g5k.
# A better way would be to disable strict schecking only for cluster hosts and not from the outside.
system("rm #{ENV['HOME']}/.ssh/known_hosts")

if g5k.get_my_jobs(G5K_SITE).empty?
  job = g5k.reserve(:site => G5K_SITE, :nodes => 2,:walltime => '2:00:00', :type => :deploy, :keys => "#{ENV['HOME']}/.ssh/id_rsa")
else
  job = g5k.get_my_jobs(G5K_SITE).first
end
#puts job
nodes = job["assigned_nodes"]
puts "nodes.first : #{nodes.first}"
master = nodes.first
slaves = nodes-[master]

puts "Slaves : #{slaves}"
puts "Master node : #{master}"

# deploying all nodes, waiting for the end of deployment
g5k.deploy(job, :nodes => [master],  :env => 'ubuntu1404-x64-min')
g5k.deploy(job, :nodes => slaves,  :env => 'ubuntu1404-x64-min')
g5k.wait_for_deploy(job)

# Upload provision scripts to the master and worker node
Net::SCP.start(master, "root", {:user => "root", :keys => ["#{ENV['HOME']}/.ssh/id_rsa"], :port => 22 }) do |scp|
  scp.upload! "#{ENV['HOME']}/Heron_on_g5k/scripts/Aurora_master_provision.sh", "/root/Aurora_master_provision.sh"
end
puts "Uploaded Aurora_master_provision_script"

slaves.each{ |node| 
  Net::SCP.start(node, "root", {:user => "root", :keys => ["#{ENV['HOME']}/.ssh/id_rsa"], :port => 22 }) do |scp|
    scp.upload! "#{ENV['HOME']}/Heron_on_g5k/scripts/Aurora_worker_provision.sh", "/root/Aurora_worker_provision.sh"
  end
}
puts "Uploaded Aurora_worker_provision script"

# Upload Heron_on_g5k repository on the master node.
options = {recursive: true}
Net::SCP.upload!(master, "root", "#{ENV['HOME']}/Heron_on_g5k", "/root/", options)

Net::SSH::Multi.start do |session|

  session.group :master do
    session.use("root@#{master}")
  end
  
  session.group :slaves do
    slaves.each{ |node| session.use("root@#{node}")}
  end
  
  session.with(:master).exec("chmod +x Aurora_master_provision.sh && ./Aurora_master_provision.sh")
  session.with(:slaves).exec("chmod +x Aurora_worker_provision.sh && ./Aurora_worker_provision.sh")
  #master_build_log.puts(log1)
  #worker_build_log.puts(log2)
end

puts "Built custom images"

# Save the custom images
system("ssh root@#{master} tgz-g5k > #{ENV['HOME']}/public/Aurora_master.tgz")
system("ssh root@#{slaves.first} tgz-g5k > #{ENV['HOME']}/public/Aurora_worker.tgz")
puts "Saved custom images in #{ENV['HOME']}/public/"

# Copy environment description files templates
system("cp ../env/Aurora_master.env #{ENV['HOME']}/public/")
system("cp ../env/Aurora_worker.env #{ENV['HOME']}/public/")
puts "copied images description file canvas in #{ENV['HOME']}/public/"

# Configure Aurora_master description file
system("sed -i 's/#user#/#{user_name}/g' #{ENV['HOME']}/public/Aurora_master.env")
system("sed -i 's/#date#/#{date}/g' #{ENV['HOME']}/public/Aurora_master.env")
system("sed -i 's/#site#/#{G5K_SITE}/g' #{ENV['HOME']}/public/Aurora_master.env")

# Configure Aurora_worker description file
system("sed -i 's/#user#/#{user_name}/g' #{ENV['HOME']}/public/Aurora_worker.env")
system("sed -i 's/#date#/#{date}/g' #{ENV['HOME']}/public/Aurora_worker.env")
system("sed -i 's/#site#/#{G5K_SITE}/g' #{ENV['HOME']}/public/Aurora_worker.env")

puts "configured images description file"

# Remove old Aurora_worker and Aurora_master env from oar database
system("kaenv3 -d Aurora_master")
system("kaenv3 -d Aurora_worker")
puts "removed old environments from database"

# Add new Aurora_worker and Aurora_master env to oar database
system("kaenv3 -a #{ENV['HOME']}/public/Aurora_master.env")
system("kaenv3 -a #{ENV['HOME']}/public/Aurora_worker.env")

puts "added new environments to database: ready to deploy"
