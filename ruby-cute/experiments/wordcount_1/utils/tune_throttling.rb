#!/usr/bin/ruby -w
gem 'ruby-cute', ">=0.6"
require 'cute'
require 'pp'
require 'net/scp'
require 'curl'
require 'optparse'
require 'pathname'
# PLEASE READ THE /!\ comments as they indicate point were edits are needed from your part
# This script takes word_size as input parameter, deploy a wordcount topology with one spout and one bolt
# emitting and counting word of the specified size.
# We throttle the emitting spout with buzy waiting so the bolt don't spend time in backpressure.
# The goal of this script is to help find the minimum throttle time for which the topology doesn't exhibit backpressure

G5K_SITE = `hostname --fqdn`.split('.')[-3] # get the site name from the `hostname` command
NB_NODES = 4
CLUSTER = 'paravance'
g5k = Cute::G5K::API.new

options = {}
OptionParser.new do |opt|
  opt.on('-d', '--redeploy BOOL', 'Redeploy images and reconfigure cluster') { |o| options[:redeploy] = o }
  opt.on('-s', '--word_size INTEGER', 'Size of the words emitted in nb of characters') { |o| options[:word_size] = o }
  opt.on('-e', '--multithread BOOL', 'Launch multithreaded topology version (BOOL in {true, false}). default = false') { |o| options[:multithread] = o }
end.parse!

puts options
if !options[:word_size].eql? nil
  word_size = Integer(options[:word_size])
else
  word_size = 20
end

if !options[:multithread].eql? nil
  multithread_opt = options[:multithread]
else
  multithread_opt = "false"
end

experiment_name = "s#{word_size}_tuning"

def upload_Heron_on_g5k (master)
  # Re-Upload Heron_on_g5k repository on the master node so we have the local version.
  system("cd #{ENV['HOME']} && tar -cf Heron_on_g5k.tar Heron_on_g5k")
  options = {recursive: true}
  Net::SCP.upload!(master, "root", "#{ENV['HOME']}/Heron_on_g5k.tar", "/root/", options)
  Net::SSH.start(master, "root") do |ssh|
    # Build wordcount and deploy topology
    results = ssh.exec!("tar -xvf /root/Heron_on_g5k.tar")
    ssh.exec!("mvn install:install-file -Dfile=/root/incubator-heron/bazel-genfiles/heron/api/src/java/heron-api.jar -DgroupId=org.apache.heron -DartifactId=heron-api-extended -Dversion=0.17.8 -Dpackaging=jar")
  end
  # Instal heron-api package into maven 

  puts("Pushed Heron_on_g5k to the nodes")
end

# If no Job is running on the site we reserve a new one for this experiment. 
if g5k.get_my_jobs(G5K_SITE).empty?
  job = g5k.reserve(:site => G5K_SITE, :cluster => CLUSTER, :nodes => NB_NODES,:walltime => '1:00:00', :type => :deploy, :name => "#{experiment_name}", :keys => "#{ENV['HOME']}/.ssh/id_rsa")
else
  # If some jobs are already running we check if one of them has the same experiment name.
  # We decide that we only run one occurence of each experiment.
  # An experiment is defined by the parameters word_size (-s) and throttle_duration (-t)
  jobs = g5k.get_my_jobs(G5K_SITE)
  job = jobs.detect { |cur_job| cur_job['name'].eql? "#{experiment_name}" }
  puts job
  if job == nil
    job = g5k.reserve(:site => G5K_SITE, :cluster => CLUSTER, :nodes => NB_NODES,:walltime => '1:00:00', :type => :deploy, :name => "#{experiment_name}", :keys => "#{ENV['HOME']}/.ssh/id_rsa")
  else
    puts "a job is already running for this experiment, we recover from it"
  end
end

nodes = job["assigned_nodes"]

# The first reserved node becomes an Aurora_master node and the rest Aurora_workers
master = nodes.first
slaves = nodes-[master]

puts "Slaves : #{slaves}"
puts "Master node : #{master}"

def deploy_topology(word_size, throttle, master, multithread_opt)
  puts "Deploying the first topology with #{word_size} character words and throttle time = #{throttle} microseconds"
  results = {}
  Net::SSH.start(master, "root") do |ssh|
    if multithread_opt.eql? "false"
      results = ssh.exec!("sh /root/Heron_on_g5k/ruby-cute/experiments/wordcount_1/heron-program/multithreaded-word-count/kill.sh; sh /root/Heron_on_g5k/ruby-cute/experiments/wordcount_1/heron-program/multithreaded-word-count/deploy.sh #{word_size} #{throttle}; echo 'submitted'")
    else
      results = ssh.exec!("sh /root/Heron_on_g5k/ruby-cute/experiments/wordcount_1/heron-program/multithreaded-word-count/kill.sh; sh /root/Heron_on_g5k/ruby-cute/experiments/wordcount_1/heron-program/multithreaded-word-count/deploy_multithread.sh #{word_size} #{throttle}; echo 'submitted multithreaded wordcount'")
    end
   
  end
  puts results
  puts "finished deployment"
end

# If your experiment is already running and configured you can run the script with -d false
# This will will skip the deployment and configuration phase run the measurments only.
if options[:redeploy].eql? "true"

  # /!\ WARNING: Brute force removing the frontend known_hosts so we don't get the strict checking alert anymore.
  # This quick fix may not fit your security need as you may want to strict check the key's fingerprints for connections comming from outside g5k.
  # A better way would be to disable strict schecking only for cluster hosts and not from the outside.
  system("rm #{ENV['HOME']}/.ssh/known_hosts")

  puts "------------------------------------------------------------------------------------------------------------------"
  puts "//////////////////////////////////////////////////////////////////////////////////////////////////////////////////"
  puts "Generate a new ssh key pair (aurora_rsa) for the experiment. This key pair will be pushed in the nodes so they can"
  puts " communicate. You can overwrite this temporary key safely every run.                                                            "
  puts "\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"
  puts "------------------------------------------------------------------------------------------------------------------"
  
  # Generate a new ssh key pair for the experiment. This key pair will be pushed in the nodes so they can communicate.
  system("rm -rf #{ENV['HOME']}/.ssh/aurora_rsa && rm -rf #{ENV['HOME']}/.ssh/aurora_rsa.pub")
  system("ssh-keygen -b 4096 -N \"\" -t rsa -f #{ENV['HOME']}/.ssh/aurora_rsa")

  # deploying all nodes, waiting for the end of deployment
  g5k.deploy(job, :nodes => [master],  :env => 'Aurora_master')
  g5k.deploy(job, :nodes => slaves,  :env => 'Aurora_worker')
  g5k.wait_for_deploy(job)

  # SSH configuration:
  # - Allowing access from outside:
  # In order to access the mesos/aurora/heron monitoring tools from your
  # local workstation we need to add your workstation ssh PUBLIC key on
  # each node /root/.ssh/authorized_keys file.
  # You first need to push your workstation public key to
  # the site frontend and edit those lines to push the public key to the node.
  # This will allow you to
  # > ssh root@master_node.site.g5k -D port
  # and access the monitoring tool via a socks proxy on your local workstation
  #
  # - Communication between nodes:
  # Worker_nodes need to upload topology files from the master through scp (see /usr/local/heron/conf/devcluster/uploader.yaml)
  # We generate a key pair for the experiment called aurora_rsa and configure the nodes so they can freely access each others.

  nodes.each{ |node| 
    Net::SCP.start(node, "root", {:user => "root", :keys => ["#{ENV['HOME']}/.ssh/id_rsa"], :port => 22 }) do |scp|
      #
      # /!\ EDIT THE TWO FOLLOWING LINES TO ADD YOUR OWN PUBLIC KEY
      #
      # copy local workstation public keys into the nodes
      scp.upload! "#{ENV['HOME']}/.ssh/work_rsa.pub", "/root/.ssh/work_rsa.pub"
      scp.upload! "#{ENV['HOME']}/.ssh/g5kvpn_rsa.pub", "/root/.ssh/g5kvpn_rsa.pub"

      # /!\ NOTHING TO EDIT HERE, this generated key pair is needed for communication between nodes
      scp.upload! "#{ENV['HOME']}/.ssh/aurora_rsa", "/root/.ssh/aurora_rsa"
      scp.upload! "#{ENV['HOME']}/.ssh/aurora_rsa.pub", "/root/.ssh/aurora_rsa.pub"
    end
  }
  puts("Pushed ssh keys to the nodes")

  # Re-Upload Heron_on_g5k repository on the master node so we have the local version.
  upload_Heron_on_g5k(master)
  
  Net::SSH::Multi.start do |session|

    session.group :master do
      session.use("root@#{master}")
    end

    session.group :all  do
      nodes.each{ |node| session.use("root@#{node}")}
    end
    
    session.group :slaves do
      slaves.each{ |node| session.use("root@#{node}")}
    end

    # Configure Ssh
    # Add our local workstations public keys to authorized_keys so we can monitor our nodes from the outside
    session.with(:all).exec!("cat /root/.ssh/work_rsa.pub >> /root/.ssh/authorized_keys")
    session.with(:all).exec!("cat /root/.ssh/g5kvpn_rsa.pub >> /root/.ssh/authorized_keys")
    # Add aurora_rsa.pub to the authorized keys so nodes can communicate together
    session.with(:all).exec!("cat /root/.ssh/aurora_rsa.pub >> /root/.ssh/authorized_keys")
    # Set aurora_rsa as the default ssh key
    session.with(:all).exec!("echo 'IdentityFile /root/.ssh/aurora_rsa' >> /root/.ssh/config && echo 'StrictHostKeyChecking no' >> /root/.ssh/config && echo 'UserKnownHostsFile /dev/null' >> /root/.ssh/config")
    puts("Configured ssh")
    
    # Configure the master
    # Heron configuration
    session.with(:master).exec!("rm -rf /usr/local/heron/conf && cp -r /root/Heron_on_g5k/heron-conf /usr/local/heron/conf")
    session.with(:master).exec!("sed -i 's/user@host/root@#{master}/g' /usr/local/heron/conf/devcluster/uploader.yaml")
    session.with(:master).exec!("sed -i 's/user@host/root@#{master}/g' /usr/local/heron/conf/devcluster/heron.aurora")
    session.with(:master).exec!("sed -i 's/127.0.0.1/#{master}/g' /usr/local/heron/conf/devcluster/statemgr.yaml")
    session.with(:master).exec!("sed -i 's/127.0.0.1/#{master}/g' /usr/local/heron/conf/heron_tracker.yaml")
    session.with(:master).exec!("sed -i 's/localhost/#{master}/g' /etc/mesos/zk")
    session.with(:master).exec!("sed -i 's/127.0.0.1/#{master}/g' /etc/aurora/clusters.json")

    # Restart the master otherwise mesos-slave won't pickup the right zk value
    session.with(:master).exec!("stop mesos-master")
    session.with(:master).exec!("start mesos-master")

    session.with(:master).exec!("sudo sh -c 'echo 'MESOS_ROOT=/var/lib/mesos' >> /etc/default/thermos'")
    session.with(:master).exec!("stop thermos")
    session.with(:master).exec!("start thermos")
    # We do not use our master node as a mesos agent in our cluster (only for coordination tasks)
    session.with(:master).exec!("stop mesos-slave")

    session.with(:master).exec!("stop aurora-scheduler")
    session.with(:master).exec!("start aurora-scheduler")
    puts("Configured the master")

    # Configure the mesos-slaves
    # We follow the Aurora recommendations for Isolation configuration: https://aurora.apache.org/documentation/latest/operations/configuration/#resource-isolation
    # Our mesos installation is based on a mesosphere package, for information regarding the configuration of
    # our mesos installation read : https://github.com/mesosphere/mesos-deb-packaging/blob/fd3c3866a847d07a8beb0cf8811f173406f910df/mesos-init-wrapper#L12-L48

    # Set the master adress
    session.with(:all).exec!("sed -i 's/localhost/#{master}/g' /etc/mesos/zk")
    # A Command line option for the mesos slave can be passed as a file placed in the /etc/mesos-slave/ directory.
    # with the name corresponding to the argument name
    
    # Set Resources isolation
    session.with(:all).exec!("echo 'cgroups/cpu,cgroups/mem,disk/du' >> /etc/mesos-slave/isolation")


    # A flag can be passed as a file named ?flag to activate the flag and ?no-flag to deactivate it.
    # Set flags
    #session.with(:slaves).exec!("touch /etc/mesos-slave/?cgroups_limit_swap")
    session.with(:all).exec!("touch /etc/mesos-slave/?cgroups_enable_cfs")
    #session.with(:slaves).exec!("touch 'mesos' >> /etc/mesos-slave/?enforce_container_disk_quota")

    # Set containers
    #session.with(:all).exec!("echo 'mesos' >> /etc/mesos-slave/containerizers")
    #session.with(:all).exec!("echo 'appc,docker' >> /etc/mesos-slave/image_providers")
    #session.with(:all).exec!("echo 'filesystem/linux,docker/runtime' >> /etc/mesos-slave/isolation")

    # We force the slaves nodes to only advertise 4 cpus so we obtain exactly one container per node 
    session.with(:slaves).exec!("echo 'cpus(*):4' > /etc/mesos-slave/resources")
    # Restart the service with upstart.
    session.with(:slaves).exec!("stop mesos-slave")
    # This ensures agent doesn't recover old live executors.
    session.with(:all).exec!("rm -f /var/lib/mesos/meta/slaves/latest") 
    session.with(:slaves).exec!("start mesos-slave")
    # Ensure that thermos working dir and mesos working directory are the same
    session.with(:slaves).exec!("sudo sh -c 'echo 'MESOS_ROOT=/var/lib/mesos' >> /etc/default/thermos'")
    session.with(:slaves).exec!("stop thermos")
    session.with(:slaves).exec!("start thermos")
    puts("Configured the slaves")
  end
  
  Net::SSH.start(master, "root") do |ssh|
    ssh.exec("heron-tracker 2>> /dev/null &")
    ssh.exec("heron-ui 2>> /dev/null &")
  end  
end

def backpressure_presence(master, throttle, word_size, duration, multithread_opt)
  deploy_topology(word_size, throttle, master, multithread_opt)
  delay=30
  puts "waiting #{duration} seconds before checking backpressure"
  sleep(duration)
  execution_state = Curl::Easy.perform("http://#{master}:8888/topologies/executionstate?cluster=devcluster&environ=devel&topology=WordCountTopology")
  parsed_execution_state = JSON.parse(execution_state.body_str)
  puts JSON.pretty_generate(parsed_execution_state)
  submission_time = parsed_execution_state['result']['submission_time']
  puts submission_time

  # Request the time spent in backpressure by the Counsume_and_count container over 1 min period starting from submition_time + 30
  backpressure = Curl::Easy.perform("http://#{master}:8888/topologies/metricsquery?cluster=devcluster&environ=devel&starttime=#{submission_time + delay}&query=DEFAULT%280%2C+TS%28__stmgr__%2C%2A%2C__time_spent_back_pressure_by_compid%2Fcontainer_1_Consume_and_count_1%29%29&endtime=#{submission_time + delay + 180}&topology=WordCountTopology")
  parsed_backpressure = JSON.parse(backpressure.body_str)
  puts JSON.pretty_generate(parsed_backpressure)
  # Look into the json for backpressure presence
  # More precisely we try to detect if the data field time series contains an entry with a value is greater than 0
  # Such a value indicates backpressure presence.
  backpressure_detected = parsed_backpressure['result']['timeline'].first['data'].detect{ |x| x[1] > 0}
  if backpressure_detected.nil?
    backpressure_time = 0
    puts "no backpressure"
  else
    backpressure_time = backpressure_detected[1]
    puts "The consume and count container spent #{backpressure_time} ms in backpressure over 1 min period starting from #{backpressure_detected[0]}"
  end
  
  return backpressure_time
end
upload_Heron_on_g5k(master)

# Find min throttle_time using big step/giant step algorithm
# We look for throttle time in the 0 to 100 microseconds range
# let i be in [0..sqrt(100)].
# we start with giant steps i*sqrt(100) for the throttle values
# once we get 0 backpressure for i == k the we start back from (k-1)*sqrt(100)
# and we the make +1 increments until we have 0 backpressure

# Big step start from 10ms throttle
duration = 240
k = (1..10).detect{ |i| backpressure_presence(master, i * 10, word_size, duration, multithread_opt) == 0}
#duration = 300
throttle = (((k-1)*10 + 1)..100).detect{ |i| backpressure_presence(master, i, word_size, duration, multithread_opt) == 0}

if throttle.nil?
  puts "Backpressure detected even with maximum throttle value"
else
  puts " potential optimal throttle_time: #{throttle}"
  throttle_verif = (throttle..2*10).detect{ |i| backpressure_presence(master, i, word_size, duration, multithread_opt) == 0}
  puts " verification result throttle_time: #{throttle_verif}"
end
