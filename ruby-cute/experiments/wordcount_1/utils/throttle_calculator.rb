# Throttle calculator:
# - Specification: throttle_calculator (word_size, throughput)
#      - word_size: An integer representing the size of the words in nb of charater emited by the spout
#      - throughput: An Integer representing the desired throughput in Mo/s
# - Semantics:
#      Given a word size and a desired throughput in Mo/s returns a throttle time value in microseconds.
#      The throttle value is used to control the throughput of the spout used in WordCount topologies.
#      It's value represent the time in microseconds we wait between two tuples emission by the spout(s).
# - Example: throttle_calculator 1500 180 -> 12
#      This tells us that if we want a throughput of 180 Mo/s with 1500 caracter long words we need to wait 12 microseconds between
#      two tuples emission.
# - Implementation:
#      Througput (t/s) = 1E6 / throttle (microsec)
#      => Throughput (o/s) = word_size * 1E6 / throttle (microsec)
#      => Throughput(mo/s) = word_size (octet) / throttle (microsec)
#      => throttle (microsec) =  word_size(octet) /Throughput (mo/s)
def throttle_calculator (word_size, throughput)
  return word_size/throughput
end

