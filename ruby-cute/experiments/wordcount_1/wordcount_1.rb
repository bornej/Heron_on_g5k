#!/usr/bin/ruby -w
gem 'ruby-cute', ">=0.6"
require 'cute'
require 'pp'
require 'net/scp'
require 'curl'
require 'optparse'
require 'pathname'

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# PLEASE READ THE /!\ comments as they indicate point were edits are needed from your part
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

G5K_SITE = `hostname --fqdn`.split('.')[-3] # get the site name from the `hostname` command
NB_NODES = 4
WALLTIME = '00:40:00'
g5k = Cute::G5K::API.new

options = {}
OptionParser.new do |opt|
  opt.on('-d', '--redeploy BOOL', 'Redeploy images and reconfigure cluster (with BOOL in {true, false}). default = true') { |o| options[:redeploy] = o }
  opt.on('-s', '--word_size INTEGER', 'Size of the words emitted in nb of characters. default = 20') { |o| options[:word_size] = o }
  opt.on('-t', '--throttle INTEGER', 'Throttle duration for the spout in Microseconds. default = 15') { |o| options[:throttle] = o }
  opt.on('-a', '--padding INTEGER', 'Padding: an integer describing the padding percentage for containers cpus. By default Heron allocate one cpu per heron instance + 0.25 cpu for the stream_manager. So if a container contain a single Heron instance (spout, bolt) its size will be 1.25 cpu. By setting a padding of 100 we increase the size of such a container to 2.25 cpu. By setting a padding of 200 we get 3.25 cpu., default = 200') { |o| options[:padding] = o }
  opt.on('-n', '--nb_of_runs INTEGER', 'The number of time we repeat the experience (not implemented yet), default = 1') { |o| options[:nb_runs] = o }
  opt.on('-l', '--length INTEGER', 'Duration of the mesurements in seconds. default = 240') { |o| options[:length] = o }
  opt.on('-m', '--measure BOOL', 'Launch measurements (BOOL in {true, false}). default = true') { |o| options[:measure] = o }
  opt.on('-u', '--resubmit BOOL', 'Resubmit the topology before the measurements (BOOL in {true, false}). default = true') { |o| options[:resubmit] = o }
  
  opt.on('-c', '--cpu BOOL', 'Activate cpu measurements (BOOL in {true, false}). default = true') { |o| options[:cpu] = o }
  opt.on('-p', '--throughput BOOL', 'Activate Heron throughput measurements (BOOL in {true, false}). default = true') { |o| options[:throughput] = o }
  opt.on('-w', '--network BOOL', 'Activate network measurements (BOOL in {true, false}). default = true') { |o| options[:network] = o }
  opt.on('-b', '--backpressure BOOL', 'Detect Backpressure (BOOL in {true, false}). default = true') { |o| options[:backpressure] = o }
  opt.on('-x', '--monothread BOOL', 'Submit monothreaded topology (BOOL in {true, false}). default = true') { |o| options[:monothread] = o }
  opt.on('-y', '--multithread BOOL', 'Submit multithreaded topology (BOOL in {true, false}). default = true') { |o| options[:multithread] = o }
  opt.on('-j', '--job_id INTEGER', 'Recover from an already running job with: job_id. default = nil') { |o| options[:job_id] = o }
  opt.on('-o', '--advertised_cpu_per_node INTEGER', 'Number of cpu offered by the mesos_slaves to the mesos master. default = 8') { |o| options[:advertised_cpu_per_node] = o }
  opt.on('-r', '--cluster_name STRING', 'The cluster to run the experiment on . default = rennes -> paravance, nancy -> grisou') { |o| options[:cluster_name] = o }
  opt.on('-z', '--cpu_test BOOL', 'Run mesos cpu test program. default = false') { |o| options[:cpu_test] = o }
end.parse!

if !options[:redeploy].eql? nil
  redeploy_opt = options[:redeploy]
else
  redeploy_opt = "true"
end

puts options
if !options[:word_size].eql? nil
  word_size = Integer(options[:word_size])
else
  word_size = 20
end

if !options[:throttle].eql? nil
  throttle = Integer(options[:throttle])
else
  throttle = 0
end

if !options[:padding].eql? nil
  padding = Integer(options[:padding])
else
  padding = 200
end

if !options[:nb_runs].eql? nil
  nb_runs = Integer(options[:nb_runs])
else
  nb_runs = 1
end

if !options[:length].eql? nil
  length = Integer(options[:length])
else
  length = 600
end

if !options[:measure].eql? nil
  measure_opt = options[:measure]
else
  measure_opt = "true"
end

if !options[:backpressure].eql? nil
  backpressure_opt = options[:backpressure]
else
  backpressure_opt = "true"
end

if !options[:cpu].eql? nil
  cpu_opt = options[:cpu]
else
  cpu_opt = "true"
end

if !options[:resubmit].eql? nil
  resubmit_opt = options[:resubmit]
else
  resubmit_opt = "true"
end

if !options[:throughput].eql? nil
  throughput_opt = options[:throughput]
else
  throughput_opt = "true"
end

if !options[:network].eql? nil
  network_opt = options[:network]
else
  network_opt = "true"
end

if !options[:multithread].eql? nil
  multithread_opt = options[:multithread]
else
  multithread_opt = "true"
end

if !options[:monothread].eql? nil
  monothread_opt = options[:monothread]
else
  monothread_opt = "true"
end

if !options[:job_id].eql? nil
  job_id_opt = options[:job_id]
else
  job_id_opt = nil
end

if !options[:advertised_cpu_per_node].eql? nil
  ADVERTISED_CPU_PER_NODE = Integer(options[:advertised_cpu_per_node])
else
  ADVERTISED_CPU_PER_NODE = padding/100 + 2
end

if !options[:cluster_name].eql? nil
  CLUSTER = options[:cluster_name]
else
  CLUSTER = case G5K_SITE
            when "rennes" then "paravance"
            when "nancy" then "grisou"
            when "grenoble" then "dahu"
            when "lyon" then "nova"
            else ""
            end
end

if !options[:cpu_test].eql? nil
  cpu_test_opt = options[:cpu_test]
else
  cpu_test_opt = "false"
end

if cpu_test_opt.eql? "true"
  experiment_name = "cpu_test"
  NB_NODES = 2
else
  # An experiment is defined by the values of parameters: word_size, throttle, padding, measurement_length.
  if (monothread_opt.eql? "true") && (multithread_opt.eql? "true")
    experiment_name = "s#{word_size}_t#{throttle}_p#{padding}_l#{length}_combined_nomem"
  elsif(monothread_opt.eql? "true") && (multithread_opt.eql? "false")
    experiment_name = "s#{word_size}_t#{throttle}_p#{padding}_l#{length}_monothread_nomem"
  elsif(monothread_opt.eql? "false") && (multithread_opt.eql? "true")
    experiment_name = "s#{word_size}_t#{throttle}_p#{padding}_l#{length}_multithread_nomem"
  end
end

# Create a folder for the experiment
# Each time we run the same experiment we create a new subfolder folder whose name is the run_id (0,1,2,...)
base_dir = Dir.pwd
puts "base_dir:#{base_dir}"
experiment_dir = "./mesures/#{experiment_name}_#{G5K_SITE}_#{CLUSTER}"
system("mkdir -p #{experiment_dir}")

Dir.chdir("#{experiment_dir}")
subdir_list = Dir['*/']
max = "0"
subdir_list.each{ |dir| 
  if dir.to_i > max.to_i
    max = dir
  end
}
puts subdir_list
run_id = "#{max.to_i+1}"
puts "run_id: #{run_id}"
system("mkdir -p #{run_id}")
Dir.chdir("#{base_dir}")

if cpu_test_opt.eql? "true"
  cpu_experiment_0_dir = "#{experiment_dir}/#{run_id}/experiment_0"
  cpu_experiment_1_dir = "#{experiment_dir}/#{run_id}/experiment_1"
  system("mkdir -p #{cpu_experiment_0_dir}")
  system("mkdir -p #{cpu_experiment_1_dir}")
else
  monothread_dir = "#{experiment_dir}/#{run_id}/monothread"
  multithread_dir = "#{experiment_dir}/#{run_id}/multithread"
  system("mkdir -p #{monothread_dir}")
  system("mkdir -p #{multithread_dir}")
end
measurements_dir = "#{experiment_dir}/#{run_id}"

def upload_Heron_on_g5k (master)
  # Re-Upload Heron_on_g5k repository on the master node so we have the local version.
  system("cd #{ENV['HOME']} && tar -cf Heron_on_g5k.tar Heron_on_g5k")
  options = {recursive: true}
  Net::SCP.upload!(master, "root", "#{ENV['HOME']}/Heron_on_g5k.tar", "/root/", options)
  Net::SSH.start(master, "root") do |ssh|
    # Build wordcount and deploy topology
    results = ssh.exec!("tar -xvf /root/Heron_on_g5k.tar")
    # Instal heron-api package into maven 
    ssh.exec!("mvn install:install-file -Dfile=/root/incubator-heron/bazel-genfiles/heron/api/src/java/heron-api.jar -DgroupId=org.apache.heron -DartifactId=heron-api-extended -Dversion=0.17.8 -Dpackaging=jar")
  end
  puts("Pushed Heron_on_g5k to the nodes")
end

# If we specified a job_id as command line parameter we recover from it
if !job_id_opt.eql? nil
  jobs = g5k.get_my_jobs(G5K_SITE)
  job = jobs.detect { |cur_job|
    cur_job['uid'].eql? job_id_opt.to_i }
  puts job
  puts "> Recover from job_id #{job_id_opt}"
else
  job = g5k.reserve(:site => G5K_SITE, :cluster => CLUSTER, :nodes => NB_NODES,:walltime => WALLTIME, :type => :deploy, :name => "#{experiment_name}", :keys => "#{ENV['HOME']}/.ssh/id_rsa")
end

nodes = job["assigned_nodes"]

# The first reserved node becomes an Aurora_master node and the rest Aurora_workers
master = nodes.first
slaves = nodes-[master]

# If your experiment is already running you can run the script with -d false (or --redeploy false)
# This will will skip the deployment and configuration phase run the measurements only.
if redeploy_opt.eql? "true"

  # /!\ WARNING: Brute force removing the frontend known_hosts so we don't get the strict checking alert anymore.
  # This quick fix may not fit your security need as you may want to strict check the key's fingerprints for connections comming from outside g5k.
  # A better way would be to disable strict schecking only for cluster hosts and not from the outside.
  system("rm #{ENV['HOME']}/.ssh/known_hosts")

  # Generate the experiments ssh key pair if it doesn't exist
  if !File.file?("#{ENV['HOME']}/.ssh/aurora_rsa") || !File.file?("#{ENV['HOME']}/.ssh/aurora_rsa.pub")
    puts "------------------------------------------------------------------------------------------------------------------"
    puts "//////////////////////////////////////////////////////////////////////////////////////////////////////////////////"
    puts "Generate a new ssh key pair (aurora_rsa) for the experiment. This key pair will be pushed in the nodes so they can"
    puts " communicate. This temporary key is overwritten every run.                                                            "
    puts "\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"
    puts "------------------------------------------------------------------------------------------------------------------"
    
    # Generate a new ssh key pair for the experiment. This key pair will be pushed in the nodes so they can communicate.
    #system("rm -rf #{ENV['HOME']}/.ssh/aurora_rsa && rm -rf #{ENV['HOME']}/.ssh/aurora_rsa.pub")
    system("ssh-keygen -b 4096 -N \"\" -t rsa -f #{ENV['HOME']}/.ssh/aurora_rsa")
  end 
  # deploying all nodes, waiting for the end of deployment
  g5k.deploy(job, :nodes => [master],  :env => 'Aurora_master')
  g5k.deploy(job, :nodes => slaves,  :env => 'Aurora_worker')
  g5k.wait_for_deploy(job)

  puts "> Wait 30s for the nodes to start"
  sleep(30)
  
  # SSH configuration:
  # ------------------
  # - Allowing access from outside g5k:
  # In order to access the mesos/aurora/heron monitoring tools from your
  # local workstations we need to add your workstations ssh PUBLIC keys on
  # each node /root/.ssh/authorized_keys file.
  # /!\ 
  # You first need to push your workstation public key to
  # the site frontend and edit the following lines to push the public key to the nodes.
  # This will allow you to
  # > ssh root@master_node.site.g5k -D port
  # and access the monitoring tool via a socks proxy on your local workstation
  #
  # - Communication between nodes:
  # Worker_nodes need to download topology files from the master through scp (see /usr/local/heron/conf/devcluster/uploader.yaml)
  # We generate a key pair for the experiment called aurora_rsa and configure the nodes so they can freely access each others.
  nodes.each{ |node| 
    Net::SCP.start(node, "root", {:user => "root", :keys => ["#{ENV['HOME']}/.ssh/id_rsa"], :port => 22 }) do |scp|
      #-------------------------------------------------------------+
      # /!\ EDIT THE TWO FOLLOWING LINES TO ADD YOUR OWN PUBLIC KEY |
      #-------------------------------------------------------------+
      # copy local workstation public keys into the nodes
      scp.upload! "#{ENV['HOME']}/.ssh/work_rsa.pub", "/root/.ssh/work_rsa.pub"
      scp.upload! "#{ENV['HOME']}/.ssh/g5kvpn_rsa.pub", "/root/.ssh/g5kvpn_rsa.pub"

      # /!\ NOTHING TO EDIT HERE, this generated key pair is needed for communication between nodes
      scp.upload! "#{ENV['HOME']}/.ssh/aurora_rsa", "/root/.ssh/aurora_rsa"
      scp.upload! "#{ENV['HOME']}/.ssh/aurora_rsa.pub", "/root/.ssh/aurora_rsa.pub"
    end
  }
  puts("> Pushed ssh keys to the nodes")

  # Re-Upload Heron_on_g5k repository on the master node so we have the local version.
  # ----------------------------------------------------------------------------------
  upload_Heron_on_g5k(master)

  #Configure the master node and the slaves
  Net::SSH::Multi.start do |session|

    session.group :master do
      session.use("root@#{master}")
    end

    session.group :all  do
      nodes.each{ |node| session.use("root@#{node}")}
    end
    
    session.group :slaves do
      slaves.each{ |node| session.use("root@#{node}")}
    end

    # ---------------------------------------------
    # Configure Ssh
    # ---------------------------------------------
    # Add our local workstations public keys to authorized_keys so we can monitor our nodes from the outside
    session.with(:all).exec!("cat /root/.ssh/work_rsa.pub >> /root/.ssh/authorized_keys")
    session.with(:all).exec!("cat /root/.ssh/g5kvpn_rsa.pub >> /root/.ssh/authorized_keys")
    # Add aurora_rsa.pub to the authorized keys so nodes can communicate together
    session.with(:all).exec!("cat /root/.ssh/aurora_rsa.pub >> /root/.ssh/authorized_keys")
    # Set aurora_rsa as the default ssh key
    session.with(:all).exec!("echo 'IdentityFile /root/.ssh/aurora_rsa' >> /root/.ssh/config && echo 'StrictHostKeyChecking no' >> /root/.ssh/config && echo 'UserKnownHostsFile /dev/null' >> /root/.ssh/config")
    puts("> Configured ssh")

    # ---------------------------------------------
    # Configure the master
    # ---------------------------------------------
    # Heron configuration:
    # Configuration files for heron are located in /usr/local/heron/conf/[aurora_cluster_name]/*.yaml
    # In each configuration file we replace the occurences of localhost, 127.0.0.1, user@host with the master node hostname
    session.with(:master).exec!("rm -rf /usr/local/heron/conf && cp -r /root/Heron_on_g5k/heron-conf /usr/local/heron/conf")
    session.with(:master).exec!("sed -i 's/user@host/root@#{master}/g' /usr/local/heron/conf/devcluster/uploader.yaml")
    session.with(:master).exec!("sed -i 's/user@host/root@#{master}/g' /usr/local/heron/conf/devcluster/heron.aurora")
    session.with(:master).exec!("sed -i 's/127.0.0.1/#{master}/g' /usr/local/heron/conf/devcluster/statemgr.yaml")
    session.with(:master).exec!("sed -i 's/127.0.0.1/#{master}/g' /usr/local/heron/conf/heron_tracker.yaml")
    session.with(:master).exec!("sed -i 's/localhost/#{master}/g' /etc/mesos/zk")
    session.with(:master).exec!("sed -i 's/127.0.0.1/#{master}/g' /etc/aurora/clusters.json")
    # Restart the master otherwise mesos-slave won't pickup the right zk value
    session.with(:master).exec!("stop mesos-master")
    session.with(:master).exec!("start mesos-master")
    # Make sure thermos and mesos work_dir are the same
    session.with(:master).exec!("sudo sh -c 'echo 'MESOS_ROOT=/var/lib/mesos' >> /etc/default/thermos'")
    session.with(:master).exec!("stop thermos")
    session.with(:master).exec!("start thermos")
    # We do not use our master node as a mesos agent in our cluster (only for coordination tasks)
    # so we stop the mesos-slave service on the master node.
    session.with(:master).exec!("stop mesos-slave")
    session.with(:master).exec!("stop aurora-scheduler")
    session.with(:master).exec!("start aurora-scheduler")
    puts("> Configured the master")

    # ---------------------------------------------
    # Configure the mesos-slaves
    # ---------------------------------------------

    # Set the master adress
    session.with(:all).exec!("sed -i 's/localhost/#{master}/g' /etc/mesos/zk")

    # ---------------------------------------------
    # Set Mesos Resources isolation:
    # We follow the Aurora recommendations for Isolation configuration:
    # https://aurora.apache.org/documentation/latest/operations/configuration/#resource-isolation
    # Our mesos installation is based on a mesosphere package, for information regarding the configuration
    # read : https://github.com/mesosphere/mesos-deb-packaging/blob/fd3c3866a847d07a8beb0cf8811f173406f910df/mesos-init-wrapper#L12-L48
    # ---------------------------------------------
    
    # Command line arguments for the mesos slaves can be passed as a files placed in the /etc/mesos-slave/ directory.
    # Each file name must correspond to a valid argument name
    session.with(:all).exec!("echo 'cgroups/cpu,disk/du' >> /etc/mesos-slave/isolation")
    #session.with(:all).exec!("echo 'cgroups/cpu,cgroups/mem,disk/du' >> /etc/mesos-slave/isolation")

    #----------------------------------------------
    # Seting up slave flags:
    # Command line flags can be passed as a files named ?flag to activate the flag and ?no-flag to deactivate it.
    #----------------------------------------------
    # Cgroups feature flag to enable hard limits on CPU resources via the CFS bandwidth limiting subfeature.
    session.with(:all).exec!("touch /etc/mesos-slave/?cgroups_enable_cfs")
    #Cgroups feature flag to enable counting of processes and threads inside a container. (default: false)
    session.with(:all).exec!("touch /etc/mesos-slave/?cgroups_cpu_enable_pids_and_tids_count")

    # Set containers
    #session.with(:all).exec!("echo 'mesos' >> /etc/mesos-slave/containerizers")
    #session.with(:all).exec!("echo 'appc,docker' >> /etc/mesos-slave/image_providers")
    #session.with(:all).exec!("echo 'filesystem/linux,docker/runtime' >> /etc/mesos-slave/isolation")

    # We force the slaves nodes to only advertise a particular number of cpus so we obtain exactly one container per node 
    session.with(:slaves).exec!("echo 'cpus(*):#{ADVERTISED_CPU_PER_NODE}' > /etc/mesos-slave/resources")
    # Stop the service with upstart.
    session.with(:slaves).exec!("stop mesos-slave")
    # This ensures agent doesn't recover old live executors.
    session.with(:all).exec!("rm -f /var/lib/mesos/meta/slaves/latest")
    session.with(:slaves).exec!("start mesos-slave")
    # Ensure that thermos working dir and mesos working directory are the same
    session.with(:slaves).exec!("sudo sh -c 'echo 'MESOS_ROOT=/var/lib/mesos' >> /etc/default/thermos'")
    session.with(:slaves).exec!("stop thermos")
    session.with(:slaves).exec!("start thermos")

    # Install nicstat network monitoring tool on the slaves
    session.with(:slaves).exec!("sudo apt-get install nicstat")
    puts("> Configured the slaves")
  end
  
  # ---------------------------------------------
  # Start heron-tracker and heron-ui
  # ---------------------------------------------
  Net::SSH.start(master, "root") do |ssh|
    ssh.exec("heron-tracker 2>> /dev/null &")
    ssh.exec("heron-ui 2>> /dev/null &")
  end
  
end

# submit_topology(word_size, throttle, padding, master, measurements_dir, multithread_opt)
# Semantics:
#    Submit the wordcount topology to the heron cluster using heron-cli
# Specification:
#    - word_size: An integer defining the number of character of the words emitted by the spout(s)
#    in the wordcount topology.
#    - throttle: An integer defining the waiting time in microseconds
#    between each word emission from the spout.
#    - padding: An integer defing the padding percentage for mesos containers.
#    - master: A string containing the name of the cluster's master node.
#    - measurements_dir: A string defining the measurement_dir path
#    - multithread_opt: A string ["true", "false"] defining which topology to submit ("true" for multithreaded topology)
def submit_topology(word_size, throttle, padding, master, measurements_dir, multithread_opt)
  puts "-----------------------------------------------------------------------------------------------"
  puts "> Submitting the topology to the cluster: with word_size = #{word_size}, throttle = #{throttle}, padding = #{padding}"
  if multithread_opt.eql? "true"
    puts "> (Multithread version)"
  end
  results = {}
  Net::SSH.start(master, "root") do |ssh|
    # Kill wordcount topology if it runs on the cluster, build the wordcount maven project and submit topology
    if multithread_opt.eql? "false"
      # submit monothreaded topology
      results = ssh.exec!("sh /root/Heron_on_g5k/ruby-cute/experiments/wordcount_1/heron-program/multithreaded-word-count/kill.sh; sh /root/Heron_on_g5k/ruby-cute/experiments/wordcount_1/heron-program/multithreaded-word-count/deploy.sh #{word_size} #{throttle} #{padding}; echo 'submitted'")
    else
      # submit multithreaded topolgy
      results = ssh.exec!("sh /root/Heron_on_g5k/ruby-cute/experiments/wordcount_1/heron-program/multithreaded-word-count/kill.sh; sh /root/Heron_on_g5k/ruby-cute/experiments/wordcount_1/heron-program/multithreaded-word-count/deploy_multithread.sh #{word_size} #{throttle} #{padding}; echo 'submitted'")
    end
  end
  puts results
  puts "> Finished submitting topology"
end

# get_physical_plan(master, measurements_dir)
# Specification:
#    - master: A string containing the name of the cluster's master node.
#    - measurements_dir: A string defining the measurement_dir path
# Semantics:
#    Fetch the topology physical plan from Heron-tracker and write the result as a JSON file.
# Returns: the parsed physical plan
def get_physical_plan(master, measurements_dir)
  puts ">> Fetching physical plan from Heron"
  json_file = File.new("#{measurements_dir}/packing_plan.json", 'w')
  physical_plan = Curl::Easy.perform("http://#{master}:8888/topologies/physicalplan?cluster=devcluster&environ=devel&topology=WordCountTopology")
  parsed_physical_plan = JSON.parse(physical_plan.body_str)
  stmgrs = parsed_physical_plan['result']['stmgrs']
  while stmgrs.empty?
    sleeping_time = 180
    puts ">> waiting #{sleeping_time} seconds for the topology to run on the cluster"
    sleep (sleeping_time)
    physical_plan = Curl::Easy.perform("http://#{master}:8888/topologies/physicalplan?cluster=devcluster&environ=devel&topology=WordCountTopology")
    parsed_physical_plan = JSON.parse(physical_plan.body_str)
    stmgrs = parsed_physical_plan['result']['stmgrs']
  end
  json_file.write (JSON.pretty_generate(parsed_physical_plan))
  json_file.close
  puts ">> Finished fetching physical plan. Physical plan written in #{measurements_dir}/physical_plan.json"
  return parsed_physical_plan
end

#  get_node_instances(physical_plan, hostname)
# Semantics:
#     Return the id of the Heron instances running on the slave node named hostname
# Specification:
#    - hostname: a string containing the hostname of the slave node we wish to retrieve
#          the Heron instances informations.
#    - physical_plan: the topology physical plan obtained from get_packing_plan procedure.
# Example:  get_node_instances("graphene-91.nancy.grid5000.fr")
#          returns ["container_2_Emit_random_words_2"]
def get_node_instances(physical_plan, hostname)
  stmgrs = physical_plan['result']['stmgrs']
  stmgrs_on_node = []
  stmgrs.each { |stmgr|
    if stmgr[1]['host'].eql? hostname
      stmgrs_on_node.push(stmgr)
    end
  }
  instances = []
  stmgrs_on_node.each { |stmgr|
    stmgr[1]['instance_ids'].each { |instance| instances.push(instance) }
  }
  return instances
end

# cpu_measure(duration, master, slaves, measurements_dir)
# Specifications:
#     - 'slaves' an array of string representing the names of cluster mesos slaves.
#    retrieved from job = g5k.get_my_jobs(G5K_SITE)[<my actual job>]
#                   nodes = job["assigned_nodes"]
#                   master = nodes.first
#                   slaves = nodes-[master]
# Semantics:
#     Fetches mesos cpu usage for each slave-node container.
#     For each measurement point we fetch cpu_usage at time A and (time_A + duration) = time_B and write the results in
#     csv_file_A and csv_file_B respectively.
def cpu_measure(duration, master, slaves, measurements_dir)
  puts "-------------------------------------"
  puts("> Fetching the cpu metrics from mesos")
  # Creating output files for measurements
  csv_file_A = File.new("#{measurements_dir}/mesos_cpu_A.csv", 'w')
  csv_file_B = File.new("#{measurements_dir}/mesos_cpu_B.csv", 'w')
  json_file_A = File.new("#{measurements_dir}/mesos_containers_A.json", 'w')
  json_file_B = File.new("#{measurements_dir}/mesos_containers_B.json", 'w')
  
  csv_file_A.puts "mesure_id;slave_id;executor_id;heron_instances;cpus_limit;timestamp_A;cpus_system_time_secs_A;cpus_user_time_secs_A;cpus_throttled_time_secs_A;processes;threads"
  csv_file_B.puts "mesure_id;slave_id;executor_id;heron_instances;cpus_limit;timestamp_B;cpus_system_time_secs_B;cpus_user_time_secs_B;cpus_throttled_time_secs_B;processes;threads"

  physical_plan = get_physical_plan(master, measurements_dir)
  measurement_interval = 10
  puts("> The measurement will last #{measurement_interval * ((duration/measurement_interval) + 1)} seconds")
  0.upto(duration/measurement_interval) do |n|
    # Fetch the measurements
    threads = []
    slaves.each{ |node|
      threads << Thread.new do
        begin
          a = Curl::Easy.perform("http://#{node}:5051/containers")
          parsed_A = JSON.parse(a.body_str)
          sleep(measurement_interval)
          b = Curl::Easy.perform("http://#{node}:5051/containers")
          parsed_B = JSON.parse(b.body_str)
        rescue SystemExit, Interrupt
          raise
        rescue TypeError => e
          puts e.backtrace.inspect
          puts "> Omited measure point: parsed nil"
        rescue Exception => e
          a = nil
          b = nil
          parsed_A = nil
          parsed_B = nil
          puts e.backtrace.inspect
          puts "Curl error, couldn't fetch mesos containers json"
        end
        instances = get_node_instances(physical_plan, node)
        if instances.empty?
          instances.push("topology_master")
        end
        # Catch Typeerrors due to bad parsing (Nil values)
        begin
          if parsed_A.nil? || parsed_B.nil? || parsed_A.empty? || parsed_B.empty? || parsed_A.detect{|x| x.empty?} || parsed_B.detect{|x| x.empty?}
            puts "> Omited measure point: parsed nil"
          else
            parsed_A.each { |container|
              csv_file_A.write ("#{n}" + ";" + "#{node}" + ";" + "#{container['executor_id']}" + ";" + "#{instances}" + ";" + "#{container['statistics']['cpus_limit']}" + ";" + "#{container['statistics']['timestamp']}" + ";" + "#{container['statistics']['cpus_system_time_secs']}" + ";" + "#{container['statistics']['cpus_user_time_secs']}" + ";" + "#{container['statistics']['cpus_throttled_time_secs']}"+ ";" + "#{container['statistics']['processes']}" + ";" + "#{container['statistics']['threads']}" + "\n")
            }
            parsed_B.each { |container|
              csv_file_B.write ("#{n}" + ";" + "#{node}" + ";" + "#{container['executor_id']}" + ";" + "#{instances}" + ";" + "#{container['statistics']['cpus_limit']}" + ";" + "#{container['statistics']['timestamp']}" + ";" + "#{container['statistics']['cpus_system_time_secs']}" + ";" + "#{container['statistics']['cpus_user_time_secs']}" + ";" + "#{container['statistics']['cpus_throttled_time_secs']}" + ";" + "#{container['statistics']['processes']}" + ";" + "#{container['statistics']['threads']}" + "\n")
            }

            json_file_A.write (parsed_A)
            json_file_B.write (parsed_B)            
          end
        rescue SystemExit, Interrupt
          raise
        rescue TypeError => e
          puts e.backtrace.inspect
          puts "> Omited measure point: parsed nil"
        rescue Exception => e
          puts e.backtrace.inspect
          puts "> Omited measure point: parsed nil"
        end
      end
    }
    threads.each{ |thread| thread.join}
  end
  csv_file_A.close
  csv_file_B.close
  json_file_A.close
  json_file_B.close
  puts "> Finished fetching cpu metrics from mesos. Results written in #{measurements_dir}/mesos_cpu_[A;B].csv"
  puts "> Containers json written in #{measurements_dir}/mesos_containers_[A;B].json"
  # Compress mesos json files
  begin
    puts "> Compress #{measurements_dir}/mesos_containers_[A;B].json into #{measurements_dir}/mesos_containers.tar.gz"
    system ("cd #{measurements_dir} && tar -cvzf mesos_containers.tar.gz mesos_containers_A.json mesos_containers_B.json")
    puts "> Remove #{measurements_dir}/mesos_containters_[A;B].json"
    system ("rm #{measurements_dir}/mesos_containers_A.json #{measurements_dir}/mesos_containers_B.json")
  rescue SystemExit, Interrupt
    raise
  rescue Exception => e
    puts e.backtrace.inspect
    puts "Error while creating mesos_containers.tar.gz"
  end
  puts "-----------------------------------------------------------------------------------------------------------------------"
end

# backpressure_presence(duration, master, waiting_delay, measurements_dir)
# Specification:
#    - 'master' a string defining the master node hostname
#    - 'duration' an integer defining the duration (in seconds) of the measurement.
#    - 'waiting_delay' an integer defining the duration (in seconds) we wait for the topolgy to
#    run before measuring backpressure presence.
# Semantics:
#   Detect the presence of backpressure in the consume_and_count container of the topology
# Implementation:
#   We fetch, from heron-tracker, backpressure statistics in the form of time slices of one minute.
#   In those time slices, we return the first time slice exhibiting
#   backpressure time > 0. And use this as a proof that backpressure was detected
# Remark:
#   Backpressure presence is an indicator that the spout emission is too fast for the bolt computation capacity.
#   In other words that the throttling time is not long enough and we should re-run the experiment
#   with higher throttle value.
#   Given a word_size The tuning_throttle.rb script should provide an optimal throttle value to you.
def backpressure_presence(duration, master, waiting_delay, measurements_dir)
  puts "-------------------------------------------------------"
  puts "> Checking backpressure presence during the measurements"
  puts ">> Backpressure presence is an indicator that the spout emission is too fast for the bolt computation capacity."
  backpressure_log_file = File.new("#{measurements_dir}/backpresure.csv", 'w')
  execution_state = Curl::Easy.perform("http://#{master}:8888/topologies/executionstate?cluster=devcluster&environ=devel&topology=WordCountTopology")
  parsed_execution_state = JSON.parse(execution_state.body_str)
  puts JSON.pretty_generate(parsed_execution_state)
  submission_time = parsed_execution_state['result']['submission_time']
  backpressure_log_file.puts "mesure_id;time;backpresure"
  
  # Request the time spent in backpressure by the Counsume_and_count container
  backpressure = Curl::Easy.perform("http://#{master}:8888/topologies/metricsquery?cluster=devcluster&environ=devel&starttime=#{submission_time + waiting_delay}&query=DEFAULT%280%2C+TS%28__stmgr__%2C%2A%2C__time_spent_back_pressure_by_compid%2Fcontainer_1_Consume_and_count_1%29%29&endtime=#{submission_time + waiting_delay + duration}&topology=WordCountTopology")
  parsed_backpressure = JSON.parse(backpressure.body_str)
  mesure_id = 0
  parsed_backpressure['result']['timeline'].first['data'].each{ |data|
    time = data[0]
    backpresure = data[1]
    backpressure_log_file.puts "#{mesure_id};#{time};#{backpresure}"
    mesure_id += 1
  }

  
  backpressure_log_file.close
  puts "> Finished detecting backpressure presence"
  puts "------------------------------------------"
end

# Garbage_collection_stats(duration, master, waiting_delay, measurements_dir)
# Specification:
#    - 'master' a string defining the master node hostname
#    - 'duration' an integer defining the duration (in seconds) of the measurement.
#    - 'waiting_delay' an integer defining the duration (in seconds) we wait for the topolgy to
#    run before measuring garbage collection.
# Semantic:
#   fetch the time spent in garbage collection by the consume_and_count and emit_random_words JVM's.
# Implementation:
#   Fetch, from heron-tracker, garbage collection statistics in the form of time slices of one minute.
# Remark:
#   There are tuning parameters in heron_internal.yaml file aimed at reducing the gc time in heron instances.
def garbage_collection_stats(duration, master, waiting_delay, measurements_dir)
  puts "-------------------------------------------------------"
  puts "> Fetching garbage collection stats"
  emit_random_words_garbage_collection_file = File.new("#{measurements_dir}/emit_random_words_garbage_collection.json", 'w')
  consume_and_count_garbage_collection_file = File.new("#{measurements_dir}/consume_and_count_garbage_collection.json", 'w')
  
  # If ever an exception is raised we ignore the garbage collection stats
  begin
    execution_state = Curl::Easy.perform("http://#{master}:8888/topologies/executionstate?cluster=devcluster&environ=devel&topology=WordCountTopology")   
    parsed_execution_state = JSON.parse(execution_state.body_str)
    submission_time = parsed_execution_state['result']['submission_time']

    # Request the time spent in garbage collecion by the Counsume_and_count container
    consume_and_count_gc = Curl::Easy.perform("http://#{master}:8888/topologies/metricsquery?cluster=devcluster&environ=devel&starttime=#{submission_time + waiting_delay}&query=MAX%28RATE%28TS%28Consume_and_count%2C%2A%2C__jvm-gc-collection-time-ms%29%29%29&endtime=#{submission_time + waiting_delay + duration}&topology=WordCountTopology")

    parsed_consume_and_count_gc = JSON.parse(consume_and_count_gc.body_str)
    consume_and_count_garbage_collection_file.write(parsed_consume_and_count_gc)

    # Request the time spent in garbage collecion by the Emit_random_words container
    emit_random_words_gc = Curl::Easy.perform("http://#{master}:8888/topologies/metricsquery?cluster=devcluster&environ=devel&starttime=#{submission_time + waiting_delay}&query=MAX%28RATE%28TS%28Emit_random_words%2C%2A%2C__jvm-gc-collection-time-ms%29%29%29&endtime=#{submission_time + waiting_delay + duration}&topology=WordCountTopology")

    parsed_emit_random_words_gc = JSON.parse(emit_random_words_gc.body_str)
    emit_random_words_garbage_collection_file.write(parsed_emit_random_words_gc)
  # If exception if systemExit or Interrupt we re-raise it
  rescue SystemExit, Interrupt
    raise
  rescue Exception => e
    puts e.backtrace.inspect
    puts "> Error while fetching the Garbage collection stats from heron tracker"
    puts "> Ignoring garbage collection stats"
  end
  consume_and_count_garbage_collection_file.close
  emit_random_words_garbage_collection_file.close
  puts "> Finished fetching garbage collection stats"
  puts "> Results written in #{measurements_dir}/emit_random_words_garbage_collection.json"
  puts "> Results written in #{measurements_dir}/consume_and_count_garbage_collection.json"
  puts "----------------------------------------------------------------------------------"
end

# network_measure(duration, master, slaves)
# Semantics:
#    Use the nicstat program to gather network statistics on the slaves.
#    Parse the values returned by nicstat on a csv file for each slave node
#    The csv file is named after the heron instances running on the node.
def network_measure(duration, master, slaves, measurements_dir)
  puts "------------------------------------------------"
  puts "> Fetch network statistics from the slaves nodes"
  # Measurements intervals in seconds
  interval = 10
  results = {}
  Cute::TakTuk.start(slaves, :user => "root") do |tak|
    results = tak.exec!("nicstat #{interval} -p #{duration/interval}")
  end

  physical_plan = get_physical_plan(master, measurements_dir)
  results.each { |slave|
    instances = get_node_instances(physical_plan, slave[0])
    if instances.empty?
      instances.push("topology_master")
    end
    csv_file = File.new("#{measurements_dir}/network_#{instances[0]}.csv", 'w')
    csv_file.puts "Time:Int:rKB/s:wKB/s:rPk/s:wPk/s:rAvs:wAvs:%Util:Sat"
    csv_file.write("#{slave[1][:output]}")
    csv_file.close
  }
  puts "> Finished fetching network statistics from the slaves nodes"
  puts "> Results written in #{measurements_dir}/network[Heron_instance_name].csv"
  puts "------------------------------------------------------------------------------------------"
end

# throughput_measur(duration, master, slaves, measurements_dir)
# Semantics:
#    Parse throughput statistics from Heron-tracker REST API
def throughput_measure(duration, master, slaves, measurements_dir)
  puts("--------------------------------------------")
  puts("> Fetching the throughput metrics from heron")
  csv_file_emit = File.new("#{measurements_dir}/emit_count.csv", 'w')
  csv_file_execute = File.new("#{measurements_dir}/execute_count.csv", 'w')
  csv_file_emit.puts "spout_instance_id;emit_count_#{duration}s"
  csv_file_execute.puts "bolt_instance_id;execute_count_#{duration}s"

  emit_count = Curl::Easy.perform("http://#{master}:8888/topologies/metrics?component=Emit_random_words&cluster=devcluster&environ=devel&topology=WordCountTopology&metricname=__emit-count%2Fdefault&interval=#{duration}")
  parsed_emit_count = JSON.parse(emit_count.body_str)
  puts JSON.pretty_generate(parsed_emit_count)
  if parsed_emit_count['result']['metrics'].empty?
    puts ">> No measurements yet! Try to re-run the program with increased measurement duration (-l option)."
  else
    parsed_emit_count['result']['metrics']['__emit-count/default'].each { |spout|
      csv_file_emit.write("#{spout[0]}" + ";" + "#{spout[1]}" + "\n")
    }
  end

  execute_count = Curl::Easy.perform("http://#{master}:8888/topologies/metrics?component=Consume_and_count&cluster=devcluster&environ=devel&topology=WordCountTopology&metricname=__execute-count%2Fdefault&interval=#{duration}")
  parsed_execute_count = JSON.parse(execute_count.body_str)
  puts JSON.pretty_generate(parsed_execute_count)
  if parsed_emit_count['result']['metrics'].empty?
    puts ">> No measurements yet! Try to re-run the program with increased measurement duration (-l option)."
  else
    parsed_execute_count['result']['metrics']['__execute-count/default'].each { |bolt|
      csv_file_execute.write("#{bolt[0]}" + ";" + "#{bolt[1]}" + "\n")
    }
  end
  csv_file_execute.close
  csv_file_emit.close
  puts "> Finished fetching throughput metrics from heron."
  puts "> Results written in #{measurements_dir}/[emit;execute]_count.csv"
  puts("----------------------------------------------------------------------------------")
end


# fail_stats(duration, master, slaves, measurements_dir)
# Semantics:
#    Parse fail statistics from Heron-tracker REST API
def failcount_stats(duration, master, slaves, measurements_dir)
  puts("--------------------------------------------")
  puts("> Fetching the fail-count metrics from heron")
  csv_file_fail = File.new("#{measurements_dir}/fail_count.csv", 'w')
  csv_file_fail.puts "container;fail_count"
  
  fail_count = Curl::Easy.perform("http://#{master}:8888/topologies/metrics?component=Consume_and_count&cluster=devcluster&environ=devel&topology=WordCountTopology&metricname=__fail-count%2Fdefault&interval=#{duration}")
  parsed_fail_count = JSON.parse(fail_count.body_str)
  puts JSON.pretty_generate(parsed_fail_count)
=begin
"result": {
    "metrics": {
      "__fail-count/default": {
        "container_1_Consume_and_count_1": "0.000000"
      }
    },
=end
  
  if parsed_fail_count['result']['metrics'].empty?
    puts ">> No measurements yet! Try to re-run the program with increased measurement duration (-l option)."
  else
    parsed_fail_count['result']['metrics']['__fail-count/default'].each { |container|
      csv_file_fail.write("#{container[0]}" + ";" + "#{container[1]}" + "\n")
    }
  end

  csv_file_fail.close
  puts "> Finished fetching fail_count metrics from heron."
  puts "> Results written in #{measurements_dir}/fail_count.csv"
  puts("----------------------------------------------------------------------------------")
end

# latency_stats(duration, master, slaves, measurements_dir)
# Semantics:
#    Parse fail statistics from Heron-tracker REST API
def latency_stats(duration, master, slaves, measurements_dir)
  puts("--------------------------------------------")
  puts("> Fetching the latency metrics from heron")
  csv_file_execute_latency = File.new("#{measurements_dir}/execute_latency.csv", 'w')
  csv_file_execute_latency.puts "container;execute_latency"

  csv_file_process_latency = File.new("#{measurements_dir}/process_latency.csv", 'w')
  csv_file_process_latency.puts "container;process_latency"

  
  process_latency = Curl::Easy.perform("http://#{master}:8888/topologies/metrics?component=Consume_and_count&cluster=devcluster&environ=devel&topology=WordCountTopology&metricname=__process-latency%2Fdefault&interval=#{duration}")

  parsed_process_latency = JSON.parse(process_latency.body_str)
  puts JSON.pretty_generate(parsed_process_latency)
  
  execute_latency = Curl::Easy.perform("http://#{master}:8888/topologies/metrics?component=Consume_and_count&cluster=devcluster&environ=devel&topology=WordCountTopology&metricname=__execute-latency%2Fdefault&interval=#{duration}")
  
  parsed_execute_latency = JSON.parse(execute_latency.body_str)
  puts JSON.pretty_generate(parsed_execute_latency)

  if parsed_execute_latency['result']['metrics'].empty?
    puts ">> No measurements yet! Try to re-run the program with increased measurement duration (-l option)."
  else
    parsed_execute_latency['result']['metrics']['__execute-latency/default'].each { |container|
      csv_file_execute_latency.write("#{container[0]}" + ";" + "#{container[1]}" + "\n")
    }
  end

  csv_file_execute_latency.close

  if parsed_process_latency['result']['metrics'].empty?
    puts ">> No measurements yet! Try to re-run the program with increased measurement duration (-l option)."
  else
    parsed_process_latency['result']['metrics']['__process-latency/default'].each { |container|
      csv_file_process_latency.write("#{container[0]}" + ";" + "#{container[1]}" + "\n")
    }
  end

  csv_file_process_latency.close
  
  puts "> Finished fetching latency metrics from heron."
  puts "> Results written in #{measurements_dir}/[process,execute]_latency.csv"
  puts("----------------------------------------------------------------------------------")
end


def submit_topology_and_measure(word_size, throttle, padding, duration, master, slaves, cpu_opt, backpressure_opt, redeploy_opt, resubmit_opt, throughput_opt, network_opt, multithread_opt, measurements_dir)
  puts "Slaves : #{slaves}"
  puts "Master node : #{master}"

  # submit the wordcount topology
  if resubmit_opt.eql? "true"
    submit_topology(word_size, throttle, padding, master, measurements_dir, multithread_opt)
  end

  # Wait some time for the topology to run after submition
  waiting_delay = 120
  puts ">> waiting #{waiting_delay} seconds for the topology to run on the cluster"
  sleep (waiting_delay)
  
  threads = []
  if cpu_opt.eql? "true"
    threads << Thread.new do
      cpu_measure(duration, master, slaves, measurements_dir)
    end
  end
  
  if network_opt.eql? "true"    
    threads << Thread.new do
      network_measure(duration, master, slaves, measurements_dir)
    end
  end
  threads.each{ |thread| thread.join}
  
  if (throughput_opt.eql? "true")
    # If cpu mesure are performed then there is no need to wait before launching emit_cout and execute_count
    # measurements.
    if (cpu_opt.eql? "false")
      puts "sleeping for #{duration}s"
      sleep(duration)
    end
    throughput_measure(duration, master, slaves, measurements_dir)
  end
  
  if backpressure_opt.eql? "true"
    backpressure_presence(duration, master, waiting_delay, measurements_dir)
  end

  #failcount_stats(duration, master, slaves, measurements_dir)
  #latency_stats(duration, master, slaves, measurements_dir)
  garbage_collection_stats(duration, master, waiting_delay, measurements_dir)
end


def submit_cpu_program_and_measure(duration, master, slaves, measurements_dir, test_id)
  # Configure the cpu program so it can find the mesos-master
  Net::SSH.start(master, "root") do |ssh|
    ssh.exec!("sed -i 's/master_node/#{master}/g' /root/Heron_on_g5k/aurora_programs/cpu/cpu.aurora")
    ssh.exec!("sed -i 's/master_node/#{master}/g' /root/Heron_on_g5k/aurora_programs/cpu_throttle/cpu.aurora")
  end
  Net::SSH.start(master, "root") do |ssh|
    if test_id == 0
      ssh.exec!("chmod +x /root/Heron_on_g5k/aurora_programs/cpu/cpu_run.sh && ./Heron_on_g5k/aurora_programs/cpu/cpu_run.sh")
    elsif test_id == 1
      ssh.exec!("chmod +x /root/Heron_on_g5k/aurora_programs/cpu_throttle/cpu_run.sh && ./Heron_on_g5k/aurora_programs/cpu_throttle/cpu_run.sh")
    end
  end

  puts("configured and ran the cpu test program")
  puts("wait 20 so the program is running on slaves")
  sleep(20)

  csv_file_A = File.new("#{measurements_dir}/mesos_cpu_A.csv", 'w')
  csv_file_B = File.new("#{measurements_dir}/mesos_cpu_B.csv", 'w')
  csv_file_A.puts "mesure_id;slave_id;executor_id;cpus_limit;timestamp_A;cpus_system_time_secs_A;cpus_user_time_secs_A;cpus_throttled_time_secs_A;run_id"
  csv_file_B.puts "mesure_id;slave_id;executor_id;cpus_limit;timestamp_B;cpus_system_time_secs_B;cpus_user_time_secs_B;cpus_throttled_time_secs_B;run_id"

  measurment_interval = 1
  1.upto(duration/measurment_interval) do |n|
    # Fetch the measurments
    slaves.each{ |node|
      a = Curl::Easy.perform("http://#{node}:5051/containers")
      parsed_A = JSON.parse(a.body_str)
      puts "A"
      puts JSON.pretty_generate(parsed_A)
      sleep(measurment_interval)
      b = Curl::Easy.perform("http://#{node}:5051/containers")
      parsed_B = JSON.parse(b.body_str)
      puts "B"
      puts JSON.pretty_generate(parsed_B)
      begin
        if parsed_A.nil? || parsed_B.nil? || parsed_A.empty? || parsed_B.empty? || parsed_A.detect{|x| x.empty?} || parsed_B.detect{|x| x.empty?}
            puts "> Omited measure point: parsed nil"
        else
        parsed_A.each { |container|
          csv_file_A.write ("#{n}" + ";" + "#{node}" + ";" + "#{container['executor_id']}" + ";" + "#{container['statistics']['cpus_limit']}" + ";" + "#{container['statistics']['timestamp']}" + ";" + "#{container['statistics']['cpus_system_time_secs']}" + ";" + "#{container['statistics']['cpus_user_time_secs']}" + ";" + "#{container['statistics']['cpus_throttled_time_secs']}" + "\n")
        }
        parsed_B.each { |container|
          csv_file_B.write ("#{n}" + ";" + "#{node}" + ";" + "#{container['executor_id']}" + ";" + "#{container['statistics']['cpus_limit']}" + ";" + "#{container['statistics']['timestamp']}" + ";" + "#{container['statistics']['cpus_system_time_secs']}" + ";" + "#{container['statistics']['cpus_user_time_secs']}" + ";" + "#{container['statistics']['cpus_throttled_time_secs']}" + "\n")
        }
        end
      rescue TypeError => e
          puts e.backtrace.inspect
          puts "> Omited measure point: parsed nil"
      end
    }
  end
  csv_file_A.close
  csv_file_B.close
end

upload_Heron_on_g5k(master)

if cpu_test_opt.eql? "true"
  submit_cpu_program_and_measure(length, master, slaves, cpu_experiment_0_dir, 0)
  submit_cpu_program_and_measure(length, master, slaves, cpu_experiment_1_dir, 1)
else
  if measure_opt.eql? "true"
    if monothread_opt.eql? "true"
      multithread = "false"
      submit_topology_and_measure(word_size, throttle, padding, length, master, slaves, cpu_opt, backpressure_opt, redeploy_opt, resubmit_opt, throughput_opt, network_opt, multithread , monothread_dir)
    end
    # then run the multithread topology
    if multithread_opt.eql? "true"
      submit_topology_and_measure(word_size, throttle, padding, length, master, slaves, cpu_opt, backpressure_opt, redeploy_opt, resubmit_opt, throughput_opt, network_opt, multithread_opt, multithread_dir)
    end
  end
end




puts "Slaves : #{slaves}"
puts "Master node : #{master}"
