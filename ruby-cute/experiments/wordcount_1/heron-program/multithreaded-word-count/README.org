* Usage
If you compiled Heron from source, add Heron-api.jar to your local maven repository
#+BEGIN_SRC Bash
mvn dependency:purge-local-repository -DmanualInclude="org.apache.heron:heron-api-built-from-sources"
mvn install:install-file -Dfile=/root/incubator-heron/bazel-genfiles/heron/api/src/java/heron-api.jar -DgroupId=org.apache.heron -DartifactId=heron-api-built-from-sources -Dversion=0.17.8 -Dpackaging=jar
#+END_SRC

Compile the project
#+BEGIN_SRC Bash
mvn package
#+END_SRC

Submit the monothreaded topology
#+BEGIN_SRC Bash
sh ./deploy.sh word_size throttle_duration paddingPercentage
#+END_SRC

Kill topologies 
#+BEGIN_SRC Bash
sh ./kill.sh
#+END_SRC

Clean
#+BEGIN_SRC Bash
mvn clean
#+END_SRC

* deploy.sh and deploy_multithreaded.sh
This script deploy a WordCountTopology using heron cli.
usage sh ./deploy.sh word_size throttle_duration paddingPercentage
The WordCountProgram takes three arguments:
- Word_size: an integer describing the size of the words emitted by the spouts of the toplogy in nb of characters.
- Throttle: an integer describing the waiting time in microsecond between two tuples emissions from the spouts.
- Padding: an integer describing the cpu padding percentage for mesos containers.
By default Heron allocate one cpu per heron instance + 0.25 cpu for the stream_manager.
So if a container contain a single Heron instance (spout, bolt) it's size will be 1.25 cpu.
By setting a padding of 100 we increase the size of such a container to 2.25 cpu.
By setting a padding of 200 we get 3.25 cpu.
