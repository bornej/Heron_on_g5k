package org.apache.heron;
import org.apache.commons.cli.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.time.Duration;
import java.util.concurrent.*;
import org.apache.heron.api.Config;
import org.apache.heron.api.HeronSubmitter;
import org.apache.heron.api.bolt.BaseRichBolt;
import org.apache.heron.api.bolt.OutputCollector;
import org.apache.heron.api.exception.AlreadyAliveException;
import org.apache.heron.api.exception.InvalidTopologyException;
import org.apache.heron.api.spout.BaseRichSpout;
import org.apache.heron.api.spout.SpoutOutputCollector;
import org.apache.heron.api.topology.OutputFieldsDeclarer;
import org.apache.heron.api.topology.TopologyBuilder;
import org.apache.heron.api.topology.TopologyContext;
import org.apache.heron.api.tuple.Fields;
import org.apache.heron.api.tuple.Tuple;
import org.apache.heron.api.tuple.Values;
import org.apache.heron.common.basics.ByteAmount;
import org.apache.heron.common.basics.SysUtils;

import org.apache.heron.streamlet.impl.KryoSerializer;


/**
 * This is a topology that does simple word counts.
 * <p>
 * In this topology,
 * 1. the spout task generate a set of random words during initial "open" method.
 * (~128k words, 20 chars per word)
 * 2. During every "nextTuple" call, each spout simply picks a word at random and emits it
 * 3. Spouts use a fields grouping for their output, and each spout could send tuples to
 * every other bolt in the topology
 * 4. Bolts maintain an in-memory map, which is keyed by the word emitted by the spouts,
 * and updates the count when it receives a tuple.
 */
public final class WordCountTopology {
    private WordCountTopology() {
    }

    // Utils class to generate random String at given length
    public static class RandomString {
        private final char[] symbols;

        private final Random random = new Random();

        private final char[] buf;

        public RandomString(int length) {
            // Construct the symbol set
            StringBuilder tmp = new StringBuilder();
            for (char ch = '0'; ch <= '9'; ++ch) {
                tmp.append(ch);
            }

            for (char ch = 'a'; ch <= 'z'; ++ch) {
                tmp.append(ch);
            }

            symbols = tmp.toString().toCharArray();
            if (length < 1) {
                throw new IllegalArgumentException("length < 1: " + length);
            }

            buf = new char[length];
        }

        public String nextString() {
            for (int idx = 0; idx < buf.length; ++idx) {
                buf[idx] = symbols[random.nextInt(symbols.length)];
            }

            return new String(buf);
        }
    }

    /**
     * A spout that emits a random word
     */
    public static class WordSpout extends BaseRichSpout {
        private static final long serialVersionUID = 4322775001819135036L;

        private static final int ARRAY_LENGTH = 128 * 1024;
        private int WORD_LENGTH = 20;
        // ThrottleDuration in microseconds
        private long throttleDuration = (long)850;
        private final String[] words = new String[ARRAY_LENGTH];

        private final Random rnd = new Random(31);

        private SpoutOutputCollector collector;
        public WordSpout(int word_length, long throttle){
            this.WORD_LENGTH = word_length;
            this.throttleDuration = throttle;
        }

	    @Override
	    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
            outputFieldsDeclarer.declare(new Fields("word"));
	    }

	    @Override
	    @SuppressWarnings("rawtypes")
	    public void open(Map map, TopologyContext topologyContext,
                         SpoutOutputCollector spoutOutputCollector) {
            RandomString randomString = new RandomString(WORD_LENGTH);
            for (int i = 0; i < ARRAY_LENGTH; i++) {
                words[i] = randomString.nextString();
            }

            collector = spoutOutputCollector;
	    }

	    @Override
	    public void nextTuple() {
            int nextInt = rnd.nextInt(ARRAY_LENGTH);
            if (throttleDuration!= 0) {
                /*
                  Implement busywaiting
                  source:
                  http://www.rationaljava.com/2015/10/measuring-microsecond-in-java.html
                */
                long waitUntil = System.nanoTime() + (throttleDuration * 1_000);
                while(waitUntil > System.nanoTime()){
                    ;
                }
            }
            collector.emit(new Values(words[nextInt]));
	    }
    }


    /**
     * A bolt that counts the words that it receives
     */
    public static class ConsumerBolt extends BaseRichBolt {
        private static final long serialVersionUID = -5470591933906954522L;

        private OutputCollector collector;
        private Map<String, Integer> countMap;

        @SuppressWarnings("rawtypes")
        public void prepare(Map map, TopologyContext topologyContext, OutputCollector outputCollector) {
            collector = outputCollector;
            countMap = new HashMap<String, Integer>();
        }

	    @Override
	    public void execute(Tuple tuple) {
            String key = tuple.getString(0);
            if (countMap.get(key) == null) {
                countMap.put(key, 1);
            } else {
                Integer val = countMap.get(key);
                countMap.put(key, ++val);
            }
	    }

	    @Override
	    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
	    }
    }

    /**
     * Main method
     */
    public static void main(String[] args) throws AlreadyAliveException, InvalidTopologyException {
        if (args.length < 1) {
            throw new RuntimeException("Specify topology name");
        }

        Options options = new Options();
        Option wordsize = new Option("s", "wordsize", true, "emited words size (nb of char)");
        wordsize.setRequired(true);
        options.addOption(wordsize);
        Option throttle = new Option("t", "throttle", true, "throttle duration in ms");
        throttle.setRequired(true);
        options.addOption(throttle);
        Option padding = new Option("p", "padding", true, "Container padding: Nb cpu of a container = (paddingPercentage*nb_Heron_instances)/100 + nb_Heron_instances");
        padding.setRequired(true);
        options.addOption(padding);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        int word_length = 20;
        long throttleDuration =(long)0;
        int paddingPercentage = 100;

        try {
            cmd = parser.parse(options, args);
            word_length = Integer.parseInt(cmd.getOptionValue("wordsize"));
            throttleDuration = (long)Double.parseDouble(cmd.getOptionValue("throttle"));
            paddingPercentage = Integer.parseInt(cmd.getOptionValue("padding"));
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("utility-name", options);

            System.exit(1);
        }
        int parallelism = 1;
        TopologyBuilder builder = new TopologyBuilder();
        /* Add a single word spout */
        builder.setSpout("Emit_random_words", new WordSpout(word_length,(long) throttleDuration), parallelism);
        /* Add a single monothreaded bolt to the topology */
        builder.setBolt("Consume_and_count", new ConsumerBolt(), parallelism)
            /* Configure the bolt so it processes the stream emerging from the WordSpout */
            .fieldsGrouping("Emit_random_words", new Fields("word"));
        /* Create a new configuration so we can specify the resources for the spout, bolts, containers */
        Config conf = new Config();
        conf.setSerializationClassName(KryoSerializer.class.getName());

        /* The following configuration work with ResourcesCompliantRoundRobin Algortithm
           see https://github.com/apache/incubator-heron/blob/58078e7c349af4c420713a9aa37fc13670291f17/heron/packing/src/java/org/apache/heron/packing/roundrobin/ResourceCompliantRRPacking.java */

        /* Force the packing plan to use two containers */
        conf.setNumStmgrs(2);

        /*
          Force the containers to have more cpus by setting padding percentage value
          - The CPU required for one instance is calculated as the default CPU value for one instance.
          (Which is 1)
          - The CPU required for a container is calculated as:
          (CPU for instances in container) + (paddingPercentage * CPU for instances in container)
        */
        conf.setContainerPaddingPercentage(paddingPercentage);
        HeronSubmitter.submitTopology(args[0], conf, builder.createTopology());
    }
}
