# This script deploy a WordCountTopology using heron cli.
# usage sh ./deploy.sh word_size throttle_duration paddingPercentage
# The WordCountProgram takes three arguments: word_size, throttleDuration and paddingPercentage
# - Word_size: an integer describing the size of the words emitted by the spouts of the toplogy in nb of characters.
#
# - Throttle: an integer describing the waiting time in microsecond between two tuples emissions from the spouts.
#   
# - Padding: an integer describing the cpu padding percentage for mesos containers.
#   By default Heron allocate one cpu per heron instance + 0.25 cpu for the stream_manager.
#   So if a container contain a single Heron instance (spout, bolt) it's size will be 1.25 cpu.
#   By setting a padding of 100 we increase the size of such a container to 2.25 cpu.
#   By setting a padding of 200 we get 3.25 cpu.


# When executing from ssh the mvn package command can't access the environment variable JAVA_HOME even if we set it on .bashrc
# so we re-set it explicitely here.
export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-amd64; cd /root/Heron_on_g5k/ruby-cute/experiments/wordcount_1/heron-program/multithreaded-word-count/; mvn package
# Deploying WordCountTopolgy on the aurora cluster "devcluster" with role "root" in environment "devel"
heron submit devcluster/root/devel ./target/multithreaded-word-count-1.0-SNAPSHOT-jar-with-dependencies.jar org.apache.heron.WordCountTopology WordCountTopology -s $1 -t $2 -p $3

