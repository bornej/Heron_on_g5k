#!/usr/bin/env Rscript
##  Etude statistique de la consomation cpu d'un programme de WordCount Heron.
library(ggplot2)
library(plyr)
library(reshape2)

#if (FALSE) {
args = commandArgs(trailingOnly=TRUE)
if (length(args)!=1) {
    stop("One argument required. usage: Rscript network.r measurement_folder_name", call.=FALSE)
}
#}


#if (FALSE) {
#####################################################
#### Nicstat network statistics #####################

##base_path= "./Experiments_1"
##run_path="s1000_t0_p200_l600_combined_nomem_rennes_paravance/3"
run_path=args[1]

## monothread
consume_path_monothread = sprintf("%s/monothread/network_container_1_Consume_and_count_1.csv", run_path)
emit_path_monothread = sprintf("%s/monothread/network_container_2_Emit_random_words_2.csv", run_path)
# multithread
consume_path_multithread = sprintf("%s/multithread/network_container_1_Consume_and_count_1.csv", run_path)
emit_path_multithread = sprintf("%s/multithread/network_container_2_Emit_random_words_2.csv", run_path)

# extraction des données consume
consume_monothread <- data.frame(
    read.csv2(file=consume_path_monothread, sep=':', dec='.')
)

consume_multithread <- data.frame(
    read.csv2(file=consume_path_multithread, sep=':', dec='.')
)

## ajout d'une collone type -> monothread
consume_monothread$type <- with(consume_monothread, "monothread")
consume_multithread$type <- with(consume_multithread, "multithread")

## filter eth0

consume_monothread_eth0 <- subset(consume_monothread, Int=="eth0")
consume_multithread_eth0 <- subset(consume_multithread, Int=="eth0")

## ajout d'une collone measurement_point
consume_monothread_eth0$measurement_point <- seq.int(nrow(consume_monothread_eth0))
consume_multithread_eth0$measurement_point <- seq.int(nrow(consume_multithread_eth0))

## Fusion des datasets consume_monothread et consume_multithread.
merged_consume_mono_multi <- merge(consume_monothread_eth0, consume_multithread_eth0, all =TRUE)

# Ajout d'une collone component
merged_consume_mono_multi$component <- with(merged_consume_mono_multi, "Consume_and_count")

#a_eth0 <- subset(a, Int=="eth0")

# extraction des données emit
emit_monothread <- data.frame(
    read.csv2(file=emit_path_monothread, sep=':', dec='.')
)

emit_multithread <- data.frame(
    read.csv2(file=emit_path_multithread, sep=':', dec='.')
)

# ajout d'une collone type -> monothread
emit_monothread$type <- with(emit_monothread, "monothread")
emit_multithread$type <- with(emit_multithread, "multithread")


emit_monothread_eth0 <- subset(emit_monothread, Int=="eth0")
emit_multithread_eth0 <- subset(emit_multithread, Int=="eth0")

# ajout d'une collone measurement_point
emit_monothread_eth0$measurement_point <- seq.int(nrow(emit_monothread_eth0))
emit_multithread_eth0$measurement_point <- seq.int(nrow(emit_multithread_eth0))

# Fusion des datasets emit_monothread et emit_multithread.
merged_emit_mono_multi <- merge(emit_monothread_eth0, emit_multithread_eth0, all =TRUE)

# Ajout d'une collone component
merged_emit_mono_multi$component <- with(merged_emit_mono_multi, "Emit_random_words")

# Merge emit and consume stats
nicstats <- merge(merged_emit_mono_multi, merged_consume_mono_multi, all =TRUE)

#nicstats

#b_eth0 <- subset(b, Int=="eth0")

# Temps moyen, écart type, variance
rKBps_stats<-ddply(
    subset(nicstats, (component=="Consume_and_count")&(Int=="eth0")),
    c("Int", "type"),
    summarise,
    N=length(Time),
    mean=mean(rKB.s),
    sum=sum(rKB.s),
    sd=sd(rKB.s),
    se=1.96*(sd/sqrt(N))
)
#rKBps_stats

# Temps moyen, écart type, variance
wKBps_stats<-ddply(
    subset(nicstats, (component=="Emit_random_words")&(Int=="eth0")),
    c("Int", "type"),
    summarise,
    N=length(Time),
    mean=mean(wKB.s),
    sum=sum(wKB.s),
    sd=sd(wKB.s),
    se=1.96*(sd/sqrt(N))
)
#wKBps_stats

emit_out_mean_path = sprintf("%s/network_container_1_Emit_random_words_1_means.png", run_path)

emit_means <- ggplot(data=wKBps_stats, aes(x=type, y=mean, color = as.factor(type), label = mean))+
    geom_bar(position="dodge", stat="identity") +
    ylim(0, 300000) +
    geom_text(size = 3, position = position_stack(vjust = 1.05)) +
    labs(title = "Emit_random_words_node mean wKB/s", x = "type", y = "wKB/s")
ggsave(emit_out_mean_path, plot = emit_means, scale = 1)
#show (emit_means)

consume_out_mean_path = sprintf("%s/network_container_1_Consume_and_count_1_means.png", run_path)
consume_means <- ggplot(data=rKBps_stats, aes(x=type, y=mean, color = as.factor(type), label = mean))+
    geom_bar(position="dodge", stat="identity") +
    ylim(0, 300000) +
    geom_text(size = 3, position = position_stack(vjust = 1.05)) +
    labs(title = "Consume_and_count_node mean rKB/s", x = "type", y = "rKB/s")
ggsave(consume_out_mean_path, plot = consume_means, scale = 1)
#show (consume_means)

consume_out_path = sprintf("%s/network_container_1_Consume_and_count_1.png", run_path)
# tracé des résultats obtenu
p1 <- ggplot(data = subset(nicstats, component =="Consume_and_count"), aes(x = measurement_point, y = rKB.s, color = as.factor(type))) +
    geom_line() +
    ylim(0, 300000) +
    labs(title = "Consume_and_count", x = "Measurement_point", y = "rKB/s") +
                                        #+ facet_wrap(~Int)+ labs( fill = "Interface")
    theme_minimal()
ggsave(consume_out_path, plot = p1, scale = 1)
#show(p1)

emit_out_path = sprintf("%s/network_container_1_Emit_random_words_1.png", run_path)
p2 <- ggplot(data = subset(nicstats, component =="Emit_random_words"), aes(x = measurement_point, y = wKB.s, color = as.factor(type))) +
    geom_line() +
    ylim(0, 300000) +
    labs(title = "Emit_random_words", x = "Measurement_point", y = "wKB/s") +
                                        #+ facet_wrap(~Int)+ labs( fill = "Interface")
    theme_minimal()
ggsave(emit_out_path, plot = p2, scale = 1)
#show(p2)

#######################################################
#### Mesos cpu usage  #################################

A_path_monothread = sprintf("%s/monothread/mesos_cpu_A.csv", run_path)
B_path_monothread = sprintf("%s/monothread/mesos_cpu_B.csv", run_path)

A_path_multithread = sprintf("%s/multithread/mesos_cpu_A.csv", run_path)
B_path_multithread = sprintf("%s/multithread/mesos_cpu_B.csv", run_path)
# extraction des données
a_monothread <- data.frame(
    read.csv2(file=A_path_monothread,
              sep=';',
              dec='.')
)

b_monothread <- data.frame(
    read.csv2(file=B_path_monothread,
              sep=';',
              dec='.')
)

a_multithread <- data.frame(
    read.csv2(file=A_path_multithread,
              sep=';',
              dec='.')
)

b_multithread <- data.frame(
    read.csv2(file=B_path_multithread,
              sep=';',
              dec='.')
)

# --------------------------------------------------------------
# Fusion des datasets a et b correspondant aux timestamps A et B.
# Monothread
merged_monothread <- merge(a_monothread,b_monothread, by = c("mesure_id", "slave_id", "heron_instances", "executor_id","cpus_limit"))

#merged_monothread[1:10,]
# Multithread
merged_multithread <- merge(a_multithread,b_multithread, by = c("mesure_id", "slave_id", "heron_instances", "executor_id","cpus_limit"))


# Fetch the containers cpus limit from data
cpus_limit = merged_monothread['cpus_limit'][1,1]
# --------------------------------------------------------------
# Ajout d'une collone calculant la consomation totale cpu pendant l'intervalle de temps [A,B]
# cpu_consumption = [(cpus_user_time_secs_B - cpus_user_time_secs_A) + (cpus_system_time_secs_B - cpus_system_time_secs_A)] / (timestamp_B - timestamp_A)
# Monothread:

merged_monothread$time_interval <- with(merged_monothread, (timestamp_B - timestamp_A))
merged_monothread$cpu_consumption <- with(merged_monothread, ((cpus_user_time_secs_B - cpus_user_time_secs_A) + (cpus_system_time_secs_B - cpus_system_time_secs_A)) / time_interval)
merged_monothread$cpu_overload <- with(merged_monothread, (cpus_throttled_time_secs_B - cpus_throttled_time_secs_A) /time_interval)

merged_monothread$type <- with(merged_monothread, "monothread")
# Multithread:

merged_multithread$time_interval <- with(merged_multithread, (timestamp_B - timestamp_A))
merged_multithread$cpu_consumption <- with(merged_multithread, ((cpus_user_time_secs_B - cpus_user_time_secs_A) + (cpus_system_time_secs_B - cpus_system_time_secs_A)) / time_interval)
merged_multithread$cpu_overload <- with(merged_multithread, (cpus_throttled_time_secs_B - cpus_throttled_time_secs_A)/time_interval)
merged_multithread$type <- with(merged_multithread, "multithread")

#merged_multithread[1:10,]

cpu_stats <- merge(merged_monothread, merged_multithread, all=TRUE)
#cpu_stats[1:13,]
out_path = sprintf("%s/mesos_cpu.png", run_path)
# tracé des résultats obtenu
p1 <- ggplot(data= cpu_stats, mapping = aes(x = mesure_id, y = cpu_consumption, color = as.factor(type))) +
    geom_line() +
    geom_hline(yintercept=cpus_limit, color = "red") +
    labs(
        colour = "heron instances",
        x = "mesure id",
        y = "cpu consumption"
    ) + facet_wrap(~heron_instances) +
        labs(
        fill = "Slave"
        ) +
    theme_minimal()
ggsave(out_path, plot = p1, scale = 1)

out_path_2 = sprintf("%s/mesos_cpu_throttled.png", run_path)
p2 <- ggplot(data= cpu_stats, mapping = aes(x = mesure_id, y = cpu_overload, color = as.factor(type))) +
    geom_line() +
    labs(
        colour = "Topology type",
        x = "mesure id",
        y = "%cpu overload"
    ) + facet_wrap(~heron_instances) +
        labs(
        fill = "Slave"
        ) +
    ylim(0, 8) +
    theme_minimal()
ggsave(out_path_2, plot = p2, scale = 1)

#show(p1)
#show(p2)

################################### !! for the if(FALSE){
#}
###################################

backpresure_path_monothread = sprintf("%s/monothread/backpresure.csv", run_path)
backpresure_path_multithread = sprintf("%s/multithread/backpresure.csv", run_path)

# extraction des données consume
backpresure_monothread <- data.frame(
    read.csv2(file=backpresure_path_monothread, sep=';', dec='.')
)

backpresure_monothread$type <- with(backpresure_monothread, "monothread")

backpresure_multithread <- data.frame(
    read.csv2(file=backpresure_path_multithread, sep=';', dec='.')
)

backpresure_multithread$type <- with(backpresure_multithread, "multithread")

merged_backpresure <- merge(backpresure_monothread, backpresure_multithread, all =TRUE)

backpresure_out_path = sprintf("%s/backpresure.png", run_path)
# tracé des résultats obtenu
backpresure_plot <- ggplot(data= merged_backpresure, mapping = aes(x = mesure_id, y = backpresure, fill = as.factor(type))) +
    geom_bar(position="dodge", stat="identity", color="black") +
    ylim(0, 60000) +
    labs(colour = "Topology version", x = "mesure id", y = "Time spent in backpresure (ms)") +
    labs(fill = "Bolt version") +
    theme_minimal()
ggsave(backpresure_out_path, plot = backpresure_plot, scale = 1)
#show(backpresure_plot)
