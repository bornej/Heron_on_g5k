#!/usr/bin/ruby -w
# This script fixes a trailing ] that was introduced by mistake in the backpresure.csv files headers.
# Iterate over all experiments and runs folders in base_dir and replace all the backpresure.csv headers.
require "csv"
require "fileutils"
base_dir = '../mesures/backpresure_v1/'
Dir.foreach(base_dir) do |experiment_dir|
  # Ignore . and ..
  next if experiment_dir == '.' or experiment_dir == '..'
  # if the current file experiment_dir is indeed a directory
  if File.directory?("#{base_dir}/#{experiment_dir}")
    # Create the result directory "merged_runs"
    merged_dir = "#{base_dir}/#{experiment_dir}/merged_runs"
    system("rm -rf #{merged_dir}")
    system("mkdir -p #{merged_dir}")
    # Fetch word_size and padding values from the experiment directory name
    word_size = experiment_dir[/s(.*?)_/,1]
    padding = experiment_dir[/p(.*?)_/,1]
    length = experiment_dir[/l(.*?)_/,1]
    # Exploring the experiment 0/ 1/ ... runs folders
    Dir.foreach("#{base_dir}/#{experiment_dir}") do |run_dir|
      next if run_dir == '.' or run_dir == '..' or run_dir == "merged_runs" or !File.directory?("#{base_dir}/#{experiment_dir}/#{run_dir}")
      run_id = run_dir
      # Exploring the current run subfolders corresponding to a topology version measurement (monothread/ and multithread/).
      Dir.foreach("#{base_dir}/#{experiment_dir}/#{run_dir}") do |topology_version_dir|
        # ignore . and ..
        next if run_dir == '.' or run_dir == '..' or !File.directory?("#{base_dir}/#{experiment_dir}/#{run_dir}/#{topology_version_dir}")
        # look for backpresure.csv
        Dir.glob("#{base_dir}/#{experiment_dir}/#{run_dir}/#{topology_version_dir}/backpresure.csv").each { |file|
          # post-processing backpressure.csv: remove trailing } in the headers
          backpresure = File.readlines(file)
          # Pu thte headers in head and the rest in tail
          head, *tail = backpresure
          File.delete(file)
          backpresure_csv = File.new("#{base_dir}/#{experiment_dir}/#{run_dir}/#{topology_version_dir}/backpresure.csv", "w")
          # Rewrite the headers (corrected)
          backpresure_csv.puts "mesure_id;time;backpresure"
          # Rewrite the data
          tail.each do |row|
            backpresure_csv << row
          end
          backpresure_csv.close
        }
      end
    end
  end
end
