#!/usr/bin/env Rscript
##  Etude statistique de la consomation cpu d'un programme de WordCount Heron.
library(ggplot2)
library(plyr)
library(reshape2)

#if (FALSE) {
args = commandArgs(trailingOnly=TRUE)
if (length(args)!=1) {
    stop("One argument required. usage: ./cpu_mesos.r [run_id] \n ## We asume the runs of cpu_consumption and cpu_overload measurements are located in ../mesures/cpu_test_[g5k_site]_[cluster] folder \n ## Each run of the experiment has it own folder 0/ 1/ \n Please select the run_id you wish to process Example: ./cpu_mesos.r 1", call.=FALSE)
}
#}

##expe_path="cpu_test_rennes_paravance/1"

## We asume the measurements are located in ../mesures/
run_id = args[1]
## The cpu_consumption and cpu_overload experiments results are in the cpu_test_[g5k_site]_[cluster] folder
## The runs of the experiments are on folders 0/ 1/
expe_path = sprintf("cpu_test_rennes_paravance/%s", run_id)

#######################################################
#### Mesos cpu usage  #################################

A_path_cpu_experiment_0 = sprintf("../mesures/%s/experiment_0/mesos_cpu_A.csv", expe_path)
B_path_cpu_experiment_0 = sprintf("../mesures/%s/experiment_0/mesos_cpu_B.csv", expe_path)

A_path_cpu_experiment_1 = sprintf("../mesures/%s/experiment_1/mesos_cpu_A.csv", expe_path)
B_path_cpu_experiment_1 = sprintf("../mesures/%s/experiment_1/mesos_cpu_B.csv", expe_path)
# extraction des données
a_0 <- data.frame(
    read.csv2(file=A_path_cpu_experiment_0,
              sep=';',
              dec='.')
)

b_0 <- data.frame(
    read.csv2(file=B_path_cpu_experiment_0,
              sep=';',
              dec='.')
)

a_1 <- data.frame(
    read.csv2(file=A_path_cpu_experiment_1,
              sep=';',
              dec='.')
)

b_1 <- data.frame(
    read.csv2(file=B_path_cpu_experiment_1,
              sep=';',
              dec='.')
)

# --------------------------------------------------------------
# Merge a and b datasets corresponding to A and B timestamps.
# Expe 0
merged_expe_0 <- merge(a_0, b_0, by = c("mesure_id", "slave_id", "executor_id","cpus_limit"))

# Expe 1
merged_expe_1 <- merge(a_1, b_1, by = c("mesure_id", "slave_id", "executor_id","cpus_limit"))


# Fetch the containers cpus limit from data
## Cpu limit for expe 0
cpus_limit_0 = merged_expe_0['cpus_limit'][1,1]
## Cpu limit for expe 1
cpus_limit_1 = merged_expe_1['cpus_limit'][1,1]
## --------------------------------------------------------------
## "cpu consumption": number of cpus used inside a container over time interval [A,B]
## We define the number of cpu used over a duration D as the total time spend by the tasks/thread of the cgroup in running state
## divided by D.
## Example1: If a container/cgroup has a capacity of eight cpus:
## Each scheduling interval (100ms), tasks/threads inside the container/cgroup have the right to spend 800ms in running state.
## Example2: If the tasks inside a cgroup spent 400ms running during a 100ms interval the we say we used 4 cpus.
## Example3: If during a 10 seconds interval the tasks inside a cgroup spent 12 seconds in running state we say we used 1.2 cpus.
merged_expe_0$time_interval <- with(merged_expe_0, (timestamp_B - timestamp_A))
merged_expe_0$cpu_consumption <- with(merged_expe_0, ((cpus_user_time_secs_B - cpus_user_time_secs_A) + (cpus_system_time_secs_B - cpus_system_time_secs_A)) / time_interval)
merged_expe_0$cpu_overload <- with(merged_expe_0, (cpus_throttled_time_secs_B - cpus_throttled_time_secs_A)*100 /(time_interval))

# Temps moyen, écart type, variance
cpu_usage_mean <- mean(merged_expe_0$cpu_consumption)
cpu_usage_mean

# Multithread:

merged_expe_1$time_interval <- with(merged_expe_1, (timestamp_B - timestamp_A))
merged_expe_1$cpu_consumption <- with(merged_expe_1, ((cpus_user_time_secs_B - cpus_user_time_secs_A) + (cpus_system_time_secs_B - cpus_system_time_secs_A)) / time_interval)
merged_expe_1$cpu_overload <- with(merged_expe_1, (cpus_throttled_time_secs_B - cpus_throttled_time_secs_A)/ time_interval)

cpu_usage_mean_1 <- mean(merged_expe_1$cpu_consumption)

cpu_overload_mean_1 <- mean(merged_expe_1$cpu_overload)

#cpu_stats <- merge(merged_expe_0, merged_expe_1, all=TRUE)
#cpu_stats[1:13,]
out_path = sprintf("../mesures/%s/mesos_cpu_consumption_0.png", expe_path)
# tracé des résultats obtenu
p1 <- ggplot(data= merged_expe_0, mapping = aes(x = mesure_id, y = cpu_consumption)) +
    geom_line() +
    geom_hline(yintercept=cpus_limit_0, color = "red") +
    geom_hline(yintercept=cpu_usage_mean, color = "blue") +
    labs(x = "mesure id", y = "cpu consumption") +
    ylim(0, 2) +
    theme_minimal()
ggsave(out_path, plot = p1, scale = 1)

out_path_2 = sprintf("../mesures/%s/mesos_cpu_consumption_1.png", expe_path)
# tracé des résultats obtenu
p2 <- ggplot(data=merged_expe_1, mapping = aes(x = mesure_id, y = cpu_consumption)) +
    geom_line() +
    geom_hline(yintercept=cpus_limit_1, color = "red") +
    geom_hline(yintercept=cpu_usage_mean_1, color = "blue") +
    labs(x = "mesure id", y = "cpu consumption") +
    ylim(0, 2) +
    theme_minimal()
ggsave(out_path_2, plot = p2, scale = 1)

out_path_3 = sprintf("../mesures/%s/mesos_cpu_overload_0.png", expe_path)
p3 <- ggplot(data= merged_expe_0, mapping = aes(x = mesure_id, y = cpu_overload)) +
    geom_line() +
    labs(colour = "Topology type", x = "mesure id", y = "cpu overload") +
    ylim(0, 4) +
    theme_minimal()
ggsave(out_path_3, plot = p3, scale = 1)

out_path_4 = sprintf("../mesures/%s/mesos_cpu_overload_1.png", expe_path)
p4 <- ggplot(data= merged_expe_1, mapping = aes(x = mesure_id, y = cpu_overload)) +
    geom_line() +
    labs(colour = "Topology type", x = "mesure id", y = "cpu overload") +
    ylim(0, 4) +
    geom_hline(yintercept=cpu_overload_mean_1, color = "blue") +
    theme_minimal()
ggsave(out_path_4, plot = p4, scale = 1)

#show(p1)
#show(p2)

