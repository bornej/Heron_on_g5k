#!/usr/bin/ruby -w

# This script combines the merged measurements of multiple experiments.
# The experiments folders have the following naming convention
# s[word_size]_t[throttle]_p[padding]_[nomem, mem]_[combined, monothread, multithread]_[g5k_site]_[cluster]
# After applying the combine_runs.rb script
# Each experiment posess a sub-folder named merged_runs/ ... corresponding to the merged results
# of multiple runs of the experiment.
#
# Example:
#   s1000_t0_p300_l600_combined_nomem_rennes_paravance/
#   |   |
#   |   +--merged_runs/mesos_cpu_A.csv mesos_cpu_B.csv network_container_1_Consume_and_count_1.csv ...
#   |
#   s500_t0_p300_l600_combined_nomem_rennes_paravance/
#   |   |
#   |   +--merged_runs/mesos_cpu_A.csv mesos_cpu_B.csv network_container_1_Consume_and_count_1.csv ...
#   .
#   .
#
#   We combine the files having the same name found into the merged_stats folders into a single file.
#   to the resulting merged file placed into the merged_experiments folder.

require "csv"
require "fileutils"

# Need to run combine_runs.rb first.
#system("chmod +x ./combine_runs.rb && ./combine_runs.rb")

base_dir = './Experiments_1'
merged_experiments_dir = "#{base_dir}/merged_experiments"
system("rm -rf #{merged_experiments_dir}")
system("mkdir -p #{merged_experiments_dir}")
Dir.foreach(base_dir) do |experiment_dir|
  # Ignore . and ..
  next if experiment_dir == '.' or experiment_dir == '..'
  # if the current file experiment_dir is indeed a directory
  if File.directory?("#{base_dir}/#{experiment_dir}")
    if  Dir.exist?("#{base_dir}/#{experiment_dir}/merged_runs")
      # look for csv file into the merged_runs folder
      Dir.glob("#{base_dir}/#{experiment_dir}/merged_runs/*.csv").each do |file|
        # Ignoring backpressure_log.csv -> not a valid csv file
        next if File.basename(file) == "backpressure_log.csv"

        # network_*.csv files use ':' as separator
        if File.basename(file).match(/network*/)
          col_separator = ':'
        else
          col_separator = ';'
        end
        csv = CSV.read(file, {headers: true, col_sep: col_separator, quote_char: "|"} )

        # Check if the current csv file is empty
        head, *tail = csv
        if head.nil? || tail.nil?
          puts "nil :#{file}"
          next
        end

        out_file = "#{merged_experiments_dir}/#{File.basename(file)}"
        # If the result csv file already exist, then the csv headers were already writen into it,
        # we don't need to append the headers again.
        write_header = !File.file?(out_file)

        # Append the current csv file to the result merged_runs/ file.
        CSV.open(out_file, "ab", :col_sep => col_separator) do |csv_file|
          if write_header
            csv_file << csv.headers
          end
          csv.each do |row|
            csv_file << row
          end
        end
      end
    end
  end
end
#system("chmod +x process_merged_experiments.r &&./process_merged_experiments.r #{base_dir}")
