#!/usr/bin/ruby -w

# This script combines the measurements of multiple experiments runs.
# The experiments folders have the following naming convention
# s[word_size]_t[throttle]_p[padding]_[nomem, mem]_[combined, monothread, multithread]_[g5k_site]_[cluster]
#
# Each experiment posess sub-folders named 0/ 1/ 2/ ... each corresponding to different runs.
#
# Example:
#   s1000_t0_p300_l600_combined_nomem_rennes_paravance/
#   |  |
#   |  +--O/
#   |  |  |
#   |  |  +-monothread/mesos_cpu_A.csv mesos_cpu_B.csv network_container_1_Consume_and_count_1.csv ...
#   |  |  |
#   |  |  +-multithread/mesos_cpu_A.csv mesos_cpu_B.csv network_container_1_Consume_and_count_1.csv ...
#   |  |
#   |  +--1/
#   |  |  |
#   |  |  +-monothread/mesos_cpu_A.csv mesos_cpu_B.csv network_container_1_Consume_and_count_1.csv ...
#   |  |  |
#   |  |  +-multithread/mesos_cpu_A.csv mesos_cpu_B.csv network_container_1_Consume_and_count_1.csv ...
#   .  |
#   .  +--2/
#   .
#
#   We combine the files having the same name found into the 0/ 1/ 2/ folder .. into a single file.
#   We also add the following headers: ["run_id", "word_size", "padding", "topology_type"]
#   to the resulting merged file placed into the merged_runs folder.
#
# Example:
#   O/mesos_cpu_A.csv and 1/mesos_cpu_A.csv
#   are merged into: merged_runs/mesos_cpu_A.csv

require "csv"
require "fileutils"
base_dir = './Experiments_1'
Dir.foreach(base_dir) do |experiment_dir|
  # Ignore . and ..
  next if experiment_dir == '.' or experiment_dir == '..'
  # if the current file experiment_dir is indeed a directory
  if File.directory?("#{base_dir}/#{experiment_dir}")
    # Create the result directory "merged_runs"
    merged_dir = "#{base_dir}/#{experiment_dir}/merged_runs"
    system("rm -rf #{merged_dir}")
    system("mkdir -p #{merged_dir}")
    # Fetch word_size and padding values from the experiment directory name
    word_size = experiment_dir[/s(.*?)_/,1]
    padding = experiment_dir[/p(.*?)_/,1]
    length = experiment_dir[/l(.*?)_/,1]
    # Exploring the experiment 0/ 1/ ... runs folders
    Dir.foreach("#{base_dir}/#{experiment_dir}") do |run_dir|
      next if run_dir == '.' or run_dir == '..' or run_dir == "merged_runs" or !File.directory?("#{base_dir}/#{experiment_dir}/#{run_dir}")
      run_id = run_dir
      # Exploring the current run subfolders corresponding to a topology version measurement (monothread/ and multithread/).
      Dir.foreach("#{base_dir}/#{experiment_dir}/#{run_dir}") do |topology_version_dir|
        # ignore . and ..
        next if run_dir == '.' or run_dir == '..' or !File.directory?("#{base_dir}/#{experiment_dir}/#{run_dir}/#{topology_version_dir}")

        # look for backpresure_log.csv or backpressure_log.txt file for pre-processing
        Dir.glob("#{base_dir}/#{experiment_dir}/#{run_dir}/#{topology_version_dir}/backpressure_log.*").each { |file|
          # post-processing backpressure_log.csv/.txt : not a valid csv file
          # Create a proper csv file out of backpressure_log.
          backpresure_log = File.readlines(file)
          backpresure_csv = File.new("#{base_dir}/#{experiment_dir}/#{run_dir}/#{topology_version_dir}/backpresure.csv", "w")
          backpresure_csv.puts "mesure_id;time;backpresure"

          n = 0
          # This is a fix for badly designed parsing in wordcount_1.rb -> wont be needed in the future
          if backpresure_log[3][/No backpressure detected/]
            if length == 600
              nb_measurements = length.to_i/60 - 2
            else
              nb_measurements = length.to_i/60
            end
            (0..nb_measurements).each do |i|
              backpresure_csv.puts "#{i};0;0"
            end
          else
            backpresure_log[4..backpresure_log.length].each{ |line|
              time = line[/submission_time:(.*?)\n/,1]
              timeslice = line[/timeslice start: (.*?); backpressure time in ms: (.*?)/,1]
              backpresure = line[/timeslice start: (.*?); backpressure time in ms: (.*?)\n/,2]
              backpresure_csv.puts "#{n};#{timeslice};#{backpresure}"
              n += 1
            }
          end
          backpresure_csv.close
          # Delete the old backpressure_log file
          File.delete(file)
        }

        # look for csv file into the topology_version folder
        Dir.glob("#{base_dir}/#{experiment_dir}/#{run_dir}/#{topology_version_dir}/*.csv").each do |file|


          # network_*.csv files use ':' as separator
          if File.basename(file).match(/network*/)
            col_separator = ':'
          else
            col_separator = ';'
          end
          csv = CSV.read(file, {headers: true, col_sep: col_separator, quote_char: "|"} )

          # Check if the current csv file is empty
          head, *tail = csv
          if head.nil? || tail.nil?
            puts "nil :#{file}"
            next
          end

          # Add headers to current csv file
          csv.each do |row|
            row["topology_version"] = topology_version_dir
            row["run_id"] = run_id
            row["word_size"] = word_size
            row["padding"] = padding
            row["length"] = length
            row["nb_cpus"] = (padding.to_i/100 + 1.25)
          end

          headers = csv.headers
          # Post processing on emit_count and execute_count csv
          # Change emit_count_600s header to emit_count
          if File.basename(file).match(/execute_count*/)
            headers[1] = "execute_count"
          end

          if File.basename(file).match(/emit_count*/)
            headers[1] = "emit_count"
          end

          out_file = "#{merged_dir}/#{File.basename(file)}"
          # If the result csv file already exist, then the csv headers were already writen into it,
          # we don't need to append the headers again.
          write_header = !File.file?(out_file)

          # Append the current csv file to the result merged_runs/ file.
          CSV.open(out_file, "ab", :col_sep => col_separator) do |csv_file|
            if write_header
              csv_file << headers
            end
            csv.each do |row|
              csv_file << row
            end
          end
        end
      end
      # Generate plots for each run
     #system ("./process_run.r #{base_dir}/#{experiment_dir}/#{run_dir}")
    end
  end
  # Generate merged_run plots
  system("./process_merged_runs.r #{base_dir}/#{experiment_dir}")
end
