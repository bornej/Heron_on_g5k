#!/usr/bin/env Rscript
##  This script analyse and plot the .csv results of combine_experiments.rb
##  Those csv files contains the merged results of all experiments and their multiple runs.
##  We study Network statics, Cpu consumption and overload , Heron throughput, And Heron backpresure:
##  -------------------------------
## - Network statistics (nicstat):
##   network_container_1_Consume_and_count_1.csv:
##         Statistics gathered on the worker node containing the Consume_and_count bolt
##   network_container_2_Emit_random_words_2.csv:
##         Statistics gathered on the worker node containing the Emit_random_words spout
##   network_topology_master.csv:
##         Statistics gathered on the worker node containing the topology master
##   --------
##   HEADERS:
##   The files contain the following headers:
##   Time:Int:rKB/s:wKB/s:rPk/s:wPk/s:rAvs:wAvs:%Util:Sat:topology_version:run_id:word_size:padding:length:nb_cpus
##   - Time: The time of the measurement point
##   - Int: The network interface observed (Int0 or loopback)
##   - rKB/s: Read KiloBytes per second on the interface input
##   - wKB/s: Written KiloBytes per second on the interface output
##   - rPk/s: Read packets per second on the interface input
##   - wPk/s: Written packets per second on the interface output
##   - rAvs: Average size of packets received
##   - wAvs: Average size of packets sent
##   - %Util: not used
##   - Sat: not used
##   - topology_version: The version of the topology observed (monothreaded or multithreaded)
##   - run_id: The experiment run_id (0, 1, 2, ...)
##   - word_size: The size of the words emited by the spout (10, 100, 200, 500, 1000, 1500)
##   - padding: The padding percentage for the containers (0, 100, 200, 300, 400)
##   - length: The duration of the measurement (600 seconds, 3600 seconds).
##   - nb_cpus: THe container size in nb of cpu (calculated from the padding percentage, padding 0 -> 1.25 cpu, 100 -> 2.25 cpus, ...)
##  ---------------------------------
##  Cpu consumption and overload (mesos):
##  mesos_cpu_A.csv and  mesos_cpu_B.csv:
##  From Mesos http://#{slave_node}:5051/containers endpoint
##  we fetch the following metrics at regular interval (10s) for each worker/slave node
##    - *cpus_user_time_secs* : time spent by tasks of the /cgroup/ in user mode.
##    - *cpus_system_time_secs* : time spent by tasks of the /cgroup/ in kernel mode.
##    - *cpu_throttled_time* : total time for which tasks in the /cgroup/ have been throttled.
##       This is the cumulative time accross all the task in the /cgroup/. [[https://issues.apache.org/jira/browse/MESOS-2103]]
##
##  We use two consecutive measurements point /A/ and /B/ to compute the *cpu_consumption* and *cpu_overload* inside
##  the container for the 10s period that separate the two points. See readme.md for more informations.
##  --------
##  HEADERS:
##  The .csv  file contains the following headers:
##  mesure_id;slave_id;executor_id;heron_instances;cpus_limit;timestamp_[A,B];cpus_system_time_secs_[A,B];
##  cpus_user_time_secs_[A,B];cpus_throttled_time_secs_[A,B];processes;
##  threads;topology_version;run_id;word_size;padding;length;nb_cpus
##
##  - mesure_id : For each run we identify measurement point by an id (0, 1, 2, ..., 60)
##  - slave_id : The worker/slave node hostname (ex:paravance-13.rennes.grid5000.fr)
##  - executor_id : The thermos executor id (ex: thermos-root-devel-WordCountTopology-0-4bd2ac17-f406-4b2c-8ff9-3919bcf93b08)
##       inside the container
##  - heron_instances : The heron instances present in the container
##       (ex:[""""container_2_Emit_random_words_2""""]
##  - cpus_limit: The container size (nb cpus)
##  - timestamp_[A,B] = the time of the measurement point A (resp. B)
##  - cpus_system_time_secs_[A,B]: time spent by tasks of the /cgroup/ in kernel mode at time A (resp. B) in seconds
##  - cpus_user_time_secs_[A,B]: time spent by tasks of the /cgroup/ in user mode at time A (resp. B) in seconds
##  - cpus_throttled_time_secs_[A,B]: total time for which tasks in the /cgroup/ have been throttled at time A (resp. B) in seconds.
##  - processes: nb of process in the cgroup
##  - threads: nb of treads in the cgroup
##  - topology_version + rest : see network headers
##  ---------------------------------
##  Heron throughput (heron tracker):
##  emit_count.csv: Number of tuples emitted for each spout instance over the duration of the experiments (600s)
##  execute_count.csv: Number of tuples processed for each bolt instance over the duration of the experiment (600s)
##  -------
##  HEADERS:
## The .csv  file contains the following headers:
## spout_instance_id;[emit,execute]_count;topology_version;run_id;word_size;padding;length;nb_cpus
## - [bolt,spout]_instance_id: The id of the spout (resp. bolt)
## - [emit,execute]_count: The number of tuples emitted (resp. processed) by the spout (resp. bolt).
## - see network for the others headers informations.

library(ggplot2)
library(plyr)

library(reshape2)

if (FALSE) {
args = commandArgs(trailingOnly=TRUE)
if (length(args)!=1) {
    stop("One argument required: experiment_dir, usage: Rscript merged.r experiment_dir", call.=FALSE)
}
}

expe_path="./Experiments_1"
#expe_path = args[1]

if (FALSE) {
#####################################################
#### Nicstat network statistics #####################

consume_path = sprintf("%s/merged_experiments/network_container_1_Consume_and_count_1.csv", expe_path)
emit_path = sprintf("%s/merged_experiments/network_container_2_Emit_random_words_2.csv", expe_path)

# Extract consume data
consume <- data.frame(read.csv2(file=consume_path, sep=':', dec='.'))

# Add a component column to the consume dataframe
consume$component <- with(consume, "Consume_and_count")

# For the bolt we are only interested in the rKBps stats
# Add a column kbps representing the rKBps value
consume$kbps <- with(consume, rKB.s)

# Extract emit data
emit <- data.frame(read.csv2(file=emit_path, sep=':', dec='.'))

# Add a component column to the emit dataframe
emit$component <- with(emit, "Emit_random_words")

# For the spout we are only interested in the wKBps stats
# Add a column kbps representing the wKBps value
emit$kbps <- with(emit, wKB.s)

# Merge emit and consume stats
nicstats <- merge(emit, consume, all =TRUE)

## In the nicstats dataframe kbps is the rKBps for the bolt and wKBps for the spout

KBps_stats<-ddply(
    subset(nicstats, Int == "eth0"),
    c("Int", "component", "topology_version", "word_size", "nb_cpus"),
    summarise,
    N=length(Time),
    mean = mean(kbps),
    sd = sd(kbps),
    se = 1.96*(sd/sqrt(N))
)

## Generate separate plots for each word_size value in nicstats dataframe
p.list = lapply(sort(unique(KBps_stats$word_size)), function(i) {
    KBps_mean_path = sprintf("%s/merged_experiments/network_kbps_s%s.png", expe_path, i)
    plot_title = sprintf("Network stats: Mean KB/s, word_size = %s", i)
    KBps_means <- ggplot(data=subset(KBps_stats, (word_size== i) & (nb_cpus < 6)),
                         aes(x=factor(component),
                             y=mean,
                             fill = factor(topology_version),
                             linetype= factor(component),
                             )
                         )+
        geom_bar(aes(alpha = factor(nb_cpus)), position="dodge", stat="identity", color="black") +
        geom_errorbar(aes(ymin=mean-se, ymax=mean+se, group = interaction(topology_version, nb_cpus, ordered = TRUE)),
                      width=.2,                    # Width of the error bars
                      position=position_dodge(.9)) +
        ylim(0, 300000) +
        geom_text(aes(label= round(mean, digits = 0), group = interaction(topology_version, nb_cpus, ordered = TRUE), angle = 90), hjust = -.3, size = 4, position = position_dodge(0.9)) +
#        theme( axis.text.x=element_blank(), axis.ticks.x=element_blank()) +
    labs(title = plot_title, x = "Container size (nb cpus)", y = "KB/s")
    ggsave(KBps_mean_path, plot =KBps_means, scale = 1)
})

## Generate separate plots for each constainer size (nb cpus) value in nicstats dataframe
p.list = lapply(sort(unique(subset(KBps_stats, nb_cpus < 6)$nb_cpus)), function(i) {
    KBps_mean_path = sprintf("%s/merged_experiments/network_kbps_p%s.png", expe_path, i)
    plot_title = sprintf("Network stats: Mean KB/s, container size (nb cpus) = %s", i)
    KBps_means <- ggplot(data=subset(KBps_stats, nb_cpus== i),
                         aes(x=factor(component),
                             y=mean,
                             fill = factor(topology_version),
                             linetype= factor(component),
                             )
                         )+
        geom_bar(aes(alpha = factor(word_size)), position="dodge", stat="identity", color="black") +
        geom_errorbar(aes(ymin=mean-se, ymax=mean+se, group = interaction(topology_version, word_size, ordered = TRUE)),
                      width=.2,                    # Width of the error bars
                      position=position_dodge(.9)) +
        ylim(0, 300000) +
        geom_text(aes(y = mean+se, label= round(mean, digits = 0), group = interaction(topology_version, word_size, ordered = TRUE), angle = 90), hjust = -.3, size = 4, position = position_dodge(0.9)) +
#        theme( axis.text.x=element_blank(), axis.ticks.x=element_blank()) +
    labs(title = plot_title, x = "Word size", y = "KB/s")
    show (KBps_means)
    ggsave(KBps_mean_path, plot =KBps_means, scale = 1)
})

## Generate facet plots with constainer size (nb cpus) value as faceting factor
KBps_mean_path = sprintf("%s/merged_experiments/network_kbps_pfacet.png", expe_path)
plot_title = "Network stats: Mean KB/s, container size (nb cpus)"
KBps_means <- ggplot(data= subset(KBps_stats, nb_cpus <6),
                     aes(x=factor(component),
                         y=mean,
                         fill = factor(topology_version),
                         linetype= factor(component),
                         )
                     )+
    geom_bar(aes(alpha = factor(word_size)), position="dodge", stat="identity", color="black") +
    geom_errorbar(aes(ymin=mean-se, ymax=mean+se, group = interaction(topology_version, word_size, ordered = TRUE)),
                  width=.2,                    # Width of the error bars
                  position=position_dodge(.9)) +
    ylim(0, 300000) +
    facet_wrap(~ nb_cpus, ncol=1) +
    geom_text(aes(y = mean+se, label= round(mean, digits = 0), group = interaction(topology_version, word_size, ordered = TRUE), angle = 90), hjust = -.1, size = 3, position = position_dodge(0.9)) +
                                        #        theme( axis.text.x=element_blank(), axis.ticks.x=element_blank()) +
    labs(title = plot_title, x = "Word size", y = "KB/s")
show (KBps_means)
ggsave(KBps_mean_path, plot =KBps_means, scale = 1)


############## !!!!!!!!!!!!!!!!! THIS } is for the if FALSE
#}
############## !!!!!!!!!!!!!!!!!

#if (FALSE) {
#######################################################
#### Mesos cpu usage  #################################

A_path = sprintf("%s/merged_experiments/mesos_cpu_A.csv", expe_path)
B_path = sprintf("%s/merged_experiments/mesos_cpu_B.csv", expe_path)

a <- data.frame(read.csv2(file=A_path, sep=';', dec='.'))
b <- data.frame(read.csv2(file=B_path, sep=';', dec='.'))

# merge a and b datasets corresponding to A and B timestamps.
merged <- merge(a, b, by = c("mesure_id", "slave_id", "heron_instances", "executor_id","cpus_limit", "processes", "threads", "topology_version", "word_size", "padding", "run_id", "nb_cpus"))

merged$time_interval <- with(merged, (timestamp_B - timestamp_A))
merged$cpu_consumption <- with(merged, ((cpus_user_time_secs_B - cpus_user_time_secs_A) + (cpus_system_time_secs_B - cpus_system_time_secs_A)) / time_interval)
merged$cpu_overload <- with(merged, (cpus_throttled_time_secs_B - cpus_throttled_time_secs_A) /time_interval)
merged$cpu_percentage <- with(merged, cpu_consumption * 100 / cpus_limit)
cpu_stats <- subset(merged, heron_instances != '"[""topology_master""]"')

# To avoid the error geom_path: Each group consists of only one observation. Do you need to adjust the group aesthetic?
# https://stackoverflow.com/questions/27082601/ggplot2-line-chart-gives-geom-path-each-group-consist-of-only-one-observation#29019102
cpu_stats$mesure_id <- as.numeric(cpu_stats$mesure_id)

cpu_stats<-ddply(
    cpu_stats,
    c("heron_instances", "topology_version", "word_size", "cpus_limit"),
    summarise,
    N=length(cpu_consumption),
    mean_consumption=mean(cpu_consumption),
    mean_percentage=mean(cpu_percentage),
    mean_overload=mean(cpu_overload),
    sd=sd(cpu_consumption),
    sd_overload=sd(cpu_overload),
    sd_percentage=sd(cpu_percentage),
    se=1.96*(sd/sqrt(N)),
    se_overload=1.96*(sd_overload/sqrt(N)),
    se_percentage=1.96*(sd_percentage/sqrt(N))
)

cpu_stats <- subset(cpu_stats, cpus_limit < 6)

## Multiple plots function source:
## https://stackoverflow.com/questions/18158461/grouped-bar-plot-in-ggplot#18162330

## Generate cpu consumption plots for each container size (nb_cpus) value in cpu_stats dataframe
p.list = lapply(sort(unique(cpu_stats$cpus_limit)), function(i) {
    df <- subset(cpu_stats, cpus_limit == i)
    plot_title = sprintf("Cpu consumption, Container size (nb cpus)=%s", i)
    cpu_out_path = sprintf("%s/merged_experiments/mesos_cpu_p%s.png", expe_path, i)
    cpu_plot <- ggplot(data=df,
                       aes(x= factor(heron_instances),
                           y=mean_consumption,
                           fill = factor(topology_version),
                           linetype = as.factor(heron_instances)))+
        geom_bar(position="dodge",stat="identity", color="black", aes(alpha = as.factor(word_size))) +
        geom_errorbar(aes(ymin=mean_consumption-se, ymax=mean_consumption+se, group = interaction(topology_version, word_size, ordered = TRUE)), width=.2, position=position_dodge(.9)) +
        ylim(0, 6) +
                                        #    scale_alpha_manual(values=c(0.9,0.1)) +
        #facet_wrap(~ word_size) +
        theme( axis.text.x=element_blank(), axis.ticks.x=element_blank()) +
        geom_hline(data=df, aes(yintercept=cpus_limit, color = "red"), alpha = 0.9) +
        geom_text(aes(y = mean_consumption+se, label= round(mean_consumption, digits = 1), group = interaction(topology_version, word_size, ordered = TRUE)), vjust = -0.4, size = 4, position = position_dodge(0.9)) +
                                        #    geom_text(size = 3, position = position_stack(vjust = 1.05)) +
        labs(title = plot_title, x = "word_size", y = "cpu consumption")
    ggsave(cpu_out_path, plot = cpu_plot, scale = 1)
    #show (cpu_plot)
})


## Generate cpu consumption plots for each word size value in cpu_stats dataframe
p.list = lapply(sort(unique(cpu_stats$word_size)), function(i) {
    df <- subset(cpu_stats, word_size == i)
    plot_title = sprintf("Cpu consumption, Word_size=%s", i)
    cpu_out_path = sprintf("%s/merged_experiments/mesos_cpu_s%s.png", expe_path, i)
    cpu_plot <- ggplot(data=df,
                       aes(x= factor(heron_instances),
                           y=mean_percentage,
                           fill = factor(topology_version),
                           linetype = as.factor(heron_instances)))+
        geom_bar(position="dodge",stat="identity", color="black", aes(alpha = as.factor(cpus_limit))) +
        geom_errorbar(aes(ymin=mean_percentage-se, ymax=mean_percentage+se, group = interaction(topology_version, cpus_limit, ordered = TRUE)), width=.2, position=position_dodge(.9)) +
        theme(axis.text.x=element_blank(), axis.ticks.x=element_blank()) +
        geom_text(aes(y = mean_percentage+se, label = round(mean_percentage, digits = 1), group = interaction(topology_version, cpus_limit, ordered = TRUE)), vjust = -0.4, size = 4, position = position_dodge(0.9)) +
        labs(title = plot_title, x = "Container size (nb cpus)", y = "%cpu consumption")
    ggsave(cpu_out_path, plot = cpu_plot, scale = 1)
    #show (cpu_plot)
})

## Generate face_wrap with cpus_limit as faceting factor
plot_title = "Cpu consumption, Container size"
cpu_facet_out_path = sprintf("%s/merged_experiments/mesos_cpu_pfacet.png", expe_path)
cpu_facet_plot <- ggplot(data=cpu_stats,
                   aes(x= factor(heron_instances),
                       y=mean_consumption,
                       fill = factor(topology_version),
                       linetype = as.factor(heron_instances)))+
    geom_bar(position="dodge",stat="identity", color="black", aes(alpha = as.factor(word_size))) +
    geom_errorbar(aes(ymin=mean_consumption-se, ymax=mean_consumption+se, group = interaction(topology_version, word_size, ordered = TRUE)), width=.2, position=position_dodge(.9)) +
    ylim(0, 6) +
                                        #    scale_alpha_manual(values=c(0.9,0.1)) +
    facet_wrap(~ cpus_limit, ncol=1) +
    theme( axis.text.x=element_blank(), axis.ticks.x=element_blank()) +
    geom_hline(data=cpu_stats, aes(yintercept=cpus_limit, color = "red"), alpha = 0.9) +
    geom_text(aes(y = mean_consumption+se, label= round(mean_consumption, digits = 1), group = interaction(topology_version, word_size, ordered = TRUE)), vjust = -0.4, size = 3, position = position_dodge(0.9)) +
                                        #    geom_text(size = 3, position = position_stack(vjust = 1.05)) +
    labs(title = plot_title, x = "word_size", y = "cpu consumption")
ggsave(cpu_facet_out_path, plot = cpu_facet_plot, scale = 1)
show (cpu_facet_plot)

############## !!!!!!!!!!!!!!!!! THIS } is for the if FALSE
#}
############## !!!!!!!!!!!!!!!!!
#if (FALSE) {

## Generate CPU OVERLOAD plot For each cpu_size
p.list = lapply(sort(unique(cpu_stats$cpus_limit)), function(i) {
    df <- subset(cpu_stats, cpus_limit == i)
    plot_title = sprintf("Cpu overload, container_size=%s", i)
    cpu_overload_out_path = sprintf("%s/merged_experiments/mesos_cpu_overload_p%s.png", expe_path, i)
    cpu_overload_plot <- ggplot(data=df,
                                aes(x= factor(heron_instances),
                                    y=mean_overload,
                                    fill = factor(topology_version),
                                    linetype = as.factor(heron_instances)))+
        geom_bar(position="dodge",stat="identity", color="black", aes(alpha = as.factor(word_size))) +
        geom_errorbar(aes(ymin=mean_overload-se_overload, ymax=mean_overload+se_overload, group = interaction(topology_version, word_size, ordered = TRUE)), width=.2, position=position_dodge(.9)) +
        ylim(0, 4) +
        theme( axis.text.x=element_blank(), axis.ticks.x=element_blank()) +
        geom_text(aes(label= round(mean_overload, digits = 1), group = interaction(topology_version, word_size, ordered = TRUE), angle = 90), hjust = -.3, size = 3, position = position_dodge(0.9)) +
        labs(title = plot_title, x = "Word size", y = "cpu overload")
    ggsave(cpu_overload_out_path, plot = cpu_overload_plot, scale = 1)
    #show (cpu_overload_plot)
})

## CPU OVERLOAD For each word_size
p.list = lapply(sort(unique(cpu_stats$word_size)), function(i) {
    df <- subset(cpu_stats, word_size == i)
    plot_title = sprintf("Cpu overload, word_size=%s", i)
    cpu_overload_out_path = sprintf("%s/merged_experiments/mesos_cpu_overload_s%s.png", expe_path, i)
    cpu_overload_plot <- ggplot(data=df,
                                aes(x= heron_instances,
                                    y=mean_overload,
                                    fill = factor(topology_version),
                                    linetype = as.factor(heron_instances)))+
        geom_bar(position="dodge",stat="identity", color="black", aes(alpha = as.factor(cpus_limit))) +
        geom_errorbar(aes(ymin=mean_overload-se_overload, ymax=mean_overload+se_overload, group = interaction(topology_version, cpus_limit, ordered = TRUE)), width=.2, position=position_dodge(.9)) +
        ylim(0, 4) +
        theme( axis.text.x=element_blank(), axis.ticks.x=element_blank()) +
        geom_text(aes(y = mean_overload+se_overload, label= round(mean_overload, digits = 1), group = interaction(topology_version, cpus_limit, ordered = TRUE)), vjust = -2, size = 4, position = position_dodge(0.9)) +
        labs(title = plot_title, x = "Container size(nb of cpus)", y = "cpu overload")
    ggsave(cpu_overload_out_path, plot = cpu_overload_plot, scale = 1)
    #show (cpu_overload_plot)
})

## Generate CPU OVERLOAD facet plot with cpu_size as facetting factor

plot_title = "Cpu overload, container_size"
cpu_overload_out_path = sprintf("%s/merged_experiments/mesos_cpu_overload_pfacet.png", expe_path)
cpu_overload_plot <- ggplot(data=cpu_stats,
                            aes(x= factor(heron_instances),
                                y=mean_overload,
                                fill = factor(topology_version),
                                linetype = as.factor(heron_instances)))+
    geom_bar(position="dodge",stat="identity", color="black", aes(alpha = as.factor(word_size))) +
    geom_errorbar(aes(ymin=mean_overload-se_overload, ymax=mean_overload+se_overload, group = interaction(topology_version, word_size, ordered = TRUE)), width=.2, position=position_dodge(.9)) +
    ylim(0, 4) +
    facet_wrap(~ cpus_limit, ncol = 1) +
    theme( axis.text.x=element_blank(), axis.ticks.x=element_blank()) +
    geom_text(aes(label= round(mean_overload, digits = 1), group = interaction(topology_version, word_size, ordered = TRUE), angle = 90), hjust = -.3, size = 3, position = position_dodge(0.9)) +
    labs(title = plot_title, x = "Word size", y = "cpu overload")
ggsave(cpu_overload_out_path, plot = cpu_overload_plot, scale = 1)
show (cpu_overload_plot)

############## !!!!!!!!!!!!!!!!! THIS } is for the if FALSE
#}
############## !!!!!!!!!!!!!!!!!
#if (FALSE) {
### Heron statistics
emit_path = sprintf("%s/merged_experiments/emit_count.csv", expe_path)
execute_path = sprintf("%s/merged_experiments/execute_count.csv", expe_path)

# extraction des données
emit <- data.frame(read.csv2(file=emit_path, sep=';', dec='.'))
execute <- data.frame(read.csv2(file=execute_path, sep=';', dec='.'))

emit$tuples_per_second <- with(emit, (emit_count/length))
emit$kbps <- with(emit, tuples_per_second*word_size/1000)

emit$component <- with(emit, "Emit_random_words")

execute$tuples_per_second <- with(execute, (execute_count/length))
execute$kbps <- with(execute, tuples_per_second*word_size/1000)
execute$component <- with(emit, "Consume_and_count")

heron_throughput <- merge (emit, execute, all= TRUE)

heron_throughput_stats<-ddply(
    heron_throughput,
    c("topology_version", "component", "word_size", "nb_cpus"),
    summarise,
    N=length(tuples_per_second),
    mean=mean(tuples_per_second),
    sd=sd(tuples_per_second),
    se=1.96*(sd/sqrt(N))
)

## Generate heron throughput plots for each word_size value in heron_throughput_stats dataframe
p.list = lapply(sort(unique(heron_throughput_stats$word_size)), function(i) {
    df <- subset(heron_throughput_stats, word_size == i)
    plot_title = sprintf("Heron throughput (tuples/s), word_size=%s", i)
    heron_throughput_out_path = sprintf("%s/merged_experiments/Heron_throughput_s%s.png", expe_path, i)
    heron_throughput_plot <- ggplot(data=df,
                                    aes(x=component,
                                        y=mean,
                                        fill = factor(topology_version),
                                        linetype = factor(component)
                                        )
                                    )+
        geom_bar(aes(alpha = as.factor(nb_cpus)), position="dodge", stat="identity", color="black") +
        geom_errorbar(aes(ymin=mean-se, ymax=mean+se, group = interaction(topology_version, nb_cpus, ordered = TRUE)), width=.2, position=position_dodge(.9)) +
        ylim(0, 1200000) +
        geom_text(aes(y=mean+se, label= round(mean, digits = 0), group = interaction(topology_version, nb_cpus, ordered = TRUE), angle = 90), hjust = -0.2, size = 4, position = position_dodge(0.9), alpha = 1) +
        labs(title = plot_title, x = "container size (nb_cpus)", y = "Tuples/s")
    ggsave(heron_throughput_out_path, plot = heron_throughput_plot, scale = 1)
    #show (heron_throughput_plot)
})

## Generate heron throughput plots for each container size (nb_cpus) value in heron_throughput_stats dataframe
p.list = lapply(sort(unique(heron_throughput_stats$nb_cpus)), function(i) {
    df <- subset(heron_throughput_stats, nb_cpus == i)
    plot_title = sprintf("Heron throughput (tuples/s), container size (nb_cpus)=%s", i)
    heron_throughput_out_path = sprintf("%s/merged_experiments/Heron_throughput_p%s.png", expe_path, i)
    heron_throughput_plot <- ggplot(data=df,
                                    aes(x=component,
                                        y=mean,
                                        fill = factor(topology_version),
                                        linetype = factor(component)
                                        )
                                    )+
        geom_bar(aes(alpha = as.factor(word_size)), position="dodge", stat="identity", color="black") +
        geom_errorbar(aes(ymin=mean-se, ymax=mean+se, group = interaction(topology_version, word_size, ordered = TRUE)), width=.2, position=position_dodge(.9)) +
        ylim(0, 1200000) +
#        theme( axis.text.x=element_blank(), axis.ticks.x=element_blank()) +
        geom_text(aes(y=mean+se, label= round(mean, digits = 0), group = interaction(topology_version, word_size, ordered = TRUE), angle = 90), hjust = -0.1, size = 4, position = position_dodge(0.9), alpha = 1) +
        labs(title = plot_title, x = "word_size", y = "Tuples/s")
    ggsave(heron_throughput_out_path, plot = heron_throughput_plot, scale = 1)
    #show (heron_throughput_plot)
})

## Generate heron throughput facet plot with container size (nb_cpus) as faceting factor
df <- subset(heron_throughput_stats, nb_cpus < 6)
plot_title = "Heron throughput (tuples/s), container size (nb_cpus)"
heron_throughput_out_path = sprintf("%s/merged_experiments/Heron_throughput_pfacet.png", expe_path)
heron_throughput_plot <- ggplot(data=df,
                                aes(x=component,
                                    y=mean,
                                    fill = factor(topology_version),
                                    linetype = factor(component)
                                    )
                                )+
    geom_bar(aes(alpha = as.factor(word_size)), position="dodge", stat="identity", color="black") +
    geom_errorbar(aes(ymin=mean-se, ymax=mean+se, group = interaction(topology_version, word_size, ordered = TRUE)), width=.2, position=position_dodge(.9)) +
    ylim(0, 1200000) +
    facet_wrap(~ nb_cpus, ncol = 1) +
                                        #        theme( axis.text.x=element_blank(), axis.ticks.x=element_blank()) +
    geom_text(aes(y=mean+se, label= round(mean, digits = 0), group = interaction(topology_version, word_size, ordered = TRUE), angle = 90), hjust = -0.1, size = 3, position = position_dodge(0.9), alpha = 1) +
    labs(title = plot_title, x = "word_size", y = "Tuples/s")
ggsave(heron_throughput_out_path, plot = heron_throughput_plot, scale = 1)
show (heron_throughput_plot)

############## !!!!!!!!!!!!!!!!! THIS } is for the if FALSE
#}
############## !!!!!!!!!!!!!!!!!


#if (FALSE) {
### Backpresure statistics
backpresure_path = sprintf("%s/merged_experiments/backpresure.csv", expe_path)

# extraction des données
backpresure <- data.frame(read.csv2(file=backpresure_path, sep=';', dec='.'))
backpresure$backpresure_percentage <- with(backpresure, (backpresure * 100 / 60000))

heron_backpresure_stats<-ddply(
    backpresure,
    c("topology_version", "word_size", "nb_cpus"),
    summarise,
    N=length(backpresure),
    mean=mean(backpresure),
    mean_percentage=mean(backpresure_percentage),
    sd=sd(backpresure),
    sd_percentage=sd(backpresure_percentage),
    se=1.96*(sd/sqrt(N)),
    se_percentage=1.96*(sd_percentage/sqrt(N))
)
heron_backpresure_stats <- subset(heron_backpresure_stats, nb_cpus < 6)

## Generate heron backpresure plots for each word_size value in heron_backpresure_stats dataframe
p.list = lapply(sort(unique(heron_backpresure_stats$word_size)), function(i) {
    df <- subset(heron_backpresure_stats, word_size == i)
    plot_title = sprintf("Average time spent in backpresure in ms per minute slice, word_size=%s", i)
    heron_backpresure_out_path = sprintf("%s/merged_experiments/Heron_backpresure_s%s.png", expe_path, i)
    heron_backpresure_plot <- ggplot(data=df,
                                     aes(x=nb_cpus,
                                         y=mean,
                                        fill = factor(topology_version),
                                        )
                                    )+
        geom_bar(aes(alpha = as.factor(nb_cpus)), position="dodge", stat="identity", color="black") +
        geom_errorbar(aes(ymin=mean-se, ymax=mean+se, group = interaction(topology_version, nb_cpus, ordered = TRUE)), width=.2, position=position_dodge(.9)) +
        ylim(0, 60000) +
        theme( axis.text.x=element_blank(), axis.ticks.x=element_blank()) +
        #geom_text(aes(label= round(mean, digits = 0), group = nb_cpus, angle = 90), hjust = -2, size = 4, position = position_dodge(0.9), alpha = 1) +
        labs(title = plot_title, x = "Container size (nb cpus)", y = "Time in backpressure ms")
    ggsave(heron_backpresure_out_path, plot = heron_backpresure_plot, scale = 1)
    #show (heron_backpresure_plot)
})


    ## Generate heron backpresure plots for each nb_cpus value in heron_backpresure_stats dataframe
p.list = lapply(sort(unique(heron_backpresure_stats$nb_cpus)), function(i) {
    df <- subset(heron_backpresure_stats, nb_cpus == i)
    plot_title = sprintf("Average time spent in backpresure in ms per minute, Container size (nb_cpus)=%s", i)
    heron_backpresure_out_path = sprintf("%s/merged_experiments/Heron_backpresure_p%s.png", expe_path, i)
    heron_backpresure_plot <- ggplot(data=df, aes(x=nb_cpus, y=mean, fill = factor(topology_version))) +
        geom_bar(aes(alpha = as.factor(word_size)), position="dodge", stat="identity", color="black") +
        geom_errorbar(aes(ymin=mean-se, ymax=mean+se, group = interaction(topology_version, word_size, ordered = TRUE)), width=.2, position=position_dodge(.9)) +
        ylim(0, 60000) +
        theme( axis.text.x=element_blank(), axis.ticks.x=element_blank()) +
        geom_text(aes(y=mean+se, label= round(mean, digits = 0), group =interaction(topology_version, word_size, ordered = TRUE), angle = 90), hjust = -0.2, size = 4, position = position_dodge(0.9), alpha = 1) +
        labs(title = plot_title, x = "Word size", y = "Time in backpressure ms")
    ggsave(heron_backpresure_out_path, plot = heron_backpresure_plot, scale = 1)
    show (heron_backpresure_plot)
})


df <- subset(heron_backpresure_stats, nb_cpus <6)
plot_title = "Average time spent in backpresure in ms per minute, Container size (nb_cpus)"
heron_backpresure_out_path = sprintf("%s/merged_experiments/Heron_backpresure_pfacet.png", expe_path)
heron_backpresure_plot <- ggplot(data=df, aes(x= interaction(word_size, topology_version, ordered = TRUE), y=mean, fill = factor(topology_version))) +
    geom_bar(aes(alpha = as.factor(word_size)), position="dodge", stat="identity", color="black") +
    geom_errorbar(aes(ymin=mean-se, ymax=mean+se, group = interaction(word_size, topology_version, ordered = TRUE)), width=.2, position=position_dodge(.9)) +
    ylim(0, 60000) +
    facet_wrap(~ nb_cpus, ncol=1) +
    theme( axis.text.x=element_blank(), axis.ticks.x=element_blank()) +
    geom_text(aes(y=mean+se, label= round(mean, digits = 0), group =interaction(topology_version, word_size, ordered = TRUE), angle = 90), hjust = -0.2, size = 3, position = position_dodge(0.9), alpha = 1) +
    labs(title = plot_title, x = "Word size", y = "Time in backpressure ms")
ggsave(heron_backpresure_out_path, plot = heron_backpresure_plot, scale = 1)
show (heron_backpresure_plot)

############## !!!!!!!!!!!!!!!!! THIS } is for the if FALSE
#}
############## !!!!!!!!!!!!!!!!!
