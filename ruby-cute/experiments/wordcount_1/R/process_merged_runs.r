#!/usr/bin/env Rscript
##  This script analyse and plot the .csv results of combine_runs.rb
##
##  Those csv files can be found in each experiment's merged_runs subfolder.
##  They contain the merged results of the multiple runs of the experiments.
##  We study Network statics, Cpu consumption and overload , Heron throughput
##  -------------------------------
## - Network statistics (nicstat):
##   network_container_1_Consume_and_count_1.csv:
##         Statistics gathered on the worker node containing the Consume_and_count bolt
##   network_container_2_Emit_random_words_2.csv:
##         Statistics gathered on the worker node containing the Emit_random_words spout
##   network_topology_master.csv:
##         Statistics gathered on the worker node containing the topology master
##   --------
##   HEADERS:
##   The files contain the following headers:
##   Time:Int:rKB/s:wKB/s:rPk/s:wPk/s:rAvs:wAvs:%Util:Sat:topology_version:run_id:word_size:padding:length:nb_cpus
##   - Time: The time of the measurement point
##   - Int: The network interface observed (Int0 or loopback)
##   - rKB/s: Read KiloBytes per second on the interface input
##   - wKB/s: Written KiloBytes per second on the interface output
##   - rPk/s: Read packets per second on the interface input
##   - wPk/s: Written packets per second on the interface output
##   - rAvs: Average size of packets received
##   - wAvs: Average size of packets sent
##   - %Util: not used
##   - Sat: not used
##   - topology_version: The version of the topology observed (monothreaded or multithreaded)
##   - run_id: The experiment run_id (0, 1, 2, ...)
##   - word_size: The size of the words emited by the spout (10, 100, 200, 500, 1000, 1500)
##   - padding: The padding percentage for the containers (0, 100, 200, 300, 400)
##   - length: The duration of the measurement (600 seconds, 3600 seconds).
##   - nb_cpus: THe container size in nb of cpu (calculated from the padding percentage, padding 0 -> 1.25 cpu, 100 -> 2.25 cpus, ...)
##  ---------------------------------
##  Cpu consumption and overload (mesos):
##  mesos_cpu_A.csv and  mesos_cpu_B.csv:
##  From Mesos http://#{slave_node}:5051/containers endpoint
##  we fetch the following metrics at regular interval (10s) for each worker/slave node
##    - *cpus_user_time_secs* : time spent by tasks of the /cgroup/ in user mode.
##    - *cpus_system_time_secs* : time spent by tasks of the /cgroup/ in kernel mode.
##    - *cpu_throttled_time* : total time for which tasks in the /cgroup/ have been throttled.
##       This is the cumulative time accross all the task in the /cgroup/. [[https://issues.apache.org/jira/browse/MESOS-2103]]
##
##  We use two consecutive measurements point /A/ and /B/ to compute the *cpu_consumption* and *cpu_overload* inside
##  the container for the 10s period that separate the two points. See readme.md for more informations.
##  --------
##  HEADERS:
##  The .csv  file contains the following headers:
##  mesure_id;slave_id;executor_id;heron_instances;cpus_limit;timestamp_[A,B];cpus_system_time_secs_[A,B];
##  cpus_user_time_secs_[A,B];cpus_throttled_time_secs_[A,B];processes;
##  threads;topology_version;run_id;word_size;padding;length;nb_cpus
##
##  - mesure_id : For each run we identify measurement point by an id (0, 1, 2, ..., 60)
##  - slave_id : The worker/slave node hostname (ex:paravance-13.rennes.grid5000.fr)
##  - executor_id : The thermos executor id (ex: thermos-root-devel-WordCountTopology-0-4bd2ac17-f406-4b2c-8ff9-3919bcf93b08)
##       inside the container
##  - heron_instances : The heron instances present in the container
##       (ex:[""""container_2_Emit_random_words_2""""]
##  - cpus_limit: The container size (nb cpus)
##  - timestamp_[A,B] = the time of the measurement point A (resp. B)
##  - cpus_system_time_secs_[A,B]: time spent by tasks of the /cgroup/ in kernel mode at time A (resp. B) in seconds
##  - cpus_user_time_secs_[A,B]: time spent by tasks of the /cgroup/ in user mode at time A (resp. B) in seconds
##  - cpus_throttled_time_secs_[A,B]: total time for which tasks in the /cgroup/ have been throttled at time A (resp. B) in seconds.
##  - processes: nb of process in the cgroup
##  - threads: nb of treads in the cgroup
##  - topology_version + rest : see network headers
##  ---------------------------------
##  Heron throughput (heron tracker):
##  emit_count.csv: Number of tuples emitted for each spout instance over the duration of the experiments (600s)
##  execute_count.csv: Number of tuples processed for each bolt instance over the duration of the experiment (600s)
##  -------
##  HEADERS:
## The .csv  file contains the following headers:
## spout_instance_id;[emit,execute]_count;topology_version;run_id;word_size;padding;length;nb_cpus
## - [bolt,spout]_instance_id: The id of the spout (resp. bolt)
## - [emit,execute]_count: The number of tuples emitted (resp. processed) by the spout (resp. bolt).
## - see network for the others headers informations.

library(ggplot2)
library(plyr)

library(reshape2)

#if (FALSE) {
args = commandArgs(trailingOnly=TRUE)
if (length(args)!=1) {
    stop("One argument required: experiment_dir, usage: Rscript merged.r experiment_dir", call.=FALSE)
}
#}
#####################################################
#### Nicstat network statistics #####################

#expe_path="./Test/s100_t0_p100_l600_combined_nomem_rennes_paravance"
expe_path = args[1]

#if (FALSE) {
consume_path = sprintf("%s/merged_runs/network_container_1_Consume_and_count_1.csv", expe_path)
emit_path = sprintf("%s/merged_runs/network_container_2_Emit_random_words_2.csv", expe_path)

# extraction des données consume
consume <- data.frame(read.csv2(file=consume_path, sep=':', dec='.'))

# Ajout d'une collone component
consume$component <- with(consume, "Consume_and_count")

# extraction des données emit
emit <- data.frame(read.csv2(file=emit_path, sep=':', dec='.'))

# Ajout d'une collone component
emit$component <- with(emit, "Emit_random_words")

# Merge emit and consume stats
nicstats <- merge(emit, consume, all =TRUE)

#nicstats

# Temps moyen, écart type, variance
rKBps_stats<-ddply(
    subset(nicstats, (component=="Consume_and_count")&(Int=="eth0")),
    c("Int", "topology_version", "run_id"),
    summarise,
    N=length(Time),
    mean=mean(rKB.s),
    sum=sum(rKB.s),
    sd=sd(rKB.s),
    se=1.96*(sd/sqrt(N))
)

rKBps_stats_stats <- ddply(
    rKBps_stats,
    c("topology_version"),
    summarise,
    N=length(topology_version),
    mean=mean(mean),
    sd=sd(sd),
    se=1.96*(sd/sqrt(N))
)

# Temps moyen, écart type, variance
wKBps_stats<-ddply(
    subset(nicstats, (component=="Emit_random_words")&(Int=="eth0")),
    c("Int", "topology_version", "run_id"),
    summarise,
    N=length(Time),
    mean=mean(wKB.s),
    sum=sum(wKB.s),
    sd=sd(wKB.s),
    se=1.96*(sd/sqrt(N))
)
#wKBps_stats

emit_out_mean_path = sprintf("%s/merged_runs/network_container_1_Emit_random_words_1_means.png", expe_path)

emit_means <- ggplot(data=wKBps_stats, aes(x=run_id, y=mean, fill = factor(topology_version),  label = mean))+
    geom_bar(position="dodge", stat="identity") +
    geom_errorbar(aes(ymin=mean-se, ymax=mean+se),
                  width=.2,                    # Width of the error bars
                  position=position_dodge(.9)) +
    ylim(0, 300000) +
    geom_text(aes(label= round(mean, digits = 0)), vjust = -2.5, size = 3, position = position_dodge(0.9)) +
#    geom_text(size = 3, position = position_stack(vjust = 1.05)) +
    labs(title = "Emit_random_words_node mean wKB/s", x = "run_id", y = "wKB/s")
ggsave(emit_out_mean_path, plot = emit_means, scale = 1)
#show (emit_means)

consume_out_mean_path = sprintf("%s/merged_runs/network_container_1_Consume_and_count_1_means.png", expe_path)
consume_means <- ggplot(data=rKBps_stats, aes(x=run_id, y=mean, fill = factor(topology_version), label = mean))+
    geom_bar(position="dodge", stat="identity") +
    geom_errorbar(aes(ymin=mean-se, ymax=mean+se),
                  width=.2,                    # Width of the error bars
                  position=position_dodge(.9)) +
    ylim(0, 300000) +
    geom_text(aes(label= round(mean, digits = 0)), vjust = -2.5, size = 3, position = position_dodge(0.9)) +
    labs(title = "Consume_and_count_node mean rKB/s", x = "run_id", y = "rKB/s")
ggsave(consume_out_mean_path, plot = consume_means, scale = 1)
#show (consume_means)

## Add a measurment point ID based on the combination of factors (topology_version, run_id, Int)
## Source: https://stackoverflow.com/questions/12925063/numbering-rows-within-groups-in-a-data-frame?noredirect=1&lq=1
nicstats$measurement_point <- ave(seq_len(nrow(nicstats)), nicstats$run_id, nicstats$topology_version, nicstats$Int, nicstats$component, FUN =seq_along)

network_stats_out_path = sprintf("%s/merged_runs/network_statistics.png", expe_path)
# tracé des résultats obtenu
p1 <- ggplot(data = subset(nicstats, (Int== "eth0") ), aes(x = measurement_point, color = as.factor(topology_version), linetype= as.factor(component))) +
    geom_line(data = subset(nicstats, (Int== "eth0")&(component=="Consume_and_count")), aes(y = rKB.s)) +
    geom_line(data = subset(nicstats, (Int== "eth0")&(component=="Emit_random_words")), aes(y = wKB.s)) +
    ylim(0, 300000) +
    facet_wrap(~run_id) +
    labs(title = "Network_statistics", x = "Measurement_point", y = "KB/s") +
    theme_minimal()
ggsave(network_stats_out_path, plot = p1, scale = 1)
#show(p1)

#######################################################
#### Mesos cpu usage  #################################

A_path = sprintf("%s/merged_runs/mesos_cpu_A.csv", expe_path)
B_path = sprintf("%s/merged_runs/mesos_cpu_B.csv", expe_path)

# extraction des données
a <- data.frame(
    read.csv2(file=A_path,
              sep=';',
              dec='.')
)

b <- data.frame(
    read.csv2(file=B_path,
              sep=';',
              dec='.')
)

# --------------------------------------------------------------
# Fusion des datasets a et b correspondant aux timestamps A et B.
merged <- merge(a, b, by = c("mesure_id", "slave_id", "heron_instances", "executor_id","cpus_limit", "processes", "threads", "topology_version", "word_size", "padding", "run_id"))

# Fetch the containers cpus limit from data
cpus_limit = merged['cpus_limit'][1,1]
# --------------------------------------------------------------
# Ajout d'une collone calculant la consomation totale cpu pendant l'intervalle de temps [A,B]
# cpu_consumption = [(cpus_user_time_secs_B - cpus_user_time_secs_A) + (cpus_system_time_secs_B - cpus_system_time_secs_A)] / (timestamp_B - timestamp_A)

merged$time_interval <- with(merged, (timestamp_B - timestamp_A))
merged$cpu_consumption <- with(merged, ((cpus_user_time_secs_B - cpus_user_time_secs_A) + (cpus_system_time_secs_B - cpus_system_time_secs_A)) / time_interval)
merged$cpu_overload <- with(merged, (cpus_throttled_time_secs_B - cpus_throttled_time_secs_A) /time_interval)

cpu_stats <- merged

# To avoid geom_path: Each group consists of only one observation. Do you need to adjust the group aesthetic?
# https://stackoverflow.com/questions/27082601/ggplot2-line-chart-gives-geom-path-each-group-consist-of-only-one-observation#29019102
cpu_stats$mesure_id <- as.numeric(cpu_stats$mesure_id)

out_path = sprintf("%s/merged_runs/mesos_cpu.png", expe_path)
# tracé des résultats obtenu
p1 <- ggplot(data= cpu_stats, mapping = aes(x = mesure_id, y = cpu_consumption, color = as.factor(topology_version), alpha = as.factor(run_id))) +
    geom_line() +
    geom_hline(yintercept=cpus_limit, color = "red") +
    labs(
        colour = "heron instances",
        x = "mesure id",
        y = "cpu consumption"
    ) + facet_wrap(~heron_instances) +
        labs(
        fill = "Slave"
        ) +
    theme_minimal()
ggsave(out_path, plot = p1, scale = 1)

out_path_2 = sprintf("%s/merged_runs/mesos_cpu_throttled.png", expe_path)
p2 <- ggplot(data= cpu_stats, mapping = aes(x = mesure_id, y = cpu_overload, color = as.factor(topology_version), alpha = as.factor(run_id))) +
    geom_line() +
    labs(
        colour = "Topology type",
        x = "mesure id",
        y = "%cpu overload"
    ) + facet_wrap(~heron_instances) +
        labs(
        fill = "Slave"
        ) +
    ylim(0, 8) +
    theme_minimal()
ggsave(out_path_2, plot = p2, scale = 1)

#show(p1)
#show(p2)

############## !!!!!!!!!!!!!!!!! THIS } is for the if FALSE
#}
############## !!!!!!!!!!!!!!!!! 
### Heron statistics
emit_path = sprintf("%s/merged_runs/emit_count.csv", expe_path)
execute_path = sprintf("%s/merged_runs/execute_count.csv", expe_path)

# extraction des données
emit <- data.frame(read.csv2(file=emit_path, sep=';', dec='.'))
execute <- data.frame(read.csv2(file=execute_path, sep=';', dec='.'))

emit$tuples_per_second <- with(emit, (emit_count/length))
emit$kbps <- with(emit, tuples_per_second*word_size/1000)

execute$tuples_per_second <- with(execute, (execute_count/length))
execute$kbps <- with(execute, tuples_per_second*word_size/1000)

emit_out_path = sprintf("%s/merged_runs/Emit_count.png", expe_path)
emit_plot <- ggplot(data=emit, aes(x=run_id, y=tuples_per_second, fill = factor(topology_version),  label = emit_count))+
    geom_bar(position="dodge", stat="identity") +
    ylim(0, 1000000) +
    geom_text(aes(label= round(tuples_per_second, digits = 0)), vjust = -2.5, size = 3, position = position_dodge(0.9)) +
    labs(title = "Emit_random_words_node Tuples/s", x = "run_id", y = "Tuples/s")
ggsave(emit_out_path, plot = emit_plot, scale = 1)
#show (emit_plot)

emit_kbps_out_path = sprintf("%s/merged_runs/Emit_kbps.png", expe_path)
emit_kbps_plot <- ggplot(data=emit, aes(x=run_id, y=kbps, fill = factor(topology_version),  label = emit_count))+
    geom_bar(position="dodge", stat="identity") +
    ylim(0, 300000) +
    geom_text(aes(label= round(kbps, digits = 0)), vjust = -2.5, size = 3, position = position_dodge(0.9)) +
    labs(title = "Emit_random_words_node kB/s", x = "run_id", y = "kB/s")
ggsave(emit_kbps_out_path, plot = emit_kbps_plot, scale = 1)
#show (emit_kbps_plot)

execute_out_path = sprintf("%s/merged_runs/Execute_count.png", expe_path)
execute_plot <- ggplot(data=execute, aes(x=run_id, y=tuples_per_second, fill = factor(topology_version),  label = execute_count))+
    geom_bar(position="dodge", stat="identity") +
    ylim(0, 1000000) +
    geom_text(aes(label= round(tuples_per_second, digits = 0)), vjust = -2.5, size = 3, position = position_dodge(0.9)) +
    labs(title = "Execute_random_words_node Tuples/s", x = "run_id", y = "Tuples/s")
ggsave(execute_out_path, plot = execute_plot, scale = 1)
#show (execute_plot)

execute_kbps_out_path = sprintf("%s/merged_runs/Execute_kbps.png", expe_path)
execute_kbps_plot <- ggplot(data=execute, aes(x=run_id, y=kbps, fill = factor(topology_version),  label = execute_count))+
    geom_bar(position="dodge", stat="identity") +
    ylim(0, 300000) +
    geom_text(aes(label= round(kbps, digits = 0)), vjust = -2.5, size = 3, position = position_dodge(0.9)) +
    labs(title = "Execute_random_words_node kB/s", x = "run_id", y = "kB/s")
ggsave(execute_kbps_out_path, plot = execute_plot, scale = 1)
#show (execute_kbps_plot)
