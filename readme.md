
# Table of Contents
 * [Introduction](#introduction)
 * [Experiment information](#experiment-information)
  * [WordCount](#wordcount)
  * [Metrics](#metrics)
  * [Configuration](#configuration)
 * [Measurements for configuration_1](#measurements-for-configuration_1)
  * [Reading the plots](#reading-the-plots)
  * [General observations](#general-observations)
  * [Cpu overload over cpu consumption](#cpu-overload-over-cpu-consumption)
  * [Influence on container size on the throughput](#influence-on-container-size-on-the-throughput)
  * [Monothreaded vs Multithreaded](#monothreaded-vs-multithreaded)
 * [Conclusions](#conclusions)
  * [The container size has a high impact on throughput performances.](#the-container-size-has-a-high-impact-on-throughput-performances)
  * [Cpu consumption metric is not efficient for detecting undersized containers](#cpu-consumption-metric-is-not-efficient-for-detecting-undersized-containers)
  * [Multithreading in counting bolt shows performances improvement](#multithreading-in-counting-bolt-shows-performances-improvement)
 * [Further developments](#further-developments)
 * [Run the experiments (for g5k users only)](#run-the-experiments-for-g5k-users-only)
  * [Quick-start](#quick-start)
 * [Other information](#other-information)
  * [Building the environments](#building-the-environments)
  * [Connect to your deployed nodes](#connect-to-your-deployed-nodes)
  * [Monitor your cluster](#monitor-your-cluster)
  * [g5k Tips](#g5k-tips)

# Introduction
We present a performance evaluation of **Apache Heron** using *Mesos/Aurora* cluters. (Work in progress).

Our first objective was to observe cpu consumption inside mesos containers and evaluate the impact of the container size on
the throughput of of a *WordCount* topology.

Our second objective was to study the performance and the viability of thread-based component parallelism in Heron.
By design, Heron instances are "monothreaded" processes. Component parallelism is normally
obtained by spawning more Heron instances (processes/JVM). We modified the bolt of a *WordCount* topology
so the counting operation could be done in two threads instead of one and compared the monothreaded
bolt throughput and cpu consumption to the multithreaded one.

We observed that container size (nb cpus) had critical impact on the *WordCount* topology throughput.
We observed that the *total throttled time* spent by tasks inside Mesos containers was a more revealing metric
than *cpu usage* to detect undersized containers.
We propose a metric called **cpu_overload** based on this throttle time which could be added to Heron-ui and Mesos
to help topology tuning.

We observed that multithreading in the *WordCount* bolt introduced a significant cpu consumption overhead.
We attributed this overhead to synchronized access by the threads to a shared tuple input list.
As a result we observed performance degradation when counting small words (<500 characters) i.e. when high tuple/s throughput
was observed. However, when counting large words (>500 character), the multithreaded bolt offered significant performance improvement
over the monothreaded version. This result suggests multithreading could be used for the computing-intensive tasks in bolts.
Furthermore, our multithreaded bolt could be improved by adding separate input tuple queues for each thread.

# Experiment information

## WordCount
We based our study on a WordCount topology provided in [heron/examples/api](https://github.com/apache/incubator-heron/blob/0138b8d451c988f53e431f951f9a6fb9f2f4c116/examples/src/java/org/apache/heron/examples/api/WordCountTopology.java).
The topology is composed of two components:
 * One *spout* emitting fixed-size words picked randomly from a dictionary of 128k entries
 * One *bolt* consuming and counting those words.

We provide two topology version:
 * monothreaded: All components are monothreaded (spouts and bolts).
 * multithreaded: The counting bolt is multithreaded (two threads to do the counting operation).
[Thanks to Jerry Peng and nlu90 for their help](https://github.com/apache/incubator-heron/pull/2692)

For both the monothreaded and multithreaded version, users can set the values of the following parameters:
 * Wordsize : the size of words emitted by the spout(s)
 * Padding : The percentage of cpu padding -> used to increase the default cpu size of the containers.
 * Throttle : The duration of the pause in microseconds between two words emission -> used to control the spout throughput.

### About padding and container size
By default each *Heron instance* is allocated 1 cpu and 512 Mo of ram.
The size of a container only depend of the number of *Heron instances* inside it.
As an example, a container only containing one *spout* will be assigned 1cpu and 512 mo of ram.
We can double the size of this container by using a *padding percentage* of 100 and get 2 cpus and 1Go of ram.
 * Rem1: We don't enforce memory isolation so the memory value is not relevant to our experiments.
 * Rem2: There are parameters in Heron to tune component specific resources however, in *Aurora*, containers are homogeneous in size and
the biggest container dictate it's dimensions to the others. For this reason it is simpler for us to use *padding percentage* to control our containers size.

### About throttling, Backpresure, and spout emission rate
Without throttling the spout is emitting words at the maximum ingestion speed of the bolt.
If the spout throughput is too large for the bolt, then the bolt will ask the spout to reduce it's emission rate
and generate **backpresure**. The topology always run at the rate of the slowest component.

## Metrics

### Mesos cpu consumption
From Mesos [http://slave_node:5051/containers](http://slave_node:5051/containers) endpoint we fetch the following metrics at regular interval (10s)
 * **cpus_user_time_secs** : time spent by tasks of the *cgroup* in user mode.
 * **cpus_system_time_secs** : time spent by tasks of the *cgroup* in kernel mode.
 * **cpu_throttled_time** : total time for which tasks in the *cgroup* have been throttled.
This is the cumulative time across all the task in the *cgroup*. [https://issues.apache.org/jira/browse/MESOS-2103](https://issues.apache.org/jira/browse/MESOS-2103)

We use two consecutive measurements point *A* and *B* to compute the **cpu_consumption** inside the container for the 10s period that separate
the two points.

**Definition**: We define the **cpu_consumption** as the number of cpus used inside a container over a duration *D*.
Let *A* and *B* be two measurements points and *D* the time duration between *A* and *B*:

```Latex
cpu_consumption = (cpus_user_time_secs_B + cpus_system_time_secs_B) - (cpus_user_time_secs_A + cpus_system_time_secs_A))* 1/D
```

**Example1**: If a container/cgroup has a capacity of eight cpus:
Each scheduling interval (100ms), tasks/threads inside the container/cgroup have the right to spend 800ms in running state.

**Example2**: If the tasks inside a *cgroup* spent 400ms running during a 100ms interval the we say we used 4 cpus.

**Example3**: If during a 10 seconds interval the tasks inside a *cgroup* spent 12 seconds in running state we say we used 1.2 cpus.

We designed an experiment that shows a program **cpu_consumption** inside a *Mesos* container
and demonstrate the correctness of this metric. To consult this experiment results
please have a look [Here](https://gitlab.com/bornej/Heron_on_g5k/tree/master/aurora_programs/cpu).


### Mesos cpu overload
With **cpu_throttled** time we compute the **cpu_overload** in a container over a duration *D*.

**Definition**: We define the cpu overload as the amount of cpu consumption that was prevented because of the container cpu limit.
Let *A* and *B* be two measurements points and *D* the duration between *A* and *B*:

```latex
cpu_overload = (cpus_throttled_time_secs_B - cpus_throttled_time_secs_A) / D
```

**Example1**: Let's consider a container/cgroup having a capacity of 1 cpu.
This means that each scheduling interval (100ms) the threads inside the *cgroup* have the right to spend 100ms
in 'running' state. Suppose that in the container, we run a program with two threads doing busy waiting.
In a scheduling interval of 100ms, Without throttling, those two threads would normally use two computing cores
for 100 ms each amounting to 200 ms spent in running state.
But as the *cgroup* only has 1 cpu capacity, this mean we will observe 100 ms of throttling inside the container and
100 ms of cpu usage. Which translate to a **cpu_consumption** of 1 cpu and a **cpu_overload** of 1 cpu.

We designed an experiment that shows **cpu_overload** inside a *Mesos* container
and demonstrate the correctness of this metric. To consult this experiment results
please have a look [Here](https://gitlab.com/bornej/Heron_on_g5k/tree/master/aurora_programs/cpu_throttle).


### Heron throughput
In order to observe the throughput in *tuple/s* of the components of our topology we fetch statistics from *heron-tracker* *REST API*.
Particularly we fetch the **Emit_count** and **Execute_count** values for the **Emit_random_word** and **Consume_and_count** components of our topology.
These values gives us respectively the number of tuples emitted by the spouts and the number of tuples processed by the bolts over
a chosen period of time (10 min, 1h, ...).

### Nicstat network statistics
On each node we use the nicstat program to gather the following network statistics on
network interface Int0:
 * rKB/s: Read Kilobytes per second on the interface input
 * wKB/s: Written Kilobytes per second on the interface output
 * rPk/s: Read packets per second on the interface input
 * wPk/s: Written packets per second on the interface output
 * rAvs: Average size of packets received
 * wAvs: Average size of packets sent

We collect the data at regular interval (10s).

## Configuration
### Mesos resource isolation
Mesos provide resources isolation via various containerization technologies.
[https://mesos.apache.org/documentation/latest/mesos-containerizer/](https://mesos.apache.org/documentation/latest/mesos-containerizer/)
We used *mesos containerizer* with linux *cgroup* 'cpu' as isolator.
We enabled *CFS Bandwidth Limiting* to enforce a 'strict' upper limit of CPU time used by tasks inside a cgroup.
[https://mesos.apache.org/documentation/latest/isolators/cgroups-cpu/](https://mesos.apache.org/documentation/latest/isolators/cgroups-cpu/)

Regarding *cgroup/mem* isolator, from aurora documentation [https://aurora.apache.org/documentation/latest/features/resource-isolation/](https://aurora.apache.org/documentation/latest/features/resource-isolation/)
/"CPU is a soft limit, and handled differently from memory and disk space.
Too low a CPU value results in throttling your application and slowing it down.
Memory and disk space are both hard limits; when your application goes over these values, it’s killed."/
Since only one container is spawned per mesos-slave We didn't enforce any memory restriction.

### Heron packing algorithm
The *physical plan* of a topology, i.e the distribution of Heron instances inside containers,
is generated with the help of a *packing_algorithm*.
We used Heron RoundRobin.ResourcesCompliant *packing algorithm* as it allows the user to:
 * Force the number of containers.
 * Increase the nb of cpus inside containers via a padding parameter.


# Measurements for configuration_1

We configured our [topology](https://gitlab.com/bornej/Heron_on_g5k/blob/master/ruby-cute/experiments/wordcount_1/heron-program/multithreaded-word-count/src/main/java/bornej/heron/WordCountTopology.java)
so that one *spout* and one *bolt* are spawned in two separate containers.
We configured our *mesos-slaves* such that they offer just enough cpu resources to host a SINGLE container each.
This way containers are located on separate nodes in our cluster.
```ditaa
       Node_1                          Node_2
+-------------------+           +-------------------+
|    Container_1    |           |    Container_2    |
|+-----------------+|           |+-----------------+|
||Emit_random_words||---------->||Consume_and_count||
|+-----------------+|           |+-----------------+|
+-------------------+           +-------------------+
```
*Rem: The topology master container, omitted here, is on a third separate node.*


We conducted our study with the following factor levels:
padding=[0, 100, 200 , 300, 400] and wordsize=[10, 100 , 200 , 500 , 1000, 1500]

An experiments consist of the measurements of **cpu consumption**, **cpu overload**, **Network statistics**, **Heron throughput**, for
a given combination of factors levels (e.g: word_size=10, padding=0) and for each topology version ([monothreaded, multithreaded]).

Before each experiment run we deploy a Heron cluster with one **Aurora-master** (Mesos-master, Heron/Aurora client,...)
and three **Aurora-workers** (Spout, Bolt, Topology Master).

We ran each experiment at least 4 times (up to 10 times), deploying the Heron cluster on different sets of machines each run.
For each topology version we ran the measurements for 600 seconds.

 * **Rem**: In real situations, Heron programs run for days or months. We could not reasonably study our topology performance over
such duration. We tried to run several experiments for 1 hour and observed that the metrics remained stable
for the total duration of the experiments. Furthermore, we obtained similar results over shorter experiments duration.
Therefore we privileged the possibility to run shorter experiments multiple times on different hardware rather than
running a longer experiment on a single set of machines. By running the same experiment on random sets of
machines we ensure that our measurements are not heavily dependent on Hardware parameters.

## Reading the plots
For each metric we represent the experiments and their multiple runs measurements in an single bar plot.

### Cpu overload
<p align="center"><img src="https://gitlab.com/bornej/Heron_on_g5k/raw/master/images/experiments/wordcount/config_1/merged_experiments/mesos_cpu_overload_p1.25.png" width="900" height="578"/></p>

On the Y axis, the mean overload observed in the Bolt and Spout containers (Here container size = 1.25 cpus).

The bars are grouped in two blocks:
The leftmost continuous-line bars block represent the Bolt container values
The rightmost dashed-line bars block represent the Spout container values.

In each block, the color shades represent the word_size values, from brightest (10 characters) to darkest (1500 characters).
For each word_size we present, side-by-side, the monothreaded topology values in red and
the multithreaded topology values in blue.

### Cpu consumption
<p align="center"><img src="https://gitlab.com/bornej/Heron_on_g5k/raw/master/images/experiments/wordcount/config_1/merged_experiments/mesos_cpu_p1.25.png" width="900" height="578"/></p>

On the Y axis the mean cpu consumption observed in the Bolt and Spout (Here: container size = 1.25 cpus).
The red horizontal line materialize the container size (nb_cpus) i.e. the cpu limit for the tasks inside the container.

### network statistics
<p align="center"><img src="https://gitlab.com/bornej/Heron_on_g5k/raw/master/images/experiments/wordcount/config_1/merged_experiments/network_kbps_p1.25.png" width="900" height="578"/></p>
On the Y axis, the mean throughput (in Kilobytes per second) observed in the Bolt and Spout nodes (Here: container size = 1.25 cpus).
For the Spout node, the KBps value represent the mean **send** Kilobytes per second on the network interface eth0.
For the Bolt node, the KBps value represent the mean **received** Kilobytes per second on the network interface eth0.
The bars are grouped similarly to the precedent plot.


 * **Rem**: By comparing the Heron throughput values to the Networks statistics measured we can verify that the nicstat values
are coherent. As an example if we measure a Heron throughput of 350000 tuples/s with a word size of 100 character (100 Bytes)
we expect to measure approx. 35000 wKB/s written on the spout's node Int0 interface.
This is a rough approximation as we ignore the size overhead introduced by the packets headers.
We observed that for word size >100

```bash
(Heron_throughput(tuples/s) * word_size(character))/1000 ~= network_kbps. 
```

 * **rem**: For word size = 10 the packets size is non negligible and we observe
network_kbps > 2*(Heron_throughput(tuples/s) * word_size(character))/1000

### Backpresure
<p align="center"><img src="https://gitlab.com/bornej/Heron_on_g5k/raw/master/images/experiments/wordcount/config_1/merged_experiments/Heron_backpresure_p1.25.png" width="700" height="525"/></p>
On the Y axis the mean time spent in backpresure by the bolt component per minute.

 * **Rem**: The error bars represent the standard deviation for each value across the different experiments run.

## General observations
By skimming through the measurement we can make the following general observations.
### Cpu overload
We recall that **cpu_overload** is an indication that task inside the container are being throttled
because they exceed the container cpus resources.
When tuning its container sizes, the topology developer should choose a value such that no overload is observed.
Click on the image below to observe what is happening to the cpu overload when we increase the container sizes.
<a href="https://gitlab.com/bornej/Heron_on_g5k/raw/master/images/experiments/wordcount/config_1/merged_experiments/gifs/mesos_cpu_overload_p.gif"><img src="https://gitlab.com/bornej/Heron_on_g5k/raw/master/images/experiments/wordcount/config_1/merged_experiments/small/mesos_cpu_overload_p1.25.png"/></a>

 * For the spout container (rightmost dashed bars):
2.25 cpus are needed to avoid **cpu_overload**.
With 1.25 cpus the spout container presents **cpu_overload**.
This indicates that, regardless of the bolt performances, with 1.25 cpu, the spout will not emit at it's maximum rate.
We will keep in mind that 1.25 cpus is a special value in our analysis.

 * For the bolt container (leftmost bars):
Respectively 4.25 cpus and 5.25 cpus are needed for the monothreaded and multithreaded version to avoid **cpu_overload** for all word sizes.
The **cpu_overload** decrease when container size increase.

### Cpu consumption
Click on the image below to observe what is happening to the cpu consumption when we increase the container sizes.
<a href="https://gitlab.com/bornej/Heron_on_g5k/raw/master/images/experiments/wordcount/config_1/merged_experiments/gifs/mesos_cpu_p.gif"><img src="https://gitlab.com/bornej/Heron_on_g5k/raw/master/images/experiments/wordcount/config_1/merged_experiments/small/mesos_cpu_p1.25.png"/></a>

see cpu overload over cpu consumption section

### Heron throughput
Click on the image below to observe what is happening to the Heron throughput when we increase the container sizes.
<a href="https://gitlab.com/bornej/Heron_on_g5k/raw/master/images/experiments/wordcount/config_1/merged_experiments/gifs/Heron_throughput_p.gif"><img src="https://gitlab.com/bornej/Heron_on_g5k/raw/master/images/experiments/wordcount/config_1/merged_experiments/small/Heron_throughput_p1.25.png"/></a>

We observe that for all container sizes, the spout and the bolt throughput are similar, this indicates all the tuples emitted by the spout are
processed by the bolt. We observe that the throughput (tuples/s) decreases with the words size.

 * For the monothreaded version (red):
The throughput double between 1.25 and 2.25 cpus containers then show small improvement between 2.25 and 3.25 cpus
and stabilizes for values above 3.25. The big gap of performance for between 1.25 and 2.25 cpu values,
can be linked to the **cpu_overload** observed in the *spout* container.
*see container size vs throughput section*

 * For the multithreaded version (blue):
The throughput increase with every increment of container size. This can be linked to the decreasing **cpu_overload** observed
in the bolt container.

### Network statistics
Click on the image below to observe what is happening to the Network throughput when we increase the container sizes.
<a href="https://gitlab.com/bornej/Heron_on_g5k/raw/master/images/experiments/wordcount/config_1/merged_experiments/gifs/network_kbps_p.gif"><img src="https://gitlab.com/bornej/Heron_on_g5k/raw/master/images/experiments/wordcount/config_1/merged_experiments/small/network_kbps_p1.25.png"/></a>

The throughput in Kilobytes per seconds increase with the container size and with the word size
(until 1500 character where it drops).
 * For the monothreaded version (red):
A maximum throughput of 193 Mo/s is observed for word_size = 1000 and container_size = 4.25/5.25
 * For the multithreaded version (blue):
A maximum throughput of 244 Mo/s is observed for word_size = 1000 and container_size = 4.25/5.25

We observe that for small word sizes and small containers, the monothreaded version
performs better than the multithreaded one.

However with large containers (4.25, 5.25 cpus) monothreaded and multithreaded versions show similar performances
for small words (10, 100, 200). For larger words (500, 1000, 1500) the multithreaded version outperforms
the monothreaded one.


### Heron Backpressure
Click on the image below to observe what is happening to the Backpressure when we increase the container sizes.
<a href="https://gitlab.com/bornej/Heron_on_g5k/raw/master/images/experiments/wordcount/config_1/merged_experiments/gifs/heron_backpresure_p.gif"><img src="https://gitlab.com/bornej/Heron_on_g5k/raw/master/images/experiments/wordcount/config_1/merged_experiments/small/Heron_backpresure_p1.25.png"/></a>
We recall that backpressure in the bolt container is an indicator that the bolt can't
follow up with the spout emission rate.

 * For the monothreaded version (red):
We observe that backpressure significantly **increase** from 1.25 to 2.25 cpus.
We recall that from our **cpu_overload** analysis
we stated that for 1.25 cpus the *spout* container was presenting heavy **cpu_overload** and
the spout likely couldn't emit tuples at it's maximum rate. This backpresure measurement
tends to validate this hypothesis.
We then see a small backpressure reduction from 2.25 cpu to 3.25 cpus
and stable values for 3.25 cpus and more.
Backpressure values increase with the word size.

 * For the multithreaded version (blue):
Let aside the 1.25 cpus containers which implies *spout* throttling,
the backpressure decrease with container size.
The backpressure relation on word size isn't as clear as for the monothreaded version.
We observe high backpressure for small word sizes which can be attributed to bad performance of
the bolt due to synchronization between the threads.
The bolt however seams to perform better with big words, especially with 1000 character words
for which no backpressure is observed.


## Cpu overload over cpu consumption
We stated that **cpu_overload** was a better metric than **cpu_consumption** to detect undersized containers.
Let's see why with this example.
<p align="center"><img src="https://gitlab.com/bornej/Heron_on_g5k/raw/master/images/experiments/wordcount/config_1/merged_experiments/mesos_cpu_p1.25.png" width="900" height="578"/></p>

On the Y axis, the mean cpu consumption observed in the Bolt and Spout with 1.25 cpus containers .
The red horizontal line materialize the container size (nb_cpus) i.e. the cpu limit for the tasks inside the container.

In the spout container (rightmost dashed bars) we observe that for words < 1000 characters,
the multithreaded topology (blue) have a low cpu consumption (<0.7 cpus).
Let's now see the **cpu_overload** for those word sizes.
<p align="center"><img src="https://gitlab.com/bornej/Heron_on_g5k/raw/master/images/experiments/wordcount/config_1/merged_experiments/mesos_cpu_overload_p1.25.png" width="900" height="578"/></p>

We observe that despite measuring mean cpu usage well under the container limit we observe
**cpu_overload** in the spout container. This means that the **cpu_consumption** metric, while [correct](https://gitlab.com/bornej/Heron_on_g5k/tree/master/aurora_programs/cpu), fail
to show that task inside the container are being throttled.

For this reason, we think that **cpu_overload** is a better suited metric than **cpu_consumption**
to properly detect undersized containers.

## Influence on container size on the throughput
To better observe the influence of the container size on the throughput we propose an alternative data representation.
In the following plot we represent for a fixed word size (100), the throughput measured for each container size
<p align="center"><img src="https://gitlab.com/bornej/Heron_on_g5k/raw/master/images/experiments/wordcount/config_1/merged_experiments/Heron_throughput_s100.png" width="900" height="578"/></p>

On the Y axis, the mean throughput (in tuples per second) observed in the Bolt and Spout containers (Here for words of size 100 characters.)

The bars are grouped in two blocks:
The leftmost continuous-line bars block represent the Bolt container values
The rightmost dashed-line bars block represent the Spout container values.

In each block, the color shades represent the container size (nb_cpus) values, from brightest (1.25 cpus) to darkest (5.25 cpus).
For each container size we present, side-by-side, the monothreaded topology values in red and
the multithreaded topology values in blue.

 * For the monothreaded version (red):
The throughput increase for containers of size 1.25, 2.25, 3.25 and stabilizes.
 * For the multithreaded version (blue):
The throughput increase for containers of size 1.25, 2.25, 3.25, 4.25.
 * **Rem**: for 5.25 we observe instability in the measurments (high standard deviation), we should add more runs to fix this.

<p align="center"><img src="https://gitlab.com/bornej/Heron_on_g5k/raw/master/images/experiments/wordcount/config_1/merged_experiments/network_kbps_s100.png" width="900" height="578"/></p>

On the Y axis, the mean throughput (in kbps per second) observed in the Bolt and Spout containers (Here for words of size 100 characters.)
Confirming the observation in Heron throughtput we observe in the Network statistics that
 * For the monothreaded version (red):
The throughput increase for containers of size 1.25, 2.25, 3.25 and stabilizes.
 * For the multithreaded version (blue):
The throughput increase for containers of size 1.25, 2.25, 3.25, 4.25 and stabilize

Those observations can be linked to the **cpu_overload** observed in the containers
<p align="center"><img src="https://gitlab.com/bornej/Heron_on_g5k/raw/master/images/experiments/wordcount/config_1/merged_experiments/mesos_cpu_overload_s100.png" width="900" height="578"/></p>
 * For the monothreaded version (red):
With 1.25 cpus the performance degradation is caused by the *spout* container overload (rightmost dashed bar).
With 2.25 cpus we don't observe overload in the spout container but we still observe small overload in the bolt container.
With 3.25, 4.25, 5.25 cpus we don't observe any overload.

 * For the multithreaded version (blue):
With 1.25 cpus we observe large overload in the bolt container 2.8 cpus
With 2.25 cpus the overload in the bolt container decrease to 1.9 cpus.
With 3.25 cpus the overload in the bolt container decrease to 0.9 cpus.
With 4.25 and 5.25 cpus we don't observe any overload.

We observe again that **cpu_overload** gives use good insight when trying to analyse topology behavior and that
containers size play an important role in throughput performance.

## Monothreaded vs Multithreaded
### Cpu overload
<p align="center"><img src="https://gitlab.com/bornej/Heron_on_g5k/raw/master/images/experiments/wordcount/config_1/merged_experiments/mesos_cpu_overload_p1.25.png" width="900" height="578"/></p>

In the bolt container(leftmost block), even for the smallest word size (10),
the multithreaded bolt show an overload of 2.8 cpu while the monothreaded version only has an overload of 0.1 cpu.
This indicates a large cpu consumption overhead introduced by multithreading.
This overhead is probably due to synchronized access by the threads to the tuple input queue (Java *LinkedBlockingQueue*).
In our configuration, the two threads share the same input queue and synchronization is required to ensure the threads
read from the queue and update without quirks.

 * *Rem: In light of those measurements, a better design would be to use separate input queues for each counting thread,
so we don't waste time in synchronization. (TODO)

### Cpu consumption
<p align="center"><img src="https://gitlab.com/bornej/Heron_on_g5k/raw/master/images/experiments/wordcount/config_1/merged_experiments/mesos_cpu_p5.25.png" width="900" height="578"/></p>
As mentionned previously, we observe a **cpu_consumption** overhead introduced by multithreading in the bolt component.
This overhead is more important for smaller word sizes.

In the spout container we observe similar **cpu_consumption** values for small word_size values, and
higher consumption in the multithreaded topology for larger words. This difference of consumption
is caused by backpressure in the monothreaded topology. (see next plot)
<p align="center"><img src="https://gitlab.com/bornej/Heron_on_g5k/raw/master/images/experiments/wordcount/config_1/merged_experiments/Heron_backpresure_p5.25.png" width="700" height="525"/></p>
The monothreaded bolt (red) can't handle the spout emission rate for large words, so the spout reduce it's emiting rate
hence consuming less cpu resources.

### Network statistics
<p align="center"><img src="https://gitlab.com/bornej/Heron_on_g5k/raw/master/images/experiments/wordcount/config_1/merged_experiments/network_kbps_p5.25.png" width="900" height="578"/></p>
When comparing the monothreaded and multithreaded version in a context where no overload is present (5.25 cpus) we observe that
For small words (10, 100, 200) the monothreaded version has a better throughput than the multithreaded version. However for larger words the multithreaded
topology outperforms the monothreaded one.

<p align="center"><img src="https://gitlab.com/bornej/Heron_on_g5k/raw/master/images/experiments/wordcount/config_1/merged_experiments/Heron_throughput_p5.25.png" width="900" height="578"/></p>
We sumarize the measured throughput in the following table.

```ditaa
+-----------+-------------------+--------------------+---------------------+------------------------+
| Word_size | Monothreaded Mo/s | Multithreaded Mo/s | Monothread Tuples/s | Multithreaded Tuples/s |
|-----------+-------------------+--------------------+---------------------+------------------------|
|        10 |                20 |                 17 |              967398 |                 805561 |
|       100 |                80 |                 73 |              789210 |                 698823 |
|       200 |               110 |                124 |              572282 |                 617793 |
|       500 |               161 |                200 |              354135 |                 429757 |
|      1000 |               188 |                244 |              211832 |                 270550 |
|      1500 |               105 |                180 |               79152 |                 134441 |
+-----------+-------------------+--------------------+---------------------+------------------------+
```

# Conclusions
## The container size has a high impact on throughput performances.
When enforcing a strict cpu limitation, as soon as tasks inside one of the containers hits the container's cpu limit, the
performance of the whole system deteriorates drastically. (TODO show a data representation to further illustrate this point).

Component experiencing throttling => Performance degradation for this component => Backpresure
=> Performance degradation for whole system.

## Cpu consumption metric is not efficient for detecting undersized containers
When tasks inside a container hits the container's cpu limit, they are throttled (put in IDLE state).
Mesos collect throttled_time we found that observing this
metric revealed more information than observing the cpu consumption inside a container.
There were cases were tasks inside a container showed a cpu consumption under the container limits yet throttling was observed.

It is crucial, when enforcing strict cpu limiting that tasks never hits the containers limits
so we need to find a reliable way to detect overload.

From throttled_time, we derived a metric "cpu_overload" that could help developers find the correct size for their containers.

## Multithreading in counting bolt shows performances improvement
We observed that a multithreaded counting bolt outperformed a monothreaded one when performing computing intensive
task (count large words). However when the spout had a high emitting velocity (small words) the multithreaded bolt
was not able to compete with the monothreaded bolt (probably due to the synchronisation mechanism used for threads to access the
tuple input-queue).

This was our first experience at conducing performance analysis, design reproducible experiments,
work with a real distributed environment,... This was a privileged occasion to improve our skills and
were very pleased to work on this first performance evaluation work.

# Further developments
 * We observed improvement with multithreading, it would be interesting to compare the performance of two monothreaded bolts
vs one multithreaded bolt (2 threads) i.e

```ditaa
       Node_1                          Node_2
+-------------------+           +-------------------+
|    Container_1    |           |    Container_2    |
|+-----------------+|           |+-----------------+|
||Emit_random_words||---------->||Consume_and_count||
|+-----------------+|           |+-----------------+|
+-------------------+           |+-----------------+|
                                ||Consume_and_count||
                                |+-----------------+|
                                +-------------------+
```
vs
```ditaa
       Node_1                          Node_2
+-------------------+           +-------------------+
|    Container_1    |           |    Container_2    |
|+-----------------+|           |+-----------------+|
||Emit_random_words||---------->||Consume_and_count<-----multithreaded (2 threads)
|+-----------------+|           |+-----------------+|
+-------------------+           +-------------------+
```
 * We implemented a naive multithreaded counting-bold, we would like to improve our implementation by introducing separed input
queues for each thread.

 * We didn't had time to further experiment with bigger/ more complex topologies.
 * Presentation of data is not optimal yet. Graphs are dense which can makes them hard to read.
We would like to synthesis the information better.

# Run the experiments (for g5k users only)
## Quick-start
To run the experiments on grid5000 you must first build custom images from scripts and do some ssh configuration.
 * Push your local ssh public key into the site-frontend you wish to conduct your experiment to (e.g. rennes)

```Bash
user@local-workstation:~/$ scp -r -p ~/.ssh/id_rsa.pub g5k-login@frontend.rennes.grid5000.fr:/home/g5k-login/.ssh/work_rsa.pub
```

/!\ This will allow your public key to be pushed on the nodes running the experiments so you can access them from outside g5K.
 * Connect to the site frontend

```Bash
user@local-workstation:~/$ ssh g5k-login@frontend.rennes.grid5000.fr
```

 * Clone the repository in your site-frontend homedir.

```Bash
user@site-frontend:~/$ git clone https://gitlab.com/bornej/Heron_on_g5k.git
```

 * Install ruby dependencies

```Bash
user@site-frontend:~/$ gem install --user-install ruby-cute net-ssh net-scp curb
user@site-frontend:~/$ export PATH=$PATH:$(ruby -e 'puts "#{Gem.user_dir}/bin"')
```

 * Build the environments (this will take at least 1h but requires no interaction)

```Bash
user@site-frontend:~/Heron_on_g5k/ruby-cute:$ ./customise_env.rb
```

 * Run mesos_cpu experiment

```Bash
user@site-frontend:~/Heron_on_g5k/ruby-cute/experiments/wordcount_1/:$ ./wordcount_1.rb -z true
```

# Other information
## Building the environments
We provide scripts (Heron_on_g5k/scripts/Aurora_[worker,master]_provision.sh)
to build **Aurora_worker** and **Aurora_master** environments starting from an ubuntu1404-x64-min image.
please take a look [here](https://gitlab.com/bornej/Heron_on_g5k/tree/master/scripts) for more information.
## Connect to your deployed nodes
 * From the site frontend

```
site frontend: ssh root@node_hostname
```

 * From you local workstation

```Bash
local workstation: ssh root@node_hostname.site.grid5000.fr
```

## Monitor your cluster
Heron, aurora, and mesos provide HTTP monitoring tools which are running on the Aurora_master node.
To access the heron, mesos and aurora http monitoring tools from your local workstation do

```Bash
local workstation: ssh root@Aurora_master_node_hostname.rennes.grid5000.fr -D 6000
```

This open a SOCKS proxy on port 6000.
Then, configure your web browser to use manual proxy for localhost on port 6000 (ex. firefox):
<p align="center"><img src="https://gitlab.com/bornej/Heron_on_g5k/raw/merged_stats/images/firefox_proxy.png"  width="600" height="580" /></p>

After this, every HTTPs traffic will be forwarded to the Aurora master node and you will be able to access:

 * Mesos : [http://localhost:5050/#/](http://localhost:5050/#/)
 * Aurora: [http://localhost:8081/](http://localhost:8081/)
 * Heron:  [http://localhost:8889/](http://localhost:8889/)

## g5k Tips
 * Once connected to a frontend site, work inside a *tmux* session.
 * Deployment will fail occasionally, most of the time I would advise you to *oardel YOUR_RESERVATION_ID* and re-run the script
