#!/usr/bin/env bash

# This script provision and configure an Apache heron/aurora/mesos cluster with a single node
# on top of a grid5000 ubuntu1404-min environnement.
# Please refer to https://gricad-gitlab.univ-grenoble-alpes.fr/bornej/Heron_on_g5k/
# for more informations.

function update_and_upgrade {
    apt-get update
    apt-get upgrade -y
}

function install_utils {
    apt-get install -y nano
}

# Source for aurora installation:
# https://aurora.apache.org/documentation/latest/operations/installation/

# -----------------------------------------------------------------------#
# Aurora Coordinator install
function install_java {
    apt-get install -y software-properties-common
    add-apt-repository -y ppa:openjdk-r/ppa
    apt-get update
    apt-get install -y openjdk-8-jre-headless
    apt-get install -y java8-runtime-headless
}

# /!\ Install this first as dpkg dependencies will break if not present when installing aurora client/tools
function install_python {
    apt-get install -y python2.7-minimal wget
}

function install_mesos {
    apt-key adv --keyserver keyserver.ubuntu.com --recv E56151BF
    DISTRO=$(lsb_release -is | tr '[:upper:]' '[:lower:]')
    CODENAME=$(lsb_release -cs)

    echo "deb http://repos.mesosphere.io/${DISTRO} ${CODENAME} main" | \
        tee /etc/apt/sources.list.d/mesosphere.list
    apt-get -y update

    # Use `apt-cache showpkg mesos | grep [version]` to find the exact version.
    apt-get -y install mesos=1.1.0-2.0.107.ubuntu1404
}

function start_mesos_master {
    start mesos-master
}

function install_zookeeper {
    apt-get install -y zookeeperd
}

function install_aurora_scheduler {
    add-apt-repository -y ppa:openjdk-r/ppa
    apt-get update
    apt-get install -y openjdk-8-jre-headless wget

    update-alternatives --set java /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java

    wget -c https://apache.bintray.com/aurora/ubuntu-trusty/aurora-scheduler_0.17.0_amd64.deb
    dpkg -i aurora-scheduler_0.17.0_amd64.deb
}

function finalizing_coordinator_install {
    stop aurora-scheduler
    sudo -u aurora mkdir -p /var/lib/aurora/scheduler/db
    sudo -u aurora mesos-log initialize --path=/var/lib/aurora/scheduler/db
    start aurora-scheduler
}

# -----------------------------------------------------------------------#
# Aurora Client install
# We collocate the aurora client in the coordinator node
# so we can submit Topology from it

function install_client {
    # Require install python
    wget -c https://apache.bintray.com/aurora/ubuntu-trusty/aurora-tools_0.17.0_amd64.deb
    dpkg -i aurora-tools_0.17.0_amd64.deb
}

# -----------------------------------------------------------------------#
# Aurora Worker install
# We configure our node as a mesos slave so it can run topologies
function start_mesos_slave {
    start mesos-slave
}

# We install aurora executor and observer to schedule and run the topologies on the node.
function install_aurora_executor_and_observer {
    # Require install_python
    # NOTE: This appears to be a missing dependency of the mesos deb package and is needed
    # for the python mesos native bindings.
    apt-get -y install libcurl4-nss-dev

    wget -c https://apache.bintray.com/aurora/ubuntu-trusty/aurora-executor_0.17.0_amd64.deb
    dpkg -i aurora-executor_0.17.0_amd64.deb
}

function worker_configuration {
    sh -c 'echo "MESOS_ROOT=/tmp/mesos" >> /etc/default/thermos'
}

function install_heron_from_sources {
    apt-get install -y git build-essential automake cmake libtool zip curl libunwind-setjmp0-dev zlib1g-dev unzip pkg-config python-setuptools
    export CC=/usr/bin/gcc
    export CCX=/usr/bin/g++
    apt-get install -y software-properties-common
    add-apt-repository -y ppa:webupd8team/java
    apt-get update -y
    echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | \
	/usr/bin/debconf-set-selections
    apt-get install -y oracle-java8-installer
    export JAVA_HOME="/usr/lib/jvm/java-8-oracle"
    wget -O /tmp/bazel.sh https://github.com/bazelbuild/bazel/releases/download/0.14.1/bazel-0.14.1-installer-linux-x86_64.sh
    chmod +x /tmp/bazel.sh
    /tmp/bazel.sh --user
    apt-get install -y python-dev python-pip
    export PATH="$PATH:$HOME/bin"
    git clone https://github.com/apache/incubator-heron.git
    cd incubator-heron/
    # We build from a version we have tested -> you may remove this line and build from latest master's head
    # however it may break the build.
    git checkout d7c8c62f05e7c14daf78d200129ad17f6d3fe970
    ./bazel_configure.py
    bazel build --config=ubuntu heron/...
    bazel build --config=ubuntu scripts/packages:binpkgs
    bazel build --config=ubuntu scripts/packages:tarpkgs
    cd bazel-bin/scripts/packages/
    ./heron-install.sh
    cd
}

function configure_heron {

    mv /usr/local/heron/conf /usr/local/heron/conf_bak
    cd 
    cp -r /root/Heron_on_g5k/heron-conf /usr/local/heron/
    mv /usr/local/heron/heron-conf /usr/local/heron/conf
    echo 'export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-amd64' >> /root/.bashrc
    source /root/.bashrc
}

function configure_aurora {
    sed -i 's/example/devcluster/g' /etc/aurora/clusters.json
}

function install_jdk {
    apt-get install -y openjdk-8-jdk
}

function install_and_configure_maven {
    apt-get install -y maven
    mvn dependency:purge-local-repository -DmanualInclude="org.apache.heron:heron-api-built-from-sources"
    mvn install:install-file -Dfile=/root/incubator-heron/bazel-genfiles/heron/api/src/java/heron-api.jar -DgroupId=org.apache.heron -DartifactId=heron-api-built-from-sources -Dversion=0.17.8 -Dpackaging=jar
}

function run_heron_ui {
    heron_tracker 2> /dev/null &
    heron_ui 2> /dev/null &
}

# -----------------------------------------------------------------------#
function install_taktuk {
    apt-get install -y taktuk
}

# Do it!
update_and_upgrade
install_python

# Aurora Coordinator installation
install_java
install_mesos
start_mesos_master
install_zookeeper
install_aurora_scheduler
finalizing_coordinator_install

# Aurora Client installation
install_client

# Aurora Worker installation
start_mesos_slave
install_aurora_executor_and_observer
worker_configuration

# Heron install
install_heron_from_sources
install_jdk
configure_heron
configure_aurora

install_and_configure_maven
install_utils
# Install taktuk
install_taktuk
