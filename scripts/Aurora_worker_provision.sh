#!/usr/bin/env bash

# This script provision and configure an Apache aurora worker node
# on top of a grid5000 ubuntu1404-min environnement.
# An Aurora worker node is a mesos slave running aurora executor and observer.
# Please refer to https://gricad-gitlab.univ-grenoble-alpes.fr/bornej/Heron_on_g5k/
# for more informations.

function update_and_upgrade {
    apt-get update
    apt-get upgrade -y
}

function install_utils {
    apt-get install -y nano
    apt-get install -y taktuk
}

# Source for aurora installation:
# https://aurora.apache.org/documentation/latest/operations/installation/

# /!\ Install this first as dpkg dependencies will break if not present when installing aurora client/tools
function install_python {
    apt-get install -y python2.7 wget
}

function install_java {
    apt-get install -y software-properties-common
    add-apt-repository -y ppa:openjdk-r/ppa
    apt-get update
    apt-get install -y openjdk-8-jre-headless
    apt-get install -y java8-runtime-headless
}

function install_mesos {
    apt-key adv --keyserver keyserver.ubuntu.com --recv E56151BF
    DISTRO=$(lsb_release -is | tr '[:upper:]' '[:lower:]')
    CODENAME=$(lsb_release -cs)
    
    echo "deb http://repos.mesosphere.io/${DISTRO} ${CODENAME} main" | \
        tee /etc/apt/sources.list.d/mesosphere.list
    apt-get -y update

    # Use `apt-cache showpkg mesos | grep [version]` to find the exact version.
    apt-get -y install mesos=1.1.0-2.0.107.ubuntu1404
}

# -----------------------------------------------------------------------#
# Aurora Worker install
# We configure our node as a mesos slave so it can run topologies
function start_mesos_slave {
    start mesos-slave
}

# We install aurora executor and observer to schedule and run the topologies on the node.
function install_aurora_executor_and_observer {
    # NOTE: This appears to be a missing dependency of the mesos deb package and is needed
    # for the python mesos native bindings.
    apt-get -y install libcurl4-nss-dev
    wget -c https://apache.bintray.com/aurora/ubuntu-trusty/aurora-executor_0.17.0_amd64.deb
    dpkg -i aurora-executor_0.17.0_amd64.deb
}

function worker_configuration {
    sh -c 'echo "MESOS_ROOT=/tmp/mesos" >> /etc/default/thermos'
    stop mesos-slave
    start mesos-slave
    stop thermos
    start thermos
}

install_python
install_mesos
start_mesos_slave
install_aurora_executor_and_observer
worker_configuration
install_java
install_utils
