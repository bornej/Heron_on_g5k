# In this experiment we deploy a mesos container of 1cpu on a mesos slave in which runs this program.
# Our objective is to verify the total_time_throtted metric offered by mesos when using mesos containerizer with cgroups/cpu
# isolator.
# This program runs two process performing busy waiting.
# Thus, each process try to use one cpu core for the total duration of the experiment.
# Because the container only offer 1 cpu we should measure that the cpu consumption is equal to one
# for the total duration of the experiment.
# Furthermore, the two processes inside the cgroup/container share this single cpu time
# We threfore expect the processes inside the container to be throttled for 50% of the experiment time.
# (whynotthreads? apparently threading is unlikely to achieve true parallelism in
# python: https://en.wikipedia.org/wiki/Global_interpreter_lock)

import time
from multiprocessing import Process

def main():

    while True:
        print("loop")

if __name__ == "__main__":
   p1 = Process(target=main)
   p1.start()
   p2 = Process(target=main)
   p2.start()
   p1.join()
   p2.join()
