* Experiment Description 
In this experiment we deploy a mesos container of 1 cpu in which runs two process doing busy waiting.
Each process try to use one cpu core for the total duration of the experiment.
Because the container only offer 1 cpu we should measure that the cpu consumption is equal to 1 
and that the cpu overload is equal to 1 for the total duration of the experiment.
We recall that cpu_overload is defined as the number of cpu that would have been used if there was no cpu limit enforced by the container.
* Measurements results
We compute the cpu_consumption inside the container over 600 periods of 1 seconds and we plot the result of the measurements: 

#+html: <p align="center"><img src="https://gricad-gitlab.univ-grenoble-alpes.fr/bornej/Heron_on_g5k/raw/master/images/experiments/cpu_overload/mesos_cpu_consumption.png" width="640" height="639" /></p>
- red line : the container cpus_limit = 1
- blue line: the mean cpu_consumption over the 600 measurement points

On the above plot we clearly observe that the cpu consumption over the experiment duration is just below 1 cpu as expected.

We compute the cpu_overload inside the container over 600 periods of 1 seconds and we plot the result of the measurements: 

#+html: <p align="center"><img src="https://gricad-gitlab.univ-grenoble-alpes.fr/bornej/Heron_on_g5k/raw/master/images/experiments/cpu_overload/mesos_cpu_overload.png" width="640" height="639" /></p>
- red line : the container cpus_limit = 1
- blue line: the mean cpu_overload over the 600 measurement points

On the above plot we clearly observe that the cpu overload over the experiment duration is equal to 1 cpu as expected.
This mean that without the cpu limit imposed by the cgroup we should have measured a mean cpu consumption equal 
to 2 (cpu_consumption_with_limit + cpu_overload).

* Usage
To run this experiment you must have a working Aurora/mesos cluster with at least one worker node, one coordinator node and one 
client node. Please refer to aurora documentation to configure a cluster:
[[https://aurora.apache.org/documentation/latest/operations/installation/]]

Run the experiment on the cluster
#+BEGIN_SRC Bash
sh ./cpu_run.sh
#+END_SRC

Kill the experiment
#+BEGIN_SRC Bash
sh ./cpu_kill.sh
#+END_SRC

** For grid5000 users

Follow the quickstart instructions to build Aurora_master and Aurora_worker environnements. 
Then do
#+BEGIN_SRC Bash
user@site-frontend:~/Heron_on_g5k/ruby-cute/experiments/wordcount_1/:$ ./wordcount_1.rb -z true
#+END_SRC

This will launch the cpu_consumption experiment, measure for 600s then launch the cpu_overload experiment measure for 600s
and write the results in
#+BEGIN_SRC Bash
user@site-frontend:~/Heron_on_g5k/ruby-cute/experiments/wordcount_1/mesures/cpu_test_[g5k_site]_cluster/[run_id]
#+END_SRC
To plot the results do
#+BEGIN_SRC Bash
user@site-frontend:~/cd Heron_on_g5k/ruby-cute/experiments/wordcount_1/R/
user@site-frontend:~/chmod +x cpu_mesos.r && ./cpu_mesos.r [run_id]
#+END_SRC
with [run_id] the id of the experiment run you wish to process.
After completion the plot's png files will be in the [run_id] folder.
