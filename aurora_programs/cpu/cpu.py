# This program consumes 100% of the cpu resources for 10 seconds and sleeps for 10 seconds.
# Our goal is to deploy this program in a mesos container and verify that the metrics provided by mesos are correct.
import time

def main():

    while True:
        t1 = time.time()
        t2 = t1
        while t2 - t1 < 10:
            t2 = time.time()
            print("t2- t1: %f " % (t2 - t1))
            print("loop")

        print("sleep")
        time.sleep(10)

if __name__ == "__main__":
    main()
